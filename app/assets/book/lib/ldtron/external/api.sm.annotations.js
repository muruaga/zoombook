/**
 * @namespace api.sm
 */
var api = api || { sm: {} };

// Compatibility with IE
if (!String.prototype.endsWith) {
  String.prototype.endsWith = function(searchString, position) {
      var subjectString = this.toString();
      if (typeof position !== 'number' || !isFinite(position) || Math.floor(position) !== position || position > subjectString.length) {
        position = subjectString.length;
      }
      position -= searchString.length;
      var lastIndex = subjectString.indexOf(searchString, position);
      return lastIndex !== -1 && lastIndex === position;
  };
}


/**
 * @class api.sm.Annotations
 * @constructor
 * @param {options} options Opciones de ejecución. Valores por defecto definidos en la clase api.sm.Annotations.DefaultOpcions
 * @param {function} requestService Función que devuelve los datos Ajax(Request) para salvar las anotaciones
 * @param {function} responseService Función que retorna los datos Ajax(Response) cuando se salvan anotaciones
 */
api.sm.Annotations = function Annotations(options, _requestService, _responseService) {
  this.options = $.extend(api.sm.Annotations.DefaultOpcions, options);
  this.requestService = _requestService;
  this.responseService = _responseService;
  if (this.options.serviceURL !== null && this.options.serviceURL.length > 0) {
    if (!this.options.serviceURL.endsWith('/')) {
      this.options.serviceURL += '/';
    }
    this.start();
  }
  else {
    console.log('api.sm.Annotations is not started. The URL is declared [' + this.options.serviceURL + ']. See the variable on window top "annotations_service_url"');
  }
};


/**
 * @class api.sm.Annotations.DefaultOpcions Valores por defecto de ejecución de la instacia api.sm.Annotations.
 * @property {String} serviceURL Url del servicio de anotaciones
 * @property {number} [checkAnnotsInterval=2000] Frecuencia para pedir al visor si hay cambios en anotaciones
 * @property {number} [verifyConnInterval=0] Frecuencia para comprobar conexion con el servicio
 * @property {element} [notifyElement=$(document)] Elemento JQuery para notificar el estado de conexión del servicio
 */
api.sm.Annotations.DefaultOpcions = {
  serviceURL: null,
  checkAnnotsInterval: 2000,
  verifyConnInterval: 10000,
  notifyElement: $(document)
};


/**
 * Métodos de la clase api.sm.Annotations
 * @type {Object}
 */
api.sm.Annotations.prototype = {

  running: false,   // Estado de ejecucion
  isRunnning: function() {
    return this.running;
  },

  connected: true, // Estado de conexion

  /**
   * Envía los datos de anotaciones por Ajax al servicio de anotaciones
   * @param {Object} data Contiene la informacion de las anotaciones y el timestamp actual
   */
  sendAnnotations: function() {
    var me = this;

    // Anotaciones del visor
    var data = this.requestService();

    if (data !== null) { // Si hay datos que enviar

      // Opciones de envio
      var ajaxOptions = {
        guidUser: this.options.guidUser,
        guidPieza: this.options.guidPieza,
        productCode: this.options.productCode,
        timestamp: data.timestamp,
        data: data.annotations,
        cookie: this.options.sessionID
      };

      // Peticion Ajax
      $.ajax({
        url: this.options.serviceURL + "save",
        type: "POST",
        data: JSON.stringify(ajaxOptions),
        contentType: "application/json",
        cache: false,
        dataType: "json",
        success: function(response, text) {
          me.responseService(response);
          me.tick();
        },
        error: function(request, status, error) {
          me.changeConnectionStatus(false);
          me.tick();
        }
      });
    }
    else {
      me.tick();
    }
  },


  /**
   * Comprueba por intervalos si el visor tiene datos modificado y los envía al servicio de anotaciones
   */
  start: function() {
    var me = this;
    if (!me.isRunnning()) {
      me.running = true;
      me.tick();
    }
  },


  /**
   * Detiene la comprobación de anotaciones
   */
  stop: function() {
    this.running = false;
  },


  /**
   * Intento de comprobación de anotaciones o conexión
   */
  tick: function() {
    if (!this.running) return; // Se tiene que llamar start para que se ejecute

    var me = this;
    var interval = (me.connected) ? me.options.checkAnnotsInterval: me.options.verifyConnInterval;
    setTimeout(function() {
      if (me.connected) {
        me.sendAnnotations();
      } else {
        me.verifyConnection();
      }
    }, interval);
  },


  /**
   * Comprueba por Ajax la conexion con el servicio
   */
  verifyConnection: function() {
    var me = this;

    // Peticion Ajax
    $.ajax({
      url: me.options.serviceURL + "verify",
      type: "GET",
      contentType: "text/plain",
      cache: false,
      success: function(data, text) {
        me.changeConnectionStatus(true);
        me.tick();
      },
      error: function(request, status, error) {
        me.tick();
      }
    });
  },


  /**
   * Notifica a notifyElement si ha cambio de conexión su estado
   * @param  {Boolean} value - true/false Indica si hay o no conexión
   */
  changeConnectionStatus: function(value) {
    this.connected = value;
    if (this.connected) {
      $(this.options.notifyElement).trigger('annotationsServiceOn');
    } else {
      $(this.options.notifyElement).trigger('annotationsServiceOff');
    }
  }
}