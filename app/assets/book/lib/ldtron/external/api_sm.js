// ATENCIÓN:
// Para evitar problemas con instalaciones que no disponen de el sinónimo '$' para la función jQuery,
// se debe utilizar SIEMPRE la función jQuery en lugar de su sinónimo.
/*
 API Integración
 Revision: 1023
 UPDATE: Mantener coherencia con VERSION-RELEASE. Alfresco consulta 'Revision:'
 */

var api_sm = function () { };

var APISM_INTEGRACION = {
    VERSION: {
        RELEASE: '1024',
        DATE: '31/08/2018'
    }
};

api_sm.prototype = {
    constructor: function () {
        this.frames = [];
        this.methods = [];
        this.listen = "";
        this.start_stop = "";
    },
    receiveMessage: function (event) {
        var json = JSON.parse(event.data);
        // Ignorar lo demás si es un mensaje que proviene de la API de trazas.
        if (json.action.indexOf("actSM") >= 0) {
            return;
        }

        switch (json.action) {

            case "register":
                this.registerFrame(json);
            break;

            case "heightFrame":
                this.ajutaAltoFrame(json.id, json.alto);
                if (typeof (PieceManager) !== "undefined" && typeof (PieceManager.iframeLoaded) !== "undefined") {
                    PieceManager.iframeLoaded(this.getFrame(json.id), json.alto); // GSPL-376
                }
                var idAPI = json.id;
                var $iframe = this.getFrame(idAPI);
                if ($iframe.length === 1) {
                  var mess = '{"action":"heightFrameDone"}';
                  $iframe[0].contentWindow.postMessage(mess, "*");
                }
            break;

            case "getPosition":
                this.sendPositionToFrame(json.id);
            break;

            case "popup":
                alert("pendiente de implementación");
            break;

            case "extend":
                var el = this;
                if (json.actions instanceof Array) {
                    jQuery.each(json.actions, function () {
                        el.methods.push(this);
                    });
                } else {
                    el.methods.push(json.actions);
                }
                this.loadJSExtend(json.id, json.url);
            break;

            case "open_reference": {
                /** Custom */
                call_reference(json.imessage);
                // call_reference_double(json.imessage);

            } break;

            case "fullscreen": {
                /** Custom */
                toggle_fullscreen();
                // make this call at the end of the toggle function or in an event fire at the end of the toggle
                setTimeout(function () { ApiSM.sendPositionToFrame(json.id); }, 1000);
                // toggle_fullscreen_double();
            } break;

            case "open":
                this.openWindow(json);
            break;

            case "redirect":
                var idAPI = json.id;
                if (this.frames.indexOf(idAPI) === -1) {
                  this.frames.push(idAPI);
                }
                this.getFrame(idAPI).attr('src', json.url);

                if (json.scroll) {
                    this.registerScroll(idAPI, json.scroll);
                }
            break;

            case "fullscreen_mode":
                var idAPI = json.id;
                if (this.getFrame(idAPI).length === 1) {
                  this.fullscreenMode(json);
                }
            break;

            case "scrollTop":
                var idAPI = json.id;
                var $iframe = this.getFrame(idAPI);
                if ($iframe.length === 1) {
                  // Se mueve el scroll de la ventana
                  var $window = jQuery(window);
                  var top = $window.scrollTop();
                  $window.scrollTop(top + json.yIncrement);
                }
            break;

            case "scrollY":
                var idAPI = json.id;
                var $iframe = this.getFrame(idAPI);
                if ($iframe.length === 1) {
                  jQuery(window).scrollTop(json.yPosition);
                }
            break;

            default:
                this.lookForExtended(json);
            break;
        }

    },
    getOriginFrame: function(origin) {
        origin = decodeURIComponent(origin);
        var frame = null;
        jQuery("iframe").each(function () {
            var src = decodeURIComponent(jQuery(this).attr("src"));
            var exists = new RegExp(origin + "$").test(src) ||
                new RegExp(src + "$").test(origin) ||
                (origin.indexOf(src) !== -1);
            if (exists) {
                frame = jQuery(this);
                return false;
            }
        });
        return frame;
    },
    registerFrame: function (json) {
        var idAPI = "MoodleReg_" + this.frames.length;
        var origin = new String(json.origin);
        var frame = this.getOriginFrame(origin);
        if (frame !== null && frame.length === 1) {
            frame.attr("id", idAPI);
            if (json.allowfullscreen) {
                frame.attr("allowfullscreen", "true");
                frame.attr("webkitallowfullscreen", "true");
                frame.attr("mozallowfullscreen", "true");
            }
            if (json.scrolling) {
                frame.attr("scrolling", json.scrolling);
            }
            this.frames.push(idAPI);

            var $iFrame = jQuery("#" + idAPI);
            $iFrame.addClass("frame-API");

            var confirmOptions = {
                action: "confirmRegister",
                idAPI: idAPI,
                apiSMRelease: APISM_INTEGRACION.VERSION.RELEASE,
                apiSMDate: APISM_INTEGRACION.VERSION.DATE,
                absolutePath: this.getAbsolutePath(frame.attr('src')),
                userGUID : window.userguid, // GUID del usuario moodle
                productCode: window.productcode, // codigo de producto
                annotationsServiceURL : window.annotations_service_url, // Url del servicio de anotaciones
                sessionID: this.getCookie('MoodleSession'), // ID de sesion de moodle
                smFullscreen: $iFrame.hasClass('sm-fullscreen')
            };
            frame[0].contentWindow.postMessage(JSON.stringify(confirmOptions), "*");
            this.apiid = idAPI;
        } else {
            console.log("error al registrarse: " + json.origin);
        }

        if (json.scroll) {
            this.registerScroll(idAPI, json.scroll);
        }
    },
    registerScroll: function (idAPI, scroll) {
        switch (scroll) {
            case "listen":
                if (this.listen !== "") this.listen += ", ";
                this.listen += "#" + idAPI;
            break;
            case "start|stop":
                if (this.start_stop !== "") this.start_stop += ", ";
                this.start_stop += "#" + idAPI;
            break;
        }
    },
    getFrame: function (id) {
        return jQuery("#" + id);
    },
    getFrames: function (id) {
        return jQuery(id);
    },
    ajutaAltoFrame: function (id, alto) {
        var iFrame = this.getFrame(id);
        if ( !isNaN(jQuery('body').css("zoom")) ) {
            var zoom = parseFloat(jQuery("body").css("zoom"));
            if (zoom !== 1){
                alto += parseInt(alto * (1-zoom), 10);
            }
        }
        if (iFrame.length === 1) {
            iFrame.height(alto); 
        }
    },
    alertEndScrollToFrames: function () {
        var el = this;
        jQuery(this.start_stop).each(function () {

            var iFrame = jQuery(this);
            var mess = el.getPositions(iFrame);
            iFrame[0].contentWindow.postMessage(mess, "*");
        });
    },
    sendPositionToListening: function () {
        var el = this;
        if (this.listen && this.listen.length) {
          jQuery.each(this.listen.split(","), function () {
            id = new String(this).replace("#", "");
            el.sendPositionToFrame(id);
          });
        }
    },
    sendPositionToFrames: function () {
        var el = this;
        jQuery.each(this.frames, function () {
            el.sendPositionToFrame(this);
        });
    },
    sendPositionToFrame: function (id) {
        var iFrame = this.getFrame(id);
        if (iFrame.length === 1) {
            var mess = this.getPositions(iFrame);
            iFrame[0].contentWindow.postMessage(mess, "*");
        }
    },
    getPositions: function (iFrame) {
        var zoom = 1;
        if ( !isNaN(jQuery("body").css("zoom")) ) {
            zoom = parseFloat(jQuery("body").css("zoom"));
        }
        var frameTop = parseInt(iFrame.offset().top, 10) * zoom;
        var scrollTop = parseInt(jQuery(window).scrollTop(), 10) * zoom;
        var winHeight = jQuery(window).height();

        var mess = '{"action":"position", ';
        mess += '"topFrame": "' + frameTop + '", ';
        mess += '"scrollTop": "' + scrollTop + '", ';
        mess += '"height": "' + winHeight + '", ';
        mess += '"zoom": "' + zoom + '" ';
        mess += ' }';

        return mess;
    },
    alertScrollToFrames: function () {
        var iFrames = this.getFrames(this.start_stop);
        iFrames.each(function () {
            this.contentWindow.postMessage('{"action":"scrollStart"}', "*");
        });
    },
    alertScrollToFrame: function (id) {
        var iFrame = this.getFrame(id);
        if (iFrame.length === 1) {
            iFrame[0].contentWindow.postMessage('{"action":"scrollStart"}', "*");
        }
    },
    loadJSExtend: function (frame, path) {
        var relativePath = jQuery("#" + frame).attr("src").split("/");
        for (i = 0; i < relativePath.length - 1; i++) {
            path = relativePath[i] + "/" + path;
        }

        jQuery.getScript(path, function () {
            //alert("registred " + path);
        });

    },
    lookForExtended: function (json) {
        jQuery.each(this.methods, function () {
            if (this.action === json.action) {
                eval(this.function).call(json);
            }
        });
    },
    openWindow: function (json) {

        var fileURL = "";
        var isAndroid = this.IsAndroidCordovaDevice();
        var isIOS = this.IsIOSCordovaDevice();

        var isHttpURL = (json.path.indexOf('http:') !== -1 || json.path.indexOf('https:') !== -1);

        if (json.isAbsolutePath === true) {
            fileURL = json.path;
        }
        else if (json.type === "doc" && !isHttpURL) {
            var relativePath = jQuery("#" + json.id).attr("src").split("/");
            for (i = relativePath.length - 2; i >= 0; i--) {
                json.path = relativePath[i] + "/" + json.path;
            }
            fileURL = json.path.split("?")[0].split("#")[0];
            if ((fileURL.indexOf("http:") === -1) && (fileURL.indexOf("https:") === -1) && (fileURL.indexOf("file:") === -1)) {
                fileURL = this.ObtenerRutaOffline(fileURL);
            }
        }

        var isFileURL = (fileURL.indexOf('file:') !== -1);

        if (json.type === "doc" && isAndroid) {
            if (isHttpURL) {
                this.openRemoteDoc(json.path);
            }
            else if (isFileURL) {
                this.openWebIntent(fileURL);
            }
        }
        else {
            var isMobileApp = typeof(window.device) !== 'undefined';
            if (isMobileApp) {
                this.openURLInsideApp(json.path);
            } else {
                window.open(json.path, '_blank');
            }
        }
    },
    openWebIntent: function (fileURL) {
        var re = /(?:\.([^.]+))?$/;
        var extension = re.exec(fileURL)[1];
        var mimeType = this.CordovaMimeTypes[extension].type;
        if (typeof (mimeType) !== "undefined") {
            var iParams = {
                action: top.window.plugins.webintent.ACTION_VIEW,
                url: fileURL,
                type: mimeType
            };
            top.window.plugins.webintent.startActivity(iParams, function (args) { }, function (args) { console.log("fail get FS in API SM: " + args); });
        }
    },
    openRemoteDoc : function(fileURL) {
        var _self = this;
        var filenameDownload = fileURL.substr(fileURL.lastIndexOf('/') + 1);

        this.showWaitPanel();
        window.requestFileSystem(LocalFileSystem.TEMPORARY, 0,
            function(fileSystem) {
                fileSystem.root.getDirectory('data', {create: true, exclusive: false},
                    function(dirEntry) {
                        dirEntry.getFile(filenameDownload, {create: true, exclusive: false},
                            function(fileEntry){
                                _self.fileDownload(fileURL, fileEntry.nativeURL,
                                    function (fileURL) {
                                        _self.hideWaitPanel();
                                        _self.openWebIntent(fileURL);
                                    }
                                );
                            },
                            _self.alertFailMsg
                        );
                    },
                    _self.alertFailMsg
                );
            },
            _self.alertFailMsg
        );
    },
    openURLInsideApp: function(url) {
        var inNodeWK = typeof(process) !== "undefined";
        if (inNodeWK) {
            if (url.indexOf('file:///') > -1 || url.indexOf('http://') > -1 || url.indexOf('https://') > -1) {
                var gui = require('nw.gui');
                gui.Window.open(url, {
                    toolbar: true,
                    frame: true,
                    position: 'center',
                    width: 1280,
                    height: 720
                }) ;
            }
            else {
                if(url.indexOf('com.sm.savia') > -1){
                    url = 'file://' + url;
                } else{
                    url = 'http:/' + url;
                }
                var gui = require('nw.gui');
                gui.Window.open(url, {
                    toolbar: true,
                    frame: true,
                    position: 'center',
                    width: 1280,
                    height: 720
                }) ;
            }
        }
        else {
            window.open(url, '_blank');
        }
    },
    fileDownload: function (fileUrl, filePath, onSuccess) {
        var _self = this;
        var fileTransfer = new FileTransfer();
        var uri = encodeURI(fileUrl);

        fileTransfer.download(
            uri,
            filePath,
            function(entry) {
                onSuccess(filePath);
            },
            function(error) {
                console.log('download error source ' + error.source);
                console.log('download error target ' + error.target);
                console.log('download error code' + error.code);
                _self.alertFailMsg('No se ha podido descargar el archivo que se quiere visualizar. Compruebe que la ruta sea correacta. ' + fileUrl);
            },
            false,
            {
                headers: {
                    'Authorization': 'Basic dGVzdHVzZXJuYW1lOnRlc3RwYXNzd29yZA=='
                }
            }
        );
    },
    alertFailMsg: function(msg) {
        alert(msg);
        this.hideWaitPanel();
    },
    showWaitPanel: function() {
        var $waitPanel = jQuery('#smWaitPanel');
        if ($waitPanel.length === 0) {
            $waitPanel = jQuery('<div id="smWaitPanel"></div>').appendTo('body');
            $waitPanel.css({
                'position':'fixed',
                'left':0,
                'top':0,
                'background-color':'#000000',
                'opacity':'.5',
                'width': '100%',
                'height': '100%'
            });
        }
    },
    hideWaitPanel: function() {
        jQuery('#smWaitPanel').remove();
    },
    IsAndroidCordovaDevice: function () {
        try {
            return (typeof window.top.device !== "undefined" && window.top.device.platform === 'Android');
        } catch (e) {
            return false;
        }
    },

    isiPhoneWA: function() {
        try {
            return ( navigator.userAgent.toLowerCase().match(/iphone/i) !== null );
        } catch (e) {
            return false;
        }
    },
    isiPadWA: function() {
        try {
            return ( navigator.userAgent.toLowerCase().match(/ipad/i) !== null );
        } catch (e) {
            return false;
        }
    },
    IsIOSCordovaDevice: function() {
        try {
            return this.isiPhoneWA() || this.isiPadWA();
        } catch (e) {
            return false;
        }
    },


    CordovaMimeTypes: {
        "xxx": { "type": "document/unknown", "icon": "unknown" }, "3gp": { "type": "video/quicktime", "icon": "quicktime", "groups": ["video"], "string": "video" }, "aac": { "type": "audio/aac", "icon": "audio", "groups": ["audio"], "string": "audio" }, "accdb": { "type": "application/msaccess", "icon": "base" }, "ai": { "type": "application/postscript", "icon": "eps", "groups": ["image"], "string": "image" }, "aif": { "type": "audio/x-aiff", "icon": "audio", "groups": ["audio"], "string": "audio" }, "aiff": { "type": "audio/x-aiff", "icon": "audio", "groups": ["audio"], "string": "audio" }, "aifc": { "type": "audio/x-aiff", "icon": "audio", "groups": ["audio"], "string": "audio" }, "applescript": { "type": "text/plain", "icon": "text" }, "asc": { "type": "text/plain", "icon": "sourcecode" }, "asm": { "type": "text/plain", "icon": "sourcecode" }, "au": { "type": "audio/au", "icon": "audio", "groups": ["audio"], "string": "audio" }, "avi": { "type": "video/x-ms-wm", "icon": "avi", "groups": ["video", "web_video"], "string": "video" }, "bmp": { "type": "image/bmp", "icon": "bmp", "groups": ["image"], "string": "image" }, "c": { "type": "text/plain", "icon": "sourcecode" }, "cct": { "type": "shockwave/director", "icon": "flash" }, "cpp": { "type": "text/plain", "icon": "sourcecode" }, "cs": { "type": "application/x-csh", "icon": "sourcecode" }, "css": { "type": "text/css", "icon": "text", "groups": ["web_file"] }, "csv": { "type": "text/csv", "icon": "spreadsheet", "groups": ["spreadsheet"] }, "dv": { "type": "video/x-dv", "icon": "quicktime", "groups": ["video"], "string": "video" }, "dmg": { "type": "application/octet-stream", "icon": "unknown" }, "doc": { "type": "application/msword", "icon": "document", "groups": ["document"] }, "docx": { "type": "application/vnd.openxmlformats-officedocument.wordprocessingml.document", "icon": "document", "groups": ["document"] }, "docm": { "type": "application/vnd.ms-word.document.macroEnabled.12", "icon": "document" }, "dotx": { "type": "application/vnd.openxmlformats-officedocument.wordprocessingml.template", "icon": "document" }, "dotm": { "type": "application/vnd.ms-word.template.macroEnabled.12", "icon": "document" }, "dcr": { "type": "application/x-director", "icon": "flash" }, "dif": { "type": "video/x-dv", "icon": "quicktime", "groups": ["video"], "string": "video" }, "dir": { "type": "application/x-director", "icon": "flash" }, "dxr": { "type": "application/x-director", "icon": "flash" }, "eps": { "type": "application/postscript", "icon": "eps" }, "epub": { "type": "application/epub+zip", "icon": "epub", "groups": ["document"] }, "fdf": { "type": "application/pdf", "icon": "pdf" }, "flv": { "type": "video/x-flv", "icon": "flash", "groups": ["video", "web_video"], "string": "video" }, "f4v": { "type": "video/mp4", "icon": "flash", "groups": ["video", "web_video"], "string": "video" }, "gif": { "type": "image/gif", "icon": "gif", "groups": ["image", "web_image"], "string": "image" }, "gtar": { "type": "application/x-gtar", "icon": "archive", "groups": ["archive"], "string": "archive" }, "tgz": { "type": "application/g-zip", "icon": "archive", "groups": ["archive"], "string": "archive" }, "gz": { "type": "application/g-zip", "icon": "archive", "groups": ["archive"], "string": "archive" }, "gzip": { "type": "application/g-zip", "icon": "archive", "groups": ["archive"], "string": "archive" }, "h": { "type": "text/plain", "icon": "sourcecode" }, "hpp": { "type": "text/plain", "icon": "sourcecode" }, "hqx": { "type": "application/mac-binhex40", "icon": "archive", "groups": ["archive"], "string": "archive" }, "htc": { "type": "text/x-component", "icon": "markup" }, "html": { "type": "text/html", "icon": "html", "groups": ["web_file"] }, "xhtml": { "type": "application/xhtml+xml", "icon": "html", "groups": ["web_file"] }, "htm": { "type": "text/html", "icon": "html", "groups": ["web_file"] }, "ico": { "type": "image/vnd.microsoft.icon", "icon": "image", "groups": ["image"], "string": "image" }, "ics": { "type": "text/calendar", "icon": "text" }, "isf": { "type": "application/inspiration", "icon": "isf" }, "ist": { "type": "application/inspiration.template", "icon": "isf" }, "java": { "type": "text/plain", "icon": "sourcecode" }, "jcb": { "type": "text/xml", "icon": "markup" }, "jcl": { "type": "text/xml", "icon": "markup" }, "jcw": { "type": "text/xml", "icon": "markup" }, "jmt": { "type": "text/xml", "icon": "markup" }, "jmx": { "type": "text/xml", "icon": "markup" }, "jpe": { "type": "image/jpeg", "icon": "jpeg", "groups": ["image", "web_image"], "string": "image" }, "jpeg": { "type": "image/jpeg", "icon": "jpeg", "groups": ["image", "web_image"], "string": "image" }, "jpg": { "type": "image/jpeg", "icon": "jpeg", "groups": ["image", "web_image"], "string": "image" }, "jqz": { "type": "text/xml", "icon": "markup" }, "js": { "type": "application/x-javascript", "icon": "text", "groups": ["web_file"] }, "latex": { "type": "application/x-latex", "icon": "text" }, "m": { "type": "text/plain", "icon": "sourcecode" }, "mbz": { "type": "application/vnd.moodle.backup", "icon": "moodle" }, "mdb": { "type": "application/x-msaccess", "icon": "base" }, "mov": { "type": "video/quicktime", "icon": "quicktime", "groups": ["video", "web_video"], "string": "video" }, "movie": { "type": "video/x-sgi-movie", "icon": "quicktime", "groups": ["video"], "string": "video" }, "m3u": { "type": "audio/x-mpegurl", "icon": "mp3", "groups": ["audio"], "string": "audio" }, "mp3": { "type": "audio/mp3", "icon": "mp3", "groups": ["audio", "web_audio"], "string": "audio" }, "mp4": { "type": "video/mp4", "icon": "mpeg", "groups": ["video", "web_video"], "string": "video" }, "m4v": { "type": "video/mp4", "icon": "mpeg", "groups": ["video", "web_video"], "string": "video" }, "m4a": { "type": "audio/mp4", "icon": "mp3", "groups": ["audio"], "string": "audio" }, "mpeg": { "type": "video/mpeg", "icon": "mpeg", "groups": ["video", "web_video"], "string": "video" }, "mpe": { "type": "video/mpeg", "icon": "mpeg", "groups": ["video", "web_video"], "string": "video" }, "mpg": { "type": "video/mpeg", "icon": "mpeg", "groups": ["video", "web_video"], "string": "video" }, "odt": { "type": "application/vnd.oasis.opendocument.text", "icon": "writer", "groups": ["document"] }, "ott": { "type": "application/vnd.oasis.opendocument.text-template", "icon": "writer", "groups": ["document"] }, "oth": { "type": "application/vnd.oasis.opendocument.text-web", "icon": "oth", "groups": ["document"] }, "odm": { "type": "application/vnd.oasis.opendocument.text-master", "icon": "writer" }, "odg": { "type": "application/vnd.oasis.opendocument.graphics", "icon": "draw" }, "otg": { "type": "application/vnd.oasis.opendocument.graphics-template", "icon": "draw" }, "odp": { "type": "application/vnd.oasis.opendocument.presentation", "icon": "impress" }, "otp": { "type": "application/vnd.oasis.opendocument.presentation-template", "icon": "impress" }, "ods": { "type": "application/vnd.oasis.opendocument.spreadsheet", "icon": "calc", "groups": ["spreadsheet"] }, "ots": { "type": "application/vnd.oasis.opendocument.spreadsheet-template", "icon": "calc", "groups": ["spreadsheet"] }, "odc": { "type": "application/vnd.oasis.opendocument.chart", "icon": "chart" }, "odf": { "type": "application/vnd.oasis.opendocument.formula", "icon": "math" }, "odb": { "type": "application/vnd.oasis.opendocument.database", "icon": "base" }, "odi": { "type": "application/vnd.oasis.opendocument.image", "icon": "draw" }, "oga": { "type": "audio/ogg", "icon": "audio", "groups": ["audio"], "string": "audio" }, "ogg": { "type": "audio/ogg", "icon": "audio", "groups": ["audio"], "string": "audio" }, "ogv": { "type": "video/ogg", "icon": "video", "groups": ["video"], "string": "video" }, "pct": { "type": "image/pict", "icon": "image", "groups": ["image"], "string": "image" }, "pdf": { "type": "application/pdf", "icon": "pdf" }, "php": { "type": "text/plain", "icon": "sourcecode" }, "pic": { "type": "image/pict", "icon": "image", "groups": ["image"], "string": "image" }, "pict": { "type": "image/pict", "icon": "image", "groups": ["image"], "string": "image" }, "png": { "type": "image/png", "icon": "png", "groups": ["image", "web_image"], "string": "image" }, "pps": { "type": "application/vnd.ms-powerpoint", "icon": "powerpoint", "groups": ["presentation"] }, "ppt": { "type": "application/vnd.ms-powerpoint", "icon": "powerpoint", "groups": ["presentation"] }, "pptx": { "type": "application/vnd.openxmlformats-officedocument.presentationml.presentation", "icon": "powerpoint" }, "pptm": { "type": "application/vnd.ms-powerpoint.presentation.macroEnabled.12", "icon": "powerpoint" }, "potx": { "type": "application/vnd.openxmlformats-officedocument.presentationml.template", "icon": "powerpoint" }, "potm": { "type": "application/vnd.ms-powerpoint.template.macroEnabled.12", "icon": "powerpoint" }, "ppam": { "type": "application/vnd.ms-powerpoint.addin.macroEnabled.12", "icon": "powerpoint" }, "ppsx": { "type": "application/vnd.openxmlformats-officedocument.presentationml.slideshow", "icon": "powerpoint" }, "ppsm": { "type": "application/vnd.ms-powerpoint.slideshow.macroEnabled.12", "icon": "powerpoint" }, "ps": { "type": "application/postscript", "icon": "pdf" }, "qt": { "type": "video/quicktime", "icon": "quicktime", "groups": ["video", "web_video"], "string": "video" }, "ra": { "type": "audio/x-realaudio-plugin", "icon": "audio", "groups": ["audio", "web_audio"], "string": "audio" }, "ram": { "type": "audio/x-pn-realaudio-plugin", "icon": "audio", "groups": ["audio"], "string": "audio" }, "rhb": { "type": "text/xml", "icon": "markup" }, "rm": { "type": "audio/x-pn-realaudio-plugin", "icon": "audio", "groups": ["audio"], "string": "audio" }, "rmvb": { "type": "application/vnd.rn-realmedia-vbr", "icon": "video", "groups": ["video"], "string": "video" }, "rtf": { "type": "text/rtf", "icon": "text", "groups": ["document"] }, "rtx": { "type": "text/richtext", "icon": "text" }, "rv": { "type": "audio/x-pn-realaudio-plugin", "icon": "audio", "groups": ["video"], "string": "video" }, "sh": { "type": "application/x-sh", "icon": "sourcecode" }, "sit": { "type": "application/x-stuffit", "icon": "archive", "groups": ["archive"], "string": "archive" }, "smi": { "type": "application/smil", "icon": "text" }, "smil": { "type": "application/smil", "icon": "text" }, "sqt": { "type": "text/xml", "icon": "markup" }, "svg": { "type": "image/svg+xml", "icon": "image", "groups": ["image", "web_image"], "string": "image" }, "svgz": { "type": "image/svg+xml", "icon": "image", "groups": ["image", "web_image"], "string": "image" }, "swa": { "type": "application/x-director", "icon": "flash" }, "swf": { "type": "application/x-shockwave-flash", "icon": "flash", "groups": ["video", "web_video"] }, "swfl": { "type": "application/x-shockwave-flash", "icon": "flash", "groups": ["video", "web_video"] }, "sxw": { "type": "application/vnd.sun.xml.writer", "icon": "writer" }, "stw": { "type": "application/vnd.sun.xml.writer.template", "icon": "writer" }, "sxc": { "type": "application/vnd.sun.xml.calc", "icon": "calc" }, "stc": { "type": "application/vnd.sun.xml.calc.template", "icon": "calc" }, "sxd": { "type": "application/vnd.sun.xml.draw", "icon": "draw" }, "std": { "type": "application/vnd.sun.xml.draw.template", "icon": "draw" }, "sxi": { "type": "application/vnd.sun.xml.impress", "icon": "impress" }, "sti": { "type": "application/vnd.sun.xml.impress.template", "icon": "impress" }, "sxg": { "type": "application/vnd.sun.xml.writer.global", "icon": "writer" }, "sxm": { "type": "application/vnd.sun.xml.math", "icon": "math" }, "tar": { "type": "application/x-tar", "icon": "archive", "groups": ["archive"], "string": "archive" }, "tif": { "type": "image/tiff", "icon": "tiff", "groups": ["image"], "string": "image" }, "tiff": { "type": "image/tiff", "icon": "tiff", "groups": ["image"], "string": "image" }, "tex": { "type": "application/x-tex", "icon": "text" }, "texi": { "type": "application/x-texinfo", "icon": "text" }, "texinfo": { "type": "application/x-texinfo", "icon": "text" }, "tsv": { "type": "text/tab-separated-values", "icon": "text" }, "txt": { "type": "text/plain", "icon": "text", "defaulticon": true }, "wav": { "type": "audio/wav", "icon": "wav", "groups": ["audio"], "string": "audio" }, "webm": { "type": "video/webm", "icon": "video", "groups": ["video"], "string": "video" }, "wmv": { "type": "video/x-ms-wmv", "icon": "wmv", "groups": ["video"], "string": "video" }, "asf": { "type": "video/x-ms-asf", "icon": "wmv", "groups": ["video"], "string": "video" }, "xdp": { "type": "application/pdf", "icon": "pdf" }, "xfd": { "type": "application/pdf", "icon": "pdf" }, "xfdf": { "type": "application/pdf", "icon": "pdf" }, "xls": { "type": "application/vnd.ms-excel", "icon": "spreadsheet", "groups": ["spreadsheet"] }, "xlsx": { "type": "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "icon": "spreadsheet" }, "xlsm": { "type": "application/vnd.ms-excel.sheet.macroEnabled.12", "icon": "spreadsheet", "groups": ["spreadsheet"] }, "xltx": { "type": "application/vnd.openxmlformats-officedocument.spreadsheetml.template", "icon": "spreadsheet" }, "xltm": { "type": "application/vnd.ms-excel.template.macroEnabled.12", "icon": "spreadsheet" }, "xlsb": { "type": "application/vnd.ms-excel.sheet.binary.macroEnabled.12", "icon": "spreadsheet" }, "xlam": { "type": "application/vnd.ms-excel.addin.macroEnabled.12", "icon": "spreadsheet" }, "xml": { "type": "application/xml", "icon": "markup" }, "xsl": { "type": "text/xml", "icon": "markup" }, "zip": { "type": "application/zip", "icon": "archive", "groups": ["archive"], "string": "archive" }
    },
    ObtenerRutaOffline: function (ruta) {
        var absURL = "";
        var href = window.location.href;
        var loc = href.split("/");
        absURL = href.replace(loc[loc.length - 1], "");

        return absURL + ruta;
    },
    fixedIFrame: function($theIFrame) {
        $theIFrame
        .attr("style",
            "position: fixed;" +
            "left: 0 !important;" +
            "top: 0 !important;" +
            "z-index: 1000000000;" +
            "margin:0;"
        )
        .attr('height', window.innerHeight)
        .attr("scrolling", "no");
    },
    fullscreenMode: function(options) {
        switch (options.mode) {
            case "IFRAME_ABSOLUTE":
                typeof toggle_fullscreen !== 'undefined' && toggle_fullscreen(); // toggle_fullscreen de SaviaDigital
                var $iFrame = this.getFrame(options.id);
                var iFramePosition = $iFrame.data('iframe-position');
                if (!iFramePosition) {
                    $iFrame.data('iframe-position', $iFrame.css('position'));
                }

                var $mainContainer=  jQuery('.main .container');
                var $mainContent=  jQuery('.main .main_content');
                if (!$iFrame.hasClass('sm-fullscreen')) {
                    $iFrame.css({
                        "position": "absolute",
                        "left": 0
                    })
                        .addClass('sm-fullscreen');
                    $iFrame.data('main-container-position', $mainContainer.css('position'));
                    $iFrame.data('main-content-position', $mainContent.css('position'));
                    $mainContainer.css('position', 'initial');
                    $mainContent.css('position', 'initial');
                }
                else {
                    $iFrame.css({
                        "position": $iFrame.data('iframe-position')
                    })
                        .removeClass('sm-fullscreen');
                    $mainContainer.css('position', $iFrame.data('main-container-position'));
                    $mainContent.css('position', $iFrame.data('main-content-position'));
                }
                setTimeout(function () { ApiSM.sendPositionToFrame(options.id); }, 1000);
                break;

            case "IFRAME_FIXED":
                typeof toggle_fullscreen !== 'undefined' && toggle_fullscreen(); // toggle_fullscreen de SaviaDigital
                var $iFrame = this.getFrame(options.id);
                var iFramePosition = $iFrame.data('iframe-position');
                if (!iFramePosition) {
                    $iFrame.data('iframe-position', $iFrame.css('position'));
                }
                var iFrameHeight = $iFrame.data('iframe-height');
                if (!iFrameHeight) {
                    $iFrame.data('iframe-height', $iFrame.css('height'));
                }
                var $mainContainer=  jQuery('.main .container');
                var $mainContent=  jQuery('.main .main_content');
                var $viewport = jQuery('meta[name="viewport"]');

                if (!$iFrame.hasClass('sm-fullscreen')) {
                    if (!$iFrame.data("style")) {
                        $iFrame.data("style", $iFrame.attr("style"));
                    }
                    $iFrame.data("height", $iFrame.attr("height"));
                    this.fixedIFrame($iFrame);
                    $iFrame.addClass('sm-fullscreen');


                    $iFrame.data('main-container-position', $mainContainer.css('position'));
                    $iFrame.data('main-content-position', $mainContent.css('position'));
                    $mainContainer.css('position', 'initial');
                    $mainContent.css('position', 'initial');
                    if ($viewport.length > 0) {
                        var content = $viewport.attr('content');
                        $iFrame.data('viewport-meta', content);
                        if (content === undefined) {
                            $viewport.attr('content', 'user-scalable=no');
                        } else {
                            $viewport.attr('content', $viewport.attr('content') + ', user-scalable=no');
                        }
                    }
                }
                else {
                    var style = null;
                    if ($iFrame.data("style")) {
                        style = $iFrame.data("style");
                    }
                    $iFrame.attr("style", style);
                    $iFrame.attr('height', $iFrame.data("height"));
                    $iFrame.removeClass('sm-fullscreen');

                    $mainContainer.css('position', $iFrame.data('main-container-position'));
                    $mainContent.css('position', $iFrame.data('main-content-position'));
                    if ($viewport.length > 0) {
                        $viewport.attr('content',  $iFrame.data('viewport-meta'));
                    }
                }
                setTimeout(function () { ApiSM.sendPositionToFrame(options.id); }, 1000);
                break;

            case "IFRAME_REPAINT_FIXED":
                var $iFrame = this.getFrame(options.id);
                this.fixedIFrame($iFrame);
                break;
        }
    },

    getAbsolutePath: function(url){
        var arrPath = url.split('/');
        arrPath.pop();
        var path = arrPath.join('/') + '/';
        path = decodeURIComponent(path);
        return path;
    },

    getCookie: function(name) {
        return decodeURIComponent(document.cookie.replace(new RegExp("(?:(?:^|.*;)\\s*" + name.replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=\\s*([^;]*).*$)|^.*$"), "$1")) || null;
    }
};

if (typeof (ApiSM) === 'undefined') {
    var ApiSM = new api_sm();
    ApiSM.constructor();

    if (typeof (messageListener) === 'undefined') {
        var messageListener = function (event) { ApiSM.receiveMessage(event); };
    }
    // events
    if (window.addEventListener) {  // all browsers except IE before version 9
        window.removeEventListener("message", messageListener, false);
        window.addEventListener("message", messageListener, false);
    }
    else {
        if (window.attachEvent) {   // IE before version 9
            window.attachEvent("onmessage", messageListener);
        }
    }
}

function API_getMyIFRAME(id) {
    return ApiSM.getFrame(id);
}


jQuery(window).ready(function () {

    var API_timer;
    var API_alert = true;
    jQuery(window).off('scroll').on('scroll', function () {
        if (API_alert) {
            ApiSM.alertScrollToFrames();
            API_alert = false;
        }
        clearTimeout(API_timer);
        API_timer = setTimeout(API_refresh, 1000);
        ApiSM.sendPositionToListening();
    });
    jQuery(window).off('resize').on('resize', function () {
        clearTimeout(API_timer);
        API_timer = setTimeout(API_refresh, 50);
    });
    var API_refresh = function () {
        API_alert = true;
        ApiSM.sendPositionToFrames();
        setTimeout(function () { if (API_alert) { ApiSM.sendPositionToFrames(); } }, 1000);
        setTimeout(function () { if (API_alert) { ApiSM.sendPositionToFrames(); } }, 2000);
    };
});