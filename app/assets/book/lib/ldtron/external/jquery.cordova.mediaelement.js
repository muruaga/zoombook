﻿/*!
    Actividades Date: 23/01/2015
    Revision: 1011
 */

// Plugin para reproducir audios y videos en Android

(function ($) {
    // Ventana que carga los plugins de cordova
    var windowAndroidDevice = window.top;

    // Devuelve si esta instalado el plugin device de cordova para su uso
    function isValidPlatformDevice() {
        try {
            return (typeof windowAndroidDevice.device !== "undefined" && windowAndroidDevice.device.platform === 'Android');
        } catch (e) {
            return false;
        }
    }

  // Actualiza el porciento de la barra de progreso y el tiempo en el medialement 
    function mejsUpdateTimeAndProgress(player, media) {
        media.getCurrentPosition(
            function(position) {
                var actualPosition = player.container.data("position");
                if (typeof actualPosition === "undefined" || actualPosition === null) {
                    actualPosition = 0;
                }
                if (position > 0 && position >= actualPosition) {
                    mejsUpdatePositionTimeAndProgress(player, media, position);
                }
            }
        );
    }

    function mejsUpdatePositionTimeAndProgress(player, media, position) {
        player.container.data("position", position);
        if (typeof player.currenttime !== "undefined") {
            player.currenttime.text(mejs.Utility.secondsToTimeCode(position));
        }
        if (typeof player.current !== "undefined") {
            //se define como maximo ancho de la barra de progreso el anncho del total
            player.current.css("max-width", parseInt(typeof player.current.parent().width()) + "px");
            var dur = player.container.data("duration");
            if (dur !== null && position <= dur) {
                var percent = position/dur;
                var totalWidth = player.total.width();
                player.current.css("width", parseInt(totalWidth*percent) + "px");
            }
        }
    }

    function mejsHideVideoControls(player) {
        var deviceChecker = {
            interval: 0,
            counter: 0
        };
        var mediaSrc = player.mediaCordova.mediaSrc;
        deviceChecker.interval = setInterval(function() {
            if (isValidPlatformDevice()) {
                // Se oculta la animacion de loading
                player.layers.find(".mejs-overlay-loading").parent().remove();
                // Se muestra el posterframe
                player.layers.find(".mejs-poster").css("display","block");

                // Si el video se cargan con path relativo se ocultan los controles.
                if (mediaSrc.indexOf("http://") === -1 && mediaSrc.indexOf("https://") === -1) {
                    // Se elimnan los controles porque se usa el reproductor por defecto
                    player.controls.remove();
                    // Se quita el volumen de mediaelemnt  para que no se escuche al inicio
                    player.setMuted(true);
                    // Se oculta el video de medialemente para que no se muestre al inicio
                    player.layers.parent(".mejs-inner").find(".mejs-mediaelement").css("display","none !important");
                } 
                // Si el video se reproducen por http se muestra solo el icono play
                else {
                    player.controls.find(".mejs-button:not(.mejs-playpause-button),.mejs-currenttime-container,.mejs-time-rail,.mejs-time mejs-duration-container").remove();
                }
                clearInterval(deviceChecker.interval);
            }
            else {
                // Maximo para comprobar 10 segundos
                if (++deviceChecker.counter === 100) {
                    clearInterval(deviceChecker.interval);
                }
            }
        }, 100);
    }

    $(document).on("activated", function (event, selector) {
        if (isValidPlatformDevice()) {
            mediaCordovaStopAllAudios(selector);
        }
    });
    

    function mediaCordovaPauseAllAudios(selector) {
        $((selector) ? selector : "audio").each(function () {
            var player = $(this)[0].player;
            if (typeof (player) !== "undefined" && typeof (player.mediaCordova) !== "undefined") {
                player.mediaCordova.pauseMedia();
            }
        });
    }

    function mediaCordovaStopAllAudios(selector) {
        $((selector) ? selector : "audio").each(function () {
            var player = $(this)[0].player;
            if (typeof (player) !== "undefined" && typeof (player.mediaCordova) !== "undefined") {
                player.mediaCordova.stopMedia();
            }
        });
    }


    //  Amplia las funciones de medialement para crear para sincronizar los eventos
    // de medialement los del eventos del objecto Media de cordova que quien reproduce los audios en Android
    $.extend(MediaElementPlayer.prototype, {

        // Para Android se tratan los videos que no son de recursos sino por http,  porque no se caragan en offline
        handleError: function(media) {
            if ( isValidPlatformDevice()) {
                var $mejs = $(media).parents(".mejs-inner");
                var getHttpProtocol = function() {
                    var isHtttp = null;
                    $.each($(media).find("source"), function(){
                        if($(this).attr("src").indexOf("http") === 0) {
                            isHtttp = $(this).attr("src");
                        }
                    });
                    return isHtttp;
                };
                var srcHttp = getHttpProtocol();
                if (srcHttp !== null) {
                    $mejs.find(".me-cannotplay").remove();
                    $mejs.find(".mejs-layers").html('<div class="mejs-overlay mejs-layer mejs-overlay-play" style="width: 100%; height:100%;"><div class="mejs-overlay-button" style="margin-top: -35px"></div></div>');
                    $mejs.find(".mejs-overlay-button").click(function(){
                        windowAndroidDevice.VideoPlayer.play(srcHttp);
                    });
                }
            }
            this.controls.hide();
        },

        buildmediaCordova: function (player, controls, layers, media) {
            // Se comprueba si ya existe instancia del objeto mediaCordova
            if (typeof player.mediaCordova !== "undefined") {return;}

            // Objecto mediaCordova para reproducir audio/video en Android
            player.mediaCordova = {
                mediaID: player.domNode.id, // Identificador único del objecto mediaCordova
                mediaPlayer:null, // Objecto que reproduce el audio/video
                mediaSrc:null, // ruta del audio/video

                timerProgress:0,
                durationCounter:0,
                timerDuration:0,

                isPlaying: false,
                playButton: null,

                init:function() {
                    if ( isValidPlatformDevice()) {

                        this.initMediaPlayer();            
                        this.initControls();

                        player.container.addClass("mediaCordova");

                        media.attr("mediaID", mediaID);
                    }
                },

                initMediaPlayer:function() {
                    var src = media.getAttribute("src");

                    // Si es local creamos la ruta absoluta del contenido de Android a partir de la ruta de la actividad
                    if (src.indexOf("http://") === -1 && src.indexOf("https://") === -1) {
                        var actDirectory = window.location.href.split('/');
                        actDirectory.pop();
                        actDirectory = actDirectory.join('/') + '/';
                        // path relativo del audio/video si es un html de recursos
                        var recursosIndex = actDirectory.indexOf("recursos/");
                        if (recursosIndex >= 0) {
                            actDirectory = actDirectory.substring(0, recursosIndex);
                        }

                        var mediaSrc = media.getAttribute("src").replace("../../","");

                        src = actDirectory + mediaSrc;
                    }

                    this.mediaSrc =src;

                    if (player.isVideo) {
                        this.initMediaVideo();
                    }
                    else {
                        this.initMediaAudio(src);
                    }
                },

                initMediaAudio: function(src) {
                    this.mediaPlayer = new windowAndroidDevice.Media(src,
                            
                        // Release the media resource once finished playing
                        function onSuccess() {
                            player.mediaCordova.mediaPlayer.release();
                        },

                        // Control de errores del audio
                        function onError(e) {
                            if (e.code === 0) {
                                // console.log('mediaCordova erroCode:' + e.code);
                            }
                        },
                        
                        // Control de eventos de ejecucion del audio para actualizar los controles y la bbara de progreso
                        function onStatus(st) {
                            // Se modifca la pinta de medialement cuando se detiene el audio
                            if (st === windowAndroidDevice.Media.MEDIA_STOPPED) {
                                // Se detie la barra de progreso
                                player.mediaCordova.progressStop();
                                // Cambio de estado del boton play a pause
                                player.mediaCordova.changePlayStatus(false);
                                // Se inicia la barra de progreso
                                mejsUpdatePositionTimeAndProgress(player, player.mediaCordova.mediaPlayer, 0);
                                // evento que se escucha en dictados para el control maximo de reproducci0nes.
                                player.container.trigger("ended");
                            }
                        }
                    );  
                },

                initMediaVideo: function() {
                    if (typeof windowAndroidDevice.VideoPlayer === "undefined" || windowAndroidDevice.VideoPlayer === null) {
                        console.log("SM cordova mediaelement API error:  El objecto VideoPlayer no está instalado...");
                        return;
                    }
                    this.mediaPlayer = windowAndroidDevice.VideoPlayer;
                    
                },

                initControls:function() {
                    if (player.isVideo) {
                        this.initVideoControls();
                    }
                    else {
                        this.initAudioControls();
                    }
                },

                initAudioControls:function() {
                    // Se implemeta un nuevo boton play/pause y no el de medialement, para no usar sus eventos
                    var $mePlayButton = player.controls.find(".mejs-playpause-button");
                    this.playButton = $mePlayButton.clone();
                    this.initEvents();

                    $mePlayButton.before(this.playButton);
                    $mePlayButton.hide();

                },

                initVideoControls:function() {
                    var timerVideoOverlayPlay = 0;
                    timerVideoOverlayPlay = setInterval(function() {
                        if (player.container.find(".mejs-overlay-play").length > 0) {
                            // Se implemeta un nuevo boton play/pause y no el de medialement, para no usar sus eventos
                            var $mePlayButton = player.container.find(".mejs-overlay-play");
                            player.mediaCordova.playButton = $mePlayButton.clone();
                            player.mediaCordova.initEvents();

                            $mePlayButton.before(player.mediaCordova.playButton);
                            $mePlayButton.hide();

                            // Se ocultan los controles del video porque no utilizan porque se reproduce con el plugin VideoPlayer
                            mejsHideVideoControls(player);

                            clearInterval(timerVideoOverlayPlay);
                        }
                    }, 100);
                },

                initEvents: function() {
                    // no se utiliza el clic porque la pregunta dictados utiliza este evento para controlar
                    // el numero de veces que se puede reproducir el audio
                    this.playButton.bind("click", function(){
                        player.mediaCordova.mediaClickPlayPause();
                    });
                },

                mediaClickPlayPause: function() {
                    if (this.isPlaying) {
                        this.pauseMedia();
                    }
                    else {
                        this.playMedia();
                    }
                    // Se cambia a pausa cuando es video porque se ejecuta en el reproductor del dispositivo.
                    if (player.isVideo) {
                        this.isPlaying = false;
                    }
                    
                },

                changePlayStatus: function(status) {
                    this.isPlaying = status;
                    if (this.isPlaying) {
                        this.playButton.removeClass("mejs-play").addClass("mejs-pause");
                    }
                    else {
                        this.playButton.removeClass("mejs-pause").addClass("mejs-play");
                    }
                },

                playMedia: function() {
                    if (this.mediaPlayer !== null) {
                        if (player.isVideo) {
                            mediaCordovaPauseAllAudios();
                            this.mediaPlayer.play(this.mediaSrc);
                        }
                        else {
                            mediaCordovaPauseAllAudios('audio:not([id="' + this.mediaID + '"])');
                            this.mediaPlayer.play();
                            this.updateDuration();
                            this.progressStart();
                        }
                        this.changePlayStatus(true);
                    }
                },

                pauseMedia: function() {
                    if (this.mediaPlayer !== null && typeof this.mediaPlayer.pause !=="undefined") {
                        this.changePlayStatus(false);
                        this.mediaPlayer.pause();
                        this.progressStop();
                    }
                },

                stopMedia :function() {
                  if (this.mediaPlayer !== null && typeof this.mediaPlayer.pause !=="undefined") {
                        this.changePlayStatus(false);
                        this.mediaPlayer.stop();
                        this.mediaPlayer.seekTo(0);
                        mejsUpdatePositionTimeAndProgress(player, player.mediaCordova.mediaPlayer, 0);
                        this.progressStop();
                  }      
                },

                progressStart: function() {
                    // Intervalo para ir actualizando la barra de progreso
                    this.timerProgress = setInterval(function() {
                        mejsUpdateTimeAndProgress(player, player.mediaCordova.mediaPlayer);
                    }, 100);
                },

                progressStop: function() {
                    // Se detiene la animacion de la barra progresos
                    clearInterval(this.timerProgress);
                },

                updateDuration: function() {
                    var _self = this;
                    _self.durationCounter = 0;
                    _self.timerDuration = setInterval(function() {
                        _self.durationCounter += 100;
                        if (_self.durationCounter > 2000) {
                            clearInterval(_self.timerDuration);
                        }

                        var actualDuration = player.container.data("duration");
                        if (typeof actualDuration === "undefined" || actualDuration === null) {
                            actualDuration = 0;
                        }

                        var dur = _self.mediaPlayer.getDuration();
                        if (dur > 0 && dur >= actualDuration) {
                            if (dur === actualDuration) {
                                clearInterval(_self.timerDuration);
                            }

                            actualDuration = dur;
                            // Se actualiza el tiempo total de medialementjs
                            if (typeof player.durationD !== "undefined") {
                                player.durationD.text(mejs.Utility.secondsToTimeCode(actualDuration));
                            }
                            player.container.data("duration", actualDuration);

                        }
                    }, 100);
                    
                }
            };
            
            player.mediaCordova.init();

        }
    });
    
})(mejs.$);