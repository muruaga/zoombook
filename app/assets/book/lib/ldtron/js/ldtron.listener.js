'use strict';

var LDTronListener = function() {
  this.starListener();
};

LDTronListener.prototype = {
  starListener: function() {
    var me = this;
    function executeMessage (event) {
      var jsonEventData = JSON.parse(event.data);
      switch (jsonEventData.action) {
        case 'position':
          var reader =  window.ldReader;
          if (reader) {
            reader.iFrameData = jsonEventData;
            /*$(document).trigger('repaintDialogLDTron');
            $(document).trigger('scrollEndLDTron');*/
          }
          break;

        case "scrollStart":
          //$(document).trigger('scrollStartLDTron');
          break;

        case 'heightFrameDone':
          //$(document).trigger('heightFrameDoneLDTron');
          break;
      }
    }
    // Para todos los navegadores excepto versiones anteriores de IE9
    if (window.addEventListener) {
      window.removeEventListener("message", executeMessage, false);
      window.addEventListener("message", executeMessage, false);
    }
    // Para versiones anterioeres a IE9
    else if (window.attachEvent) {
      window.attachEvent("onmessage", executeMessage);
    }
  }
};
// Inciar listener
$(window).ready(function() {
  new LDTronListener();
});