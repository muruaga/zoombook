'use strict';

(function ($) {

  $(document).on('documentLoaded', function () {
      ZoomBookEditMode.init();
  });

  var ZoomBookEditMode = {
    isEditing: false,

    init: function() {
      this.viewer = readerControl.docViewer;
      this.createEditButton();
      this.eventEditButton();
    },

    createEditButton: function() {
      var htmlEditButton =
        '<div id="edit-button" class="showing">' +
          '<div id="edit-ball"></div>' +
          '<div id="edit-icons">' +
            '<div id="edit-top-icon" class="tooltip" title="Modo edición"><i data-feather="edit-2"></i></div>' +
            '<div id="edit-bottom-icon" class="tooltip" title="Modo presentación"><i data-feather="monitor"></i></div>' +
          '</div>' +
          '<div id="edit-notification">Modo edición activado</div>' +
          '<div id="show-notification">Modo presentación activado</div>' +
        '</div>';
      $('body').append(htmlEditButton);
      feather.replace();
    },

    eventEditButton: function() {
      $('#edit-button').click(this.toggleEditButton);
    },

    showEditButton: function() {
      $('#edit-button').show('slow');
    },

    toggleEditButton: function() {
      if (ZoomBookEditMode.isEditing) {
        $('#edit-button').removeClass('editing').addClass('showing');
        anime({
            targets: '#edit-ball',
            translateY: [0, 50],
            easing: 'easeInCubic',
            duration: 400,
            backgroundColor: '#BF003B'
        });
        $('body').removeClass('edit-show');
      } else {
        $('#edit-button').addClass('editing').removeClass('showing');
        anime({
            targets: '#edit-ball',
            translateY: [50, 0],
            easing: 'easeInCubic',
            duration: 400,
            backgroundColor: '#58A5FF'
        });
        $('body').addClass('edit-show');
        $(document).trigger('toggleOptionTeacher', true);
      }
      ZoomBookEditMode.isEditing = !ZoomBookEditMode.isEditing;
      $(document).trigger('toggleEditing', ZoomBookEditMode.isEditing);
    },

    hideEditButton: function() {
      $('#edit-button').hide('slow');
    },

  };
})(jQuery);