(function f($) {
  $(document).ready(function () {
    var ZoomBookContextBar = {
      init: function () {
        var me = this;
        me.createBar();
        $(document).on('ContextShow', function (event, context) {
          me.showBar();
        });
      },

      createBar: function () {
        var htmlBar =
          '<div id="ContextBar">' +
            '<div id="ButtonCloseContext"><i data-feather="arrow-left"></i> Volver</div>' +
            '<div class="title text-center">' +
              '6.º EP Ciencias de la Naturaleza Madrid. Savia' +
              '<div class="sub-title">Unidad 1. el cuerpo humano y la relación</div>' +
              '<div class="sub-title pg">Página 7</div>' +
            '</div>' +
          '</div>';
        this.$bar = $(htmlBar);
        $('body').append(this.$bar);

        feather.replace();
        this.eventBarButtons();
      },

      showBar: function () {
        this.$bar.show();
      },

      hideBar: function () {
        this.$bar.hide();
        readerControl.docViewer.setFitMode(readerControl.docViewer.FitMode.FitPage);
      },

      eventBarButtons: function () {
        var me = this;
        $('#ButtonCloseContext').on('click', function () {
          $(document).trigger('ContextClose');
          me.hideBar();
        });
      }
    };

    ZoomBookContextBar.init();
  });
})(jQuery);