'use strict';

(function ($) {

  window.showMode = 'document';

  $(document).on('ContextShow', function (event, context) {
      ZoomBookContextActivities.init();
      ZoomBookContextActivities.showContextActivitiesIcons(context);
  });
  $(document).on('ContextClose', function (event) {
      ZoomBookContextActivities.hideContextActivities();
  });
  $(document).on('ContextChange', function (event, context) {
      ZoomBookContextActivities.hideContextActivities();
      if (context.activities.length) {
        ZoomBookContextActivities.showContextActivitiesIcons(context);
      }
  });

  $(document).on('execOptionUpload', function () {
    if (typeof window.resourcesDialog === 'object') {
      window.resourcesDialog.dialog('close');
    }
  });

  var ZoomBookContextActivities = {
    active: false,

    init: function() {
      this.viewer = readerControl.docViewer;
    },

    clickContextActivity: function(event, type) {
      if (type === 'solutions') {
        window.resourcesDialog.dialog('close');
      }

      var $activity = $(event.target);
      if ($activity[0].getAttribute('data-activity') === null) {
        $activity = $activity.parents('[data-activity]');
      }
      var solutions = $activity.data('solutions');
      if (typeof solutions === 'string') {
        $(document).trigger('openSolutions', $activity.data('solutions'));
      }
      var url = $activity.data('url');
      if (typeof url === 'string') {
        window.open(url,'_blank');
      }
    },

    openResourcesDialog: function(filters) {
      window.resourcesDialog = this.$resourcesPanel.dialog({
        modal: true,
        width: '50%',
        maxWidth: '600',
        open: function() {
          var $titlebar = $(this).parent().find('.ui-dialog-titlebar');
          $titlebar.find('.ui-icon').replaceWith('<i data-feather="x"></i>');
          var select = '<select>';
            $()
            select += '</select>';
          $titlebar.append();
          feather.replace();
        }
      });
    },

    clickRemoveActivities: function(activities, index, context) {
      activities.splice(index, 1);
      if (window.contextShown) {
        $(document).trigger('ContextChange', context);
      }
    },

    showContextActivitiesIcons: function(context) {
      this.$htmlActivities = $('<div id="context-activities"></div>');
      $('body').append(this.$htmlActivities);

      this.$htmlActivitiesContainer = $('<div id="context-activities-container"></div>');
      this.$htmlToggleActivitiesButton = $('<div id="context-activities-button-list"><div id="btnActivities" class="btn-activity"><i data-feather="chevron-up"></i></div></div>');
      this.$htmlActivities.append(this.$htmlActivitiesContainer);
      this.$htmlActivities.append(this.$htmlToggleActivitiesButton);

      this.$htmlActivitiesContainer.html('');
      this.resoucesCount = [];

      if (typeof(context.activities) !== 'undefined') {
          var htmlActivity = '<div class="btn-activity {0}" data-type="{0}" title="{1}"><i {2}></i></div>';
          for (var i = 0; i < context.activities.length; i++) {
              var type = context.activities[i].type, origin = context.activities[i].origin, icon = '', typeTitle= '';
              switch (type) {
                  case 'video':
                      icon = 'data-feather="play"';
                      typeTitle = 'Video';
                      break;
                  case 'interactive':
                      icon = 'data-feather="loader"';
                      typeTitle = 'Interactivo';
                      break;
                  case 'image':
                      icon = 'data-feather="image"';
                      typeTitle = 'Imagen';
                      break;
                  case 'file':
                      icon = 'data-feather="download"';
                      typeTitle = 'Actividad';
                      break;
                  case 'solutions':
                      icon = 'data-icons="solucionario.svg" style="background-image: url(&quot;../sm/icons/solucionario.svg&quot;);transform: scale(1) translateY(-5px); width: 15px;"';
                      typeTitle = 'Solucionario';
                      break;
              }

              if (typeof this.resoucesCount[origin+type] === 'undefined') {
                this.resoucesCount[origin+type] = [context.activities[i]];
              }
              else {
                this.resoucesCount[origin+type].push(context.activities[i]);
              }

              var $activity, btnClass = '';
              if (context.activities[i].type === 'solutions') {
                btnClass = 'btn-solutions';
              }
              if (context.activities[i].origin === 'sm-teacher') {
                btnClass = 'btn-sm-teacher';
              }
              if (context.activities[i].origin === 'teacher') {
                btnClass = 'btn-teacher'
              }

              if (this.resoucesCount[origin+type].length === 1) {
                $activity = $(htmlActivity.replace(/\{0\}/g, type).replace('{1}', typeTitle).replace('{2}', icon));
                $activity.addClass(btnClass);
                this.$htmlActivitiesContainer.append($activity);
              }
              else {
                this.$htmlActivitiesContainer.find('.btn-activity.' + btnClass).addClass('many').attr('resource-count', this.resoucesCount[origin+type].length);
              }
          }

          feather.replace();

          // Eventos
          var me = this;
          this.$htmlActivitiesContainer.find('.btn-activity').click(function () {
            me.showResourcesDialog(context, $(this).attr('data-type'));
          });
          this.$htmlToggleActivitiesButton.find('.btn-activity').click(function () {
            me.showResourcesDialog(context);
          });

      }
    },

    showResourcesDialog: function(context, typeFilter) {
      var me = this;
      this.$resourcesPanel = $(
        '<div id="ResourcesPanel" title="Recursos asociados">' +
        '<div class="container"></div>' +
        '</div>'
      );
      $('body').append(this.$resourcesPanel);

      me.$resoucesList = me.$resourcesPanel.find('.container');

      if (typeof(context.activities) !== 'undefined') {
        var htmlActivity = '<div class="btn-activity {0}"><i {1}></i></div>';
        for (var i = 0; i < context.activities.length; i++) {
          var type = context.activities[i].type, origin = context.activities[i].origin, icon = '', typeTitle= '';
          if (typeof typeFilter === 'undefined' || type === typeFilter) {
            switch (type) {
              case 'video':
                icon = 'data-feather="play"';
                typeTitle = 'Video';
                break;
              case 'interactive':
                icon = 'data-feather="loader"';
                typeTitle = 'Interactivo';
                break;
              case 'image':
                icon = 'data-feather="image"';
                typeTitle = 'Imagen';
                break;
              case 'file':
                icon = 'data-feather="download"';
                typeTitle = 'Actividad';
                break;
              case 'solutions':
                icon = 'data-icons="solucionario.svg" style="background-image: url(&quot;../sm/icons/solucionario.svg&quot;);transform: scale(1) translateY(-5px); width: 15px;"';
                typeTitle = 'Solucionario';
                break;
            }

            var $activity, btnClass = '';
            if (context.activities[i].type === 'solutions') {
              btnClass = 'btn-solutions';
            }
            if (context.activities[i].origin === 'sm-teacher') {
              btnClass = 'btn-sm-teacher';
            }
            if (context.activities[i].origin === 'teacher') {
              btnClass = 'btn-teacher'
            }
            $activity = $(htmlActivity.replace('{0}', type).replace('{1}', icon));
            $activity.addClass(btnClass);



            var $resoucesRowElem = $('<div class="row"></div>');

            var $resoucesColIcon = $('<div class="col icon"></div>');

            var $clone = $activity.clone();
            $resoucesColIcon.append($clone);
            $resoucesRowElem.append($resoucesColIcon);

            var $resoucesColText = $('<div class="col text"></div>');
            $resoucesColText.append('<div class="col-type">' + typeTitle + '</div>');
            $resoucesColText.append('<div class="col-title">' + context.activities[i].title + '</div>');
            $resoucesRowElem.append($resoucesColText);
            var $resoucesColButtons = $('<div class="col buttons"></div>');
            if (context.activities[i].origin === 'teacher') {
              $resoucesColButtons.append('<span class="delete-resource"><i data-feather="trash-2"></i></span>');
            }
            $resoucesColButtons.append('<span class="settings-resource"><i data-feather="settings"></i></span>');
            $resoucesRowElem.append($resoucesColButtons);

            if (context.activities[i].type === 'solutions') {
              $clone.data('solutions', JSON.stringify(context.activities[i].solutions));
              $resoucesColText.data('solutions', JSON.stringify(context.activities[i].solutions));
            } else {
              $clone.data('url', context.activities[i].url);
              $resoucesColText.data('url', context.activities[i].url);
            }
            $clone.attr('data-activity', '');
            $resoucesColText.attr('data-activity', '');

            me.$resoucesList.append($resoucesRowElem);

            $resoucesRowElem.find('[data-activity]').click(function (event) {
              me.clickContextActivity(event, type);
            });
            }
        }

        feather.replace();
        // Eventos
        var me = this;
        this.$htmlToggleActivitiesButton.find('.btn-activity').click(function () {
          me.openResourcesDialog();
        });
        this.$resoucesList.find('.delete-resource').click(function (event) {
          me.clickRemoveActivities(context.activities, $(event.target).parents('.row').index(), context);
        });

        this.openResourcesDialog();
      }
    },

    hideContextActivities: function() {
        $('.btn-activity').addClass('btn-destroying').removeClass('btn-activity');
        if (this.$htmlActivities) {
          this.$htmlActivities.remove();
        }
        if (this.$resourcesPanel) {
          this.$resourcesPanel.remove();
        }
    },

  };
})(jQuery);