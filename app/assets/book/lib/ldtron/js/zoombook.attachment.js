'use strict';

(function ($) {

  $(document).on('documentLoaded', function () {
    ZoomAttachment.init();
  });

  $(document).on('toggleEditing', function (event, isEditing) {
    if (!isEditing) {
      ZoomAttachment.dragContextMode = false;
      $(document).trigger('DragContextMode', ZoomAttachment.dragContextMode);
      $('#ZoomAttachment').removeClass('active-button');
    }
  });

  var ZoomAttachment = {
    init: function () {
      this.createInterface();
      this.eventInterface();
    },

    createInterface: function () {
      this.$buttonTextTools =
        $('<div id="ZoomAttachment" class="svg-button">' +
            '<div class="active-option"><i data-feather="x"></i></div>' +
          '</div>');

      $('body').append(this.$buttonTextTools);
    },

    dragContextMode: false,

    eventInterface: function () {
      var me = this;
      $('#ZoomAttachment').on('click',function () {
        me.dragContextMode = !me.dragContextMode;
        $(document).trigger('DragContextMode', me.dragContextMode);
        if (me.dragContextMode) {
          $(this).addClass('active-button');
        }
        else {
          $(this).removeClass('active-button');
        }
      });
    }
  }
})(jQuery);