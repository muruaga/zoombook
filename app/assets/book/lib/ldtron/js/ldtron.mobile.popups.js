'use strict';

var transition_popups = 'fade';

var LDTronPopup = {
  openHTMLContent: function(params) {
    var popup = '<div data-role="popup" id="LDTronPopup" data-overlay-theme="b" data-theme="a" data-tolerance="15,15" class="ui-content">';
    popup += '<a href="#" data-rel="back" data-role="button" data-theme="a" data-iconpos="notext" class="closeBtn ui-btn-right ui-btn ui-icon-delete ui-btn-icon-notext ui-corner-all ui-mini"></a>';
    popup += '<iframe src="' + params.src + '" width="' + params.popupW + '" height="' + params.popupH + '" seamless frameBorder="0"></iframe></div>';

    var $popup = $(popup).appendTo("body");
    $popup.popup({
      transition: transition_popups,
      afteropen: function() {
        $(document).trigger('openDialogLDTron', 'html');
      },
      afterclose: function () {
        $(document).trigger('closeDialogLDTron');
        $popup.remove();
      }
    });
    $popup.popup('open');
  },

  openImgContent: function(params) {
    var style = '';
    if (params.maxWidth && params.maxWidth < params.popupW) {
      style = 'style="max-width:' + params.maxWidth + 'px !important"';
    }
    var popup = '<div data-role="popup" id="LDTronPopup" data-overlay-theme="b" data-theme="a" data-tolerance="15,15" class="ui-content" ' + style + '>';
    popup += '<a href="#" data-rel="back" data-role="button" data-theme="a" data-icon="delete" data-iconpos="notext" class="closeBtn ui-btn-right ui-btn ui-icon-delete ui-btn-icon-notext ui-corner-all ui-mini"></a>';
    popup += '<img src="' + params.src + '" border="0" ' + style + '/>';
    popup += '</div>';
    var $popup = $(popup).appendTo("body");
    $popup.find('img').load(function () {
      // ipad safari fix to center the popup
      $(this).parent().height( $(this).parent().height() );
      $popup.popup({
        transition: transition_popups,
        afteropen: function () {
          $(document).trigger('openDialogLDTron', 'image');
        },
        afterclose: function () {
          $(document).trigger('closeDialogLDTron');
          $popup.remove();
        }
      });
      $popup.popup('open');
    });
  },

  openVideoContent: function(params) {
    var srcMp4 = params.src;
    var srcWebm = params.src2;

    if (srcMp4.indexOf(".mp4") == -1 || srcWebm.indexOf(".webm") == -1) {
      srcWebm = params.src;
      srcMp4 = params.src2;
    }

    var popup_w = parseInt(params.popupW);
    var popup_h = parseInt(params.popupH);

    var popup = '<div data-role="popup" id="LDTronPopup" data-overlay-theme="b" data-theme="a" data-tolerance="15,15" class="ui-content">';
    popup += '<a href="#" data-rel="back" data-role="button" data-theme="a" data-icon="delete" data-iconpos="notext" class="closeBtn ui-btn-right ui-btn ui-icon-delete ui-btn-icon-notext ui-corner-all ui-mini"></a>';

    popup += '<video id="videoContent" width="' + popup_w + '" height="' + popup_h + '" controls="controls" preload="none">' +
        '<source type="video/webm" src="' + srcWebm + '"></source>' +
        '<source type="video/mp4" src="' + srcMp4 + '"></source>' +
        '</video>';
    popup += '</div>';

    var $popup = $(popup).appendTo('body');
    $('#videoContent').css({ 'width': popup_w + 'px', 'height': popup_h + 'px' });

    setTimeout(function() {
      var player = null;
      $popup.popup({
        transition: transition_popups,
        afteropen: function() {
          $(document).trigger('openDialogLDTron', 'video');
          player = new MediaElementPlayer('#videoContent', {
            features: ['playpause', 'progress', 'duration', 'current', 'volume', 'fullscreen', 'mediaCordova'],
            defaultVideoWidth: popup_w,
            defaultVideoHeight: popup_h,
            pauseOtherPlayers: true,
            success: function (mediaElement, domObject) {
              $('.mejs-container').css('overflow', 'hidden');
              $('#videoContent').css({ 'width': popup_w + 'px', 'height': popup_h + 'px' });
              mediaElement.load();
              mediaElement.play();
            }
          });
        },
        afterclose: function () {
          $(document).trigger('closeDialogLDTron');
          if (player) {
            player.pause();
            player.remove();
          }
          $popup.remove();
        }
      });
      $popup.popup('open');
    }, 500);
  },

  openAudioContent: function(params, bt) {
    var srcMp3 = params.src;
    var srcOgg = params.src2;
    if (srcMp3.indexOf(".mp3") === -1 || srcOgg.indexOf(".ogg") === -1) {
      srcMp3 = params.src2;
      srcOgg = params.src2;
    }
    var popup = '<div data-role="popup" id="popupAudio" data-overlay-theme="b" data-theme="a" data-tolerance="15,15" class="ui-content">';
    popup += '<a href="#" data-rel="back" data-role="button" data-theme="a" data-icon="delete" data-iconpos="notext" class="closeBtn ui-btn-right ui-btn ui-icon-delete ui-btn-icon-notext ui-corner-all ui-mini"></a>';
    popup += '<audio id="audioContent">' +
            '<source id="audio_player_ogg" src="' + srcOgg + '"  type="audio/ogg" />' +
            '<source id="audio_player_mp3" src="' + srcMp3 + '"  type="audio/mpeg" />' +
            '</audio>';
    popup += '</div>';

    var $popup = $(popup).appendTo('body');
    var player = null;
    var playerWidth = 150, playerHeihgt = 30;

    setTimeout(function() {
      $popup.popup({
        transition: transition_popups,
        positionTo: bt,
        afteropen: function () {
          var $popup = $(this).parent();
          var $closeBtn = $popup.find('.closeBtn');
          var offsetRight = parseInt($(this).parent().offset().left, 10) + parseInt(playerWidth, 10);
          if (offsetRight > $(window).width()) {
            $popup.css('left', $(this).parent().offset().left - playerWidth - $closeBtn.outerWidth(true)*2);
          }
          $(document).trigger('openDialogLDTron', 'audio');
          player = new MediaElementPlayer('#audioContent', {
            features: ['playpause', 'progress', 'duration', 'current', 'mediaCordova'],
            audioWidth: playerWidth,
            audioHeight: playerHeihgt,
            pauseOtherPlayers: true,
            success: function(mediaElement, domObject) {
              $('.mejs-container').css('overflow', 'hidden');
              $('#audioContent').css({ 'width': playerWidth + 'px', 'height': playerHeihgt + 'px' });
              mediaElement.play();
            }
          });
        },
        afterclose: function () {
          $(document).trigger('closeDialogLDTron');
          if (player) {
            player.pause();
            player.remove();
          }
          $popup.remove();
        }
      });
      $popup.popup('open');
    }, 500);
  },

  showInfo: function(title, msg) {
    var popup = '<div data-role="popup" id="LDTronPopup" data-overlay-theme="b" data-theme="a" data-tolerance="15,15" class="ui-content popupInfo">';
        popup += '<a href="#" data-rel="back" data-role="button" data-theme="a" data-icon="delete" data-iconpos="notext" class="closeBtn ui-btn-right ui-btn ui-icon-delete ui-btn-icon-notext ui-corner-all ui-mini"></a>';
        popup += '<p>' + msg + '</p>'
        popup += '<div>';
    var $popup = $(popup).appendTo('body');
    $popup.popup({
      transition: transition_popups,
      afteropen: function() {
        $(document).trigger('openDialogLDTron', 'info');
      },
      afterclose: function () {
        $(document).trigger('closeDialogLDTron');
        $popup.remove();
      }
    });
    $popup.popup('open');
  },

   showConfirm: function(opts) {
    var popup = '<div data-role="popup" id="LDTronPopup" data-overlay-theme="b" data-theme="a" data-tolerance="15,15" class="ui-content popupInfo confirm">';
        popup += '<p class="message">' + opts.mess + '</p>'
        popup += '<div class="botonera">'
        popup += '<a  href="#" id="btconfirm" data-role="button" data-inline="true"  data-theme="a" class="ui-link ui-btn ui-btn-a ui-btn-inline ui-shadow ui-corner-all" role="button">' + opts.btyes + '</a>';
        popup += '<a  href="#" id="btdismiss" data-role="button" data-inline="true"  data-theme="a" class="ui-link ui-btn ui-btn-a ui-btn-inline ui-shadow ui-corner-all" role="button">' + opts.btno + '</a>';
        popup += '</div></div>';
    var $popup = $(popup).appendTo('body');
    $popup.find("#btconfirm").on('click', function(){
      opts.callback.call();
      $popup.popup('close');
    });
    $popup.find("#btdismiss").on('click', function(){
      $popup.popup('close');
    });
    $popup.popup({
      transition: transition_popups,
      afteropen: function() {
        $(document).trigger('openDialogLDTron', 'info');
      },
      afterclose: function () {
        $(document).trigger('closeDialogLDTron');
        $popup.remove();
      }
    });
    $popup.popup('open');
  }

};

// Inciar listener
$(window).ready(function() {
  $(document).on('LDTronPopupInfo', function(event, options){
    var title = options.title;
    var msg = options.msg;
    LDTronPopup.showInfo(title, msg);
  });

   $(document).on('LDTronPopupConfirm', function(event, options){
    var title = options.title;
    var msg = options.msg;
    LDTronPopup.showConfirm(options);
  });

});