var ZoomBookUtil = {
  animationClick: function($element, animation, callback){
    $element.addClass('animated ' + animation);
    window.setTimeout( function(){
      $element.removeClass('animated ' + animation);
      if (typeof callback === 'function') {
        callback();
      }
    }, 150);
  }
};