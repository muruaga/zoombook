'use strict';

var LDTronPageContext = {
  init: function() {
    var $pageContext = $('#page-context');
    var htmlContext = '<div id="context-bar"></div>' +
        '<div id="navigate-left"><i data-feather="arrow-left"></i></div>' +
        '<div id="navigate-right"><i data-feather="arrow-right"></i></div>' +
        '<div id="ind-activities"></div>' +
        '<div id="edit-mode"></div>' +
        '<div id="layers"></div>';
    $pageContext.append($(htmlContext));
    feather.replace();


  },
  show: function(idContext) {
    $('#page-context').show();
  }
};