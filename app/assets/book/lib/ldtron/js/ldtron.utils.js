'use strict';

var LDTronUtils = {
  isMobileDevice: function() {
    return (navigator.userAgent.match(/Android/i) || navigator.userAgent.match(/webOS/i)
          || navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPod/i)
          || navigator.userAgent.match(/iPad/i) || navigator.userAgent.match(/Touch/i)
          || navigator.userAgent.match(/IEMobile/i) || navigator.userAgent.match(/Silk/i));
  },

  isIOS: function() {
    return navigator.userAgent.match(/(iPad|iPhone|iPod)/i)
  },
  
  getAbsolutePath: function(url){
    var arrPath = url.split('/');
    arrPath.pop();
    return arrPath.join('/') + '/';
  },

  getRealPageByIndex: function(pageIndex, initialPage) {
    return (pageIndex + initialPage);
  },

  getRealPageByNumber: function(pageNumber, initialPage) {
    return this.getRealPageByIndex(pageNumber, initialPage) - 1;
  },

  resize: function(idAPI) {
    var mess = '{"id": "' + idAPI + '" , "action":"getPosition"}';
    window.top.postMessage(mess, "*");
  },

  fullScreen: function(idAPI) {
    var mess = '{"id": "' + idAPI + '" , "action":"fullscreen_mode", "mode":"IFRAME_ABSOLUTE"}';
    window.top.postMessage(mess, "*");
    var $ldtron = $('.ldtron');
    var isZoom = $ldtron.hasClass('zoom');
    if (isZoom) {
      $ldtron.removeClass('zoom');
    }
    else {
      $ldtron.addClass('zoom');
    }
  },

  fixHeightParent: function(idAPI, heightFrame) {
    var mess = '{"id": "' + idAPI + '" , "action":"heightFrame", "alto":' + heightFrame + '}';
    window.top.postMessage(mess, "*");
  },

  i18n: function(lang, callback) {
    if (lang === null) {
      lang = 'es';
    };
    lang = lang.toLowerCase();
    i18n.setLng(lang, function() {
      $('body').i18n();
      // Traducciones de los textos en las clases
      Tools.FreeTextCreateTool.prototype.initialText = i18n.t('annotations.insertTextHere');

      if (callback) {
        callback();
      }
    });
  },

  scrollTop: function(idAPI, yIncrement) {
    var mess = '{"id": "' + idAPI + '", "action":"scrollTop", "yIncrement":' + yIncrement + '}';
    window.top.postMessage(mess, "*");
  },

  scrollY: function(idAPI, yPos) {
    var mess = '{"id": "' + idAPI + '", "action":"scrollY", "yPosition":' + yPos + '}';
    window.top.postMessage(mess, "*");
  },

  existProfesorIcons: function() {
    var exist = false;
    if (contentData && contentData.length > 0) {
      $.each(contentData, function (i, icon) {
        if (icon.person === '1') {
          return exist = true;
        }
      });
    }
    return exist;
  },

  getAnnotationIconClass : function (annotation) {
    var nodeType = $('<div class="node-type sm-icons">');
    var classType = '';
    if (annotation instanceof Annotations.StickyAnnotation) {
      classType = 'sm-icons-bocadillo-lleno';
    }
    if (annotation instanceof Annotations.FreeTextAnnotation) {
      classType = 'sm-icons-caja-texto';
    }
    if (annotation instanceof Annotations.EllipseAnnotation) {
      classType = 'sm-icons-forma-circulo';
    }
    if (annotation instanceof Annotations.RectangleAnnotation) {
      classType = 'sm-icons-forma';
    }
    if (annotation instanceof Annotations.TextHighlightAnnotation) {
      classType = 'sm-icons-texto_resaltado';
    }
    if (annotation instanceof Annotations.TextUnderlineAnnotation) {
      classType = 'sm-icons-texto_subrayado';
    }
    if (annotation instanceof Annotations.TextStrikeoutAnnotation) {
      classType = 'sm-icons-texto_tachado';
    }
    if (annotation instanceof Annotations.TextSquigglyAnnotation) {
      classType = 'sm-icons-texto_curvas';
    }
    return classType;
  }
};