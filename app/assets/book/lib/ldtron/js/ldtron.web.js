'use strict';

// LDTron para Web
var LDTronWeb = function LDWebReader(idAPI, options) {
  this.idAPI = idAPI;
  this.options = options;
};

// Metodos LDTron para Web
LDTronWeb.prototype = {
  initialize: function() {
    var me = this;
    var viewer = readerControl.docViewer;

    // Instancias DOM
    me.$ldtron = $('.ldtron');
    me.$control = $('#control');
    me.$tabs = $('#tabs');
    me.$annotationEdit = $('#annotationEdit');
    me.$sidePanel = $('#sidePanel');
    me.$thumbnailView = $('#thumbnailView');
    me.$fullSearchView = $('#fullSearchView');
    me.$bookmarkView = $('#bookmarkView');
    me.$notesPanel = $('#notesPanel');
    me.$notesPanelWrapper = $('#notesPanelWrapper');
    me.$searchBox = $('#searchBox');
    me.$relaseInfo = $('.ReleaseInfo');
    me.$noteOptions = $('#noteOptions');
    me.$noteOptionsButton = $('#noteOptionsButton');
    me.$toggleNotesButton = $('.toggleNotesButton');
    me.$EmptyPanelMessage = $('.EmptyPanelMessage');
    me.$noteOptionsCloseButton = $('#noteOptionsCloseButton');

    readerControl.thumbContainerWidth = 130;
    readerControl.thumbContainerHeight = 180;

    // Clase del visor LDTron Web
    me.$ldtron.addClass('web');

    // Metodos nativos personalizados
    readerControl = $.extend(readerControl, {
      initBookmarkView: me.initBookmarkViewLDTron, // Índices
      searchText: me.searchTextLDTron // Búqueda
    });

    $(document).on('documentLoaded', function() {
      readerControl.setShowSideWindow(false, false);
      me.initThumbnailsPageNumber(); // Numeración de página según initialPage en los thumnails
      me.initToolbarPageNumbers(); // Numeración de página según initialPage en la barra de herramientas
      me.initLocale(me.options.lang); // Idioma del visor
      me.changeBackgroundColor(me.options.color);

      if (!readerControl.enableAnnotations) {
        $('#overflowPaintTools, #overflowButton').hide();
      }

      // Si tenemos exportación de Anotaciones (sólo Perú por defecto), mostramos los iconos
      if (me.options.expAnnots) {
        $('#loadAnnots').show();
        $('#saveAnnots').show();
      }

      me.$relaseInfo = $('.ReleaseInfo');
      me.$EmptyPanelMessage = $('.EmptyPanelMessage');
      me.repaintControls(me.iFrameData);
      me.resizeSidePanelByID(me.iFrameData, me.actualPanelID);
      me.resizeNotesPanels(me.iFrameData);

      $(document).trigger('ldtronLoaded');

      // Si no está dentro de un Iframe se dehabilita el fullscreen
      if (typeof me.iFrameData !== 'object') {
        $('#fullScreenButton').addClass('disabled').unbind('click');
      }

      setTimeout(function () {
        readerControl.setLayoutMode(me.DisplayModes.Facing);
        readerControl.docViewer.setFitMode(readerControl.docViewer.FitMode.FitPage);
      },100);
    });

    readerControl.docViewer.on('displayModeUpdated', function() {
      var $liLayoutMode = $('[data-layout-mode=Facing]');
      if (me.options.showCoverPage &&  $liLayoutMode.length > 0) {
        var  isCoverFacing = (me.getLayoutMode() === me.DisplayModes.CoverFacing);
        $liLayoutMode
          .attr('data-layout-mode', 'CoverFacing')
          .attr('data-layout-icon', 'cover_page')
          .addClass((isCoverFacing) ? 'active': '')
          .i18n();
      }
    });

    readerControl.docViewer.getAnnotationManager().on('annotationChanged annotationSelected', function() {
      setTimeout(function () {
        me.fixIFrameHeight();
      }, 500);
    });


    $(document).on('notesPanelBecomingVisible', function() {
      setTimeout(function () {
        me.resizeNotesPanels(me.iFrameData);
      }, 500);
    });


    me.$pageNumberBox = $('#pageNumberBox');
    me.$pageNumberBox.replaceWith(
      '<input type="text" pattern="[0-9]*" name="pageNumberBox" id="pageNumberBox">'
    );
    me.$pageNumberBox.keyup(function(e) {
      if (e.which === 13) {
        me.updatePageNumber(this.value);
      }
    });

    viewer.on('pageNumberUpdated', function(e, pageNumber) {
      me.updateBookmarksView(pageNumber);
    });

    viewer.on('pageComplete', function (e, pageIndex) {
      //me.appendIconsContentData(pageIndex);
    });


    var $fullScreenButton=$('#fullScreenButton');
    $fullScreenButton.replaceWith(
      '<span id="fullScreenButton" class="sm-icons sm-icons-pantalla-completa" data-i18n="[title]controlbar.fullScreen" title="Pantalla completa"></span>'
    );

    $fullScreenButton.click(function () {
      me.toolsFadeOut();
      var $bt = $(this);

      if ($bt.hasClass('sm-icons-pantalla-completa')){
        $bt.removeClass('sm-icons-pantalla-completa').addClass('sm-icons-pantalla-ventana');
      }else{
        $bt.removeClass('sm-icons-pantalla-ventana').addClass('sm-icons-pantalla-completa');
      }

      setTimeout(function() {
        LDTronUtils.fullScreen(me.idAPI);
      }, 300);
    });


    $(document).on('click', '#btFitWidth', function(){
      readerControl.docViewer.setFitMode(readerControl.docViewer.FitMode.FitWidth);
    });

    var calcZoomLevelWithBounds = function(zoom) {
      if (zoom <= me.MIN_ZOOM) {
        zoom = me.MIN_ZOOM;
      } else if (zoom > me.MAX_ZOOM) {
        zoom = me.MAX_ZOOM;
      }
      return zoom;
    };

    var $viewerElement = $('#DocumentViewer');
    var toolbarOffset = me.$control.outerHeight();
    $viewerElement.unbind('mousewheel');
    $viewerElement.on('mousewheel', function(event, delta) {
      event.preventDefault();
      if (event.ctrlKey || event.altKey) {
        var sidePanelWidth = readerControl.sidePanelVisible() ? me.$sidePanel.width() : 0;
        if (delta < 0) {
          viewer.zoomToMouse(calcZoomLevelWithBounds(readerControl.getZoomLevel() * 0.8), sidePanelWidth, toolbarOffset);
        } else {
          viewer.zoomToMouse(calcZoomLevelWithBounds(readerControl.getZoomLevel() * 1.25), sidePanelWidth, toolbarOffset);
        }
        return;
      }

      // Scroll down/up del visor para web
      LDTronUtils.scrollTop(me.idAPI, (delta < 0) ? me.options.yScrollTopIncrement : me.options.yScrollTopIncrement * -1);
    });

    $('#prevPage').replaceWith(
      '<span id="prevPageLDTron" class="sm-icons sm-icons-flecha_atras_circulo" data-i18n="[title]controlbar.previousPage;" title="Previous Page"></span>'
    );

    $('#prevPageLDTron').on('click', function() {
      var currentPage = viewer.getCurrentPage();
      if (currentPage > 1) {
        --currentPage;
        var displayMode = viewer.getDisplayModeManager().getDisplayMode().mode;
        if (currentPage > 1 && (displayMode === me.DisplayModes.Facing || displayMode === me.DisplayModes.CoverFacing)) {
          --currentPage;
          if (currentPage === 2 ) {
            currentPage = 1;
          }
        }
        viewer.setCurrentPage(currentPage);
      }
    });

    $('.width-option').click(function(event){
          var layout =  $(event.target).attr("data-layout-mode");
          var layoutMode = me.DisplayModes.Single;
          if (typeof(layout) !== 'undefined'){
              if (layout === 'Facing'){
                  layoutMode = me.DisplayModes.Facing;
              }
              if (layout === 'CoverFacing'){
                  layoutMode = me.DisplayModes.CoverFacing;
              }
          }
          readerControl.setLayoutMode(layoutMode);
      });


    $('#nextPage').replaceWith(
      '<span id="nextPageLDTron" class="sm-icons sm-icons-flecha_adelante_circulo" data-i18n="[title]controlbar.nextPage;" title="Next Page"></span>'
    );

    $('#nextPageLDTron').on('click', function() {
      var displayMode = viewer.getDisplayModeManager().getDisplayMode().mode;
      var currentPage = viewer.getCurrentPage();
      var pageCount = viewer.getPageCount();
      if (currentPage < pageCount) {
        ++currentPage;
        if (displayMode === me.DisplayModes.Facing ) {
          ++currentPage;
        }
        else if (displayMode === me.DisplayModes.CoverFacing && currentPage > 2) {
          ++currentPage;
        }
        if (currentPage > pageCount) {
          currentPage = pageCount;
        }
        viewer.setCurrentPage(currentPage);
      }
    });

    $(document).on('scrollStartLDTron', function() {
      var iFrameData = me.iFrameData;
      if (typeof iFrameData === 'object') {
        var topTools =  parseInt(iFrameData.topFrame) - parseInt(iFrameData.scrollTop);
        if (topTools < 0) {
          $(document).trigger('hiddeLDTronTools');
          me.toolsFadeOut();
        }
      }
    });

    $(document).on('scrollEndLDTron', function() {
      me.repaintControls(me.iFrameData);
      me.resizeSidePanelByID(me.iFrameData, me.actualPanelID);
      me.resizeNotesPanels(me.iFrameData);
    });

    $(document).on('openDialogLDTron', function() {
      me.setDialogOpened(true);
    });
    $(document).on('closeDialogLDTron', function() {
      me.setDialogOpened(false);
    });

    me.$noteOptionsButton.on('click', function() {
      setTimeout(function () {
        me.resizeNotesPanels(me.iFrameData);
      }, 500);
    });

    me.$noteOptionsCloseButton.on('click', function() {
      setTimeout(function () {
        me.resizeNotesPanels(me.iFrameData);
      }, 500);
      me.$notesPanelWrapper.removeClass('noteOptionsVisible');
    });

    // Alto barra de herramientas
    me.headerHeight = parseInt(me.$control.outerHeight(true));

    // Posicionamiento del la busqueda completa
    $(document).on('click', '.searchResultLine', function (event) {
      if (readerControl.clickedSearchResult !== -1) {
        $('#searchResultLine' + readerControl.clickedSearchResult).removeClass('ui-state-active');
        readerControl.clickedSearchResult = -1;
      }
      readerControl.clickedSearchResult = $(this).data('data').searchResultLineId;
      $(this).addClass('ui-state-active');
      var result = $(this).data("data").result;
      $(document).trigger('displaySearchResult', result, me.headerHeight);
    });

    // Se indica si esta o no visible el panel izquierdo
    $('#toggleSidePanel').on('click', function() {
      if (readerControl.getShowSideWindow()) {
        me.$sidePanel.addClass('visible');
        me.resizeSidePanelByID(me.iFrameData, (me.actualPanelID)?me.actualPanelID:"thumbnailView");
      }
      else {
        me.$sidePanel.removeClass('visible');
      }
    });

    // Inicializar valor de input para que
    $('#loadAnnots').on('click', function() {
       $("#loadAnnotsFile").val('');
    });

    // Carga de anotaciones
    $('#loadAnnotsFile').on('change', function() {
      // Cargar de fichero y hacer un synchronizedAnnotations(newAnnots);
      // Primero controlar si el nombre del fichero se parece a me.options.pdfFile... !!
      var file = this.files[0];
      var reader = new FileReader();
      reader.addEventListener('load', function (e) {
        // UPDATE 12/07/2017: Ana Castro pide quitar esta comprobación, de manera
        // que se puedan incorporar las anotaciones que hagan falta independientemente
        // del nombre... Lo dejamos comentado por si acaso...
        //if (file.name.toLowerCase() === me.options.pdfFile.toLowerCase() + ".txt") {
          readerControl.storage.syncronizedAnnotations(e.target.result);
        /*
        } else {
          alert(i18n.t("notesPanel.errorAnnots"));
        }*/
      }, false);

      if (file) {
        reader.readAsText(file);
      }
    });

    // Guardado de anotaciones, vía FileSaver.js
    $('#saveAnnots').on('click', function() {
      var am = readerControl.docViewer.getAnnotationManager();
      var annotsViewer = encodeURIComponent(am.exportAnnotations());
      var blob = new Blob([annotsViewer], {type: 'text/plain;charset=utf-8'});
      saveAs(blob, me.options.pdfFile + '.txt');
    });

    $('#noteOptions .ui-button').on('click', function() {
      var $button = $(this);
      if ($button.attr('id') === 'timeSortButton') {
        me.$notesPanel.addClass('timeSort');
      }
      else {
        me.$notesPanel.removeClass('timeSort');
      }
      me.fixIFrameHeight();
    });
    me.$noteOptionsButton.on('click', function() {
      me.$notesPanelWrapper.addClass('noteOptionsVisible');
    });

    $('#deletePageAnnotationsButton').on('click', function(ev) {

      var pages = [];
      var currentPage = parseInt(viewer.getCurrentPage(),10);
      pages.push(currentPage);
      var messageConfirm = i18n.t('annotations.deleteAllConfirm');

      if (readerControl.getLayoutMode() === me.DisplayModes.Facing){

        if (currentPage % 2 === 1){
            pages.push(currentPage + 1);    // current page is on the left side
        }else{
            pages.push(currentPage - 1);    // current page is on the right side
        }
        messageConfirm = i18n.t('annotations.deleteAllConfirm2');
      }

      if (readerControl.getLayoutMode() === me.DisplayModes.CoverFacing){
        if (currentPage % 2 === 0){
            pages.push(currentPage + 1);    // current page is on the left side
        }
        else{
            pages.push(currentPage - 1);    // current page is on the right side
        }
        messageConfirm = i18n.t('annotations.deleteAllConfirm2');
      }

      if (me.confirm(messageConfirm, 
                      i18n.t('notesPanel.deleteAll'),
                      i18n.t('notesPanel.cancel'),
                      ev.target,
                      function(){
                        deletePageAnnotations(pages);
                      })
      );

      $(document).trigger('hiddeLDTronTools');

    });

    me.$tabs.on('tabsactivate', function( event, ui ) {
      me.resizeSidePanelByID(me.iFrameData, ui.newPanel.children(0).attr('id'));
    });
    me.$tabs.on('tabsafterupdateheight', function() {
      me.resizeSidePanelByID(me.iFrameData, me.actualPanelID);
    });
    $(document).on('heightFrameDoneLDTron', function() {
      me.resizeSidePanelByID(me.iFrameData, me.actualPanelID);
    });

    // Constantes Modo de vista
    me.DisplayModes = window.CoreControls.DisplayModes;

    // Opciones de pintado del visor
    me.customizeViewer();

    // Control de scroll
    me.initScrollEvents();

    // Inicio estado de dialogo
    me.setDialogOpened(false);

    // Se ajusta tamaño del iframe del visor
    me.resize();
  },
  confirm: function(mess, btyes, btno, bt, callback){
    var me = this;
    var divModal = '<div id="dialog-confirm">';
    divModal +=     '<p><span class="ui-icon ui-icon-alert" style="float:left; margin:12px 12px 20px 0;"></span>'+ mess +'</p>';
    divModal +=   '</div>';

    divModal = $(divModal).appendTo('body');

    var buttonsConfirm = [];
    buttonsConfirm.push({
        text: btyes,
        click : function() {
          $( this ).dialog('close');
          callback.call();
        }
    });
    buttonsConfirm.push({
        text: btno,
         click : function() {
          $( this ).dialog('close');
        }
    });

    divModal.dialog({
      resizable: false,
      height: "auto",
      width: 400,
      modal: true,
      buttons: buttonsConfirm,
      dialogClass: "sm-confirm",
      position: {
        my: "center",
        at: "center",
        of: window
      },
      open: function(event, ui) {
        $(event.target).parent().css('position', 'fixed');
        $(event.target).parent().css('top', (window.outerHeight/2 - $(event.target).parent().height() + parseInt(me.iFrameData.scrollTop)) + 'px');
      },
      close: function(){
        divModal.dialog("destroy"); 
        divModal.remove();
      }
    });

  },
  repaintControls: function(iFrameData) {
    if (typeof iFrameData !== 'object') { return; }

    var me = this;
    var topTools = '';
    var top = parseInt(iFrameData.topFrame) - parseInt(iFrameData.scrollTop);
    if (top < 0) {
      topTools = Math.abs(top) + 'px';
    }
    me.$control.css('top', topTools);
    me.$tabs.css('top', topTools);
    me.toolsFadeIn();
  },
  toolsFadeIn: function() {
    var me = this;
    setTimeout(function(){
      me.$control.removeClass('hiddenVisibilityAnimated');

      if (readerControl.sidePanelVisible()){
          me.$sidePanel.fadeIn();
      }
      
      if (me.searchBoxIsFocus) {
        me.$searchBox.focus();
      }
    }, 10);
  },
  toolsFadeOut: function() {
    var me = this;
    setTimeout(function(){
      me.searchBoxIsFocus = me.$searchBox.is(":focus");
      me.$control.addClass('hiddenVisibilityAnimated');
      if (readerControl.sidePanelVisible()){
        me.$sidePanel.fadeOut();
      }
    },10);
  },
  resizeSidePanelByID: function(iFrameData, idPanel) {
    var me = this;
    me.actualPanelID = idPanel;
    switch (idPanel) {
      case 'thumbnailView':
        me.resizeSidePanel(iFrameData, me.$thumbnailView);
      break;
      case 'bookmarkView':
        me.resizeSidePanel(iFrameData, me.$bookmarkView);
      break;
      case 'personalView':
        me.resizeSidePanel(iFrameData, me.$personalView);
      break;
      case 'searchView':
        me.resizeSearchSidePanels(iFrameData);
      break;
    }
  },
  /**
   * Ajusta el alto del resultado de busqueda para que se muestre completa
   * @param  {Object} iFrameData Datos de dimensones del Iframe y window
   */
  resizeSearchSidePanels: function(iFrameData) {

    var sidePanelHeight = this.$notesPanelWrapper.outerHeight(true);
    if (sidePanelHeight > 0) {
      this.$sidePanel.height(sidePanelHeight);
    }
    var posTop = 0;
    if (typeof iFrameData === 'object') {
      posTop = iFrameData.topFrame - iFrameData.scrollTop;
    }

    if (posTop < 0) {
      posTop = 0;
    }
    var $tabPanels = $('.tab-panel-item-fixed');
    var tabsHeight = $tabPanels.outerHeight(true) * $tabPanels.length;
    var windowHeight = (typeof iFrameData === 'object') ?  iFrameData.height : window.innerHeight;
    var panelHeight = windowHeight - tabsHeight - posTop - this.$control.outerHeight(true);
    var diffY = sidePanelHeight - (this.$fullSearchView.offset().top + panelHeight);
    if (diffY < 0) {
      panelHeight += diffY;
    }
    panelHeight -= 5;
    this.$fullSearchView.height(panelHeight - 50);
  },

  /**
   * Ajusta el alto del resultado de busqueda para que se muestre completa
   * @param  {Object} iFrameData Datos de dimensones del Iframe y window
   * @param  {Element} $panel Panel...
   */
  resizeSidePanel: function(iFrameData, $panel) {
    if (typeof $panel === 'undefined') { return; }

    var sidePanelHeight = this.$notesPanelWrapper.outerHeight(true);
    if (sidePanelHeight > 0) {
      this.$sidePanel.height(sidePanelHeight);
    }
    var posTop = 0;
    if (typeof iFrameData === 'object') {
      posTop = iFrameData.topFrame - iFrameData.scrollTop;
    }
    if (posTop < 0) {
      posTop = 0;
    }
    var windowHeight = (typeof iFrameData === 'object') ?  iFrameData.height : window.innerHeight;
    var panelHeight = windowHeight -  posTop - this.$control.outerHeight(true) - $("#tabMenu").outerHeight(true);
    var diffY = sidePanelHeight - ($panel.offset().top + panelHeight);
    if (diffY < 0) {
      panelHeight += diffY;
    }
    panelHeight -= 5;
    $panel.height(panelHeight);
  },



  resizeNotesPanels: function (iFrameData) {
    if (this.$notesPanel.hasClass('timeSort')) {

      // Datos de la windows
      var windowHeight = (typeof iFrameData === 'object') ? parseInt(iFrameData.height) : window.innerHeight;
      var windowTop = (typeof iFrameData === 'object') ? parseInt(iFrameData.topFrame) : 0;
      var windowScrollTop = (typeof iFrameData === 'object') ? parseInt(iFrameData.scrollTop) : $(window).scrollTop();

      // Option de arriba.
      var isVisibleNoteOptions  = (this.$noteOptions[0].style.display === "block");
      var $topOption = (isVisibleNoteOptions) ?  this.$noteOptions: this.$noteOptionsButton;
      var bottomTopOption = $topOption.offset().top + $topOption.outerHeight(true);

      var posTop = 0;
      if (typeof iFrameData === 'object') {
        posTop = windowTop - windowScrollTop - this.$control.outerHeight(true);
      }
      if (posTop < 0) {
        posTop = 0;
      }

      var panelHeight = windowHeight -  posTop - this.$control.outerHeight(true);
      if (this.$relaseInfo.length) {
        var releasetop = this.$relaseInfo.offset().top + windowTop;
        if (releasetop < (windowHeight + windowScrollTop)) {
          panelHeight -= this.$relaseInfo.outerHeight(true) * 2;
        }
      }

      var panelTop = $topOption.outerHeight(true);
      if (windowScrollTop - windowTop + this.$control.outerHeight(true) > bottomTopOption) {
        panelTop = windowScrollTop - windowTop
      }
      else {
        panelHeight -= bottomTopOption;
      }
      if (panelTop < 0) {
        panelTop = Math.abs(panelTop) ;
      }

      this.$notesPanel.css({
        'height' : panelHeight + 'px',
        'margin-top': panelTop + 'px'
      });

      // icono de la barra e icono de vacío

      var top = windowTop - windowScrollTop;
      if (top < 0) {
        top = 0;
      }
      panelHeight = windowHeight - top  - this.$control.outerHeight(true);
      var windowTopMiddle = parseInt(panelHeight/2 + windowScrollTop);

      var toogleTopBehaviours = windowTop;
      if (top !== 0) {
        toogleTopBehaviours = 0;
      }
      if (top > 0) {
        toogleTopBehaviours += windowScrollTop;
      }
      var toogleTop = windowTopMiddle - toogleTopBehaviours - this.$toggleNotesButton.outerHeight()/2;
      this.$toggleNotesButton.css('top', parseInt(toogleTop) + 'px');

      var emptyPanelMessageTop = windowTopMiddle + top - this.$EmptyPanelMessage.outerHeight()/2 - this.$control.outerHeight(true);
      this.$EmptyPanelMessage.css('top', parseInt(emptyPanelMessageTop) + 'px');
    }
    else {
      this.$notesPanel.css({
        'height' : '',
        'margin-top': ''
      });
    }
  },

  customizeViewer: function() {
    var me = this;

    // Boton de cambio de modo de pagina si por configuración se deshabita twoPages
    if (this.options.twoPages === '0') {
      $('#layoutModeDropDown').parent().remove();
    }

    // menu zoom (Acrobat like)
    context.attach('#btConfigWidth', $('#menuConfigWidth')[0], {translateY: 25, translateX: -125});

    // Carpeta personal
    if (tablePersonal !== null && tablePersonal.length > 0) {
      this.initPersonalFolders(tablePersonal);
    }else{
       $("#tabs").tabs("disable", "#tabs-4");
    }

    // Se adiciona el titulo
    this.appendTitle(this.options.pdfVisibleTitle);

    
    // siempre vació por desision de diseño 
    this.appendLogo(this.options.sello);
   
    // Personalizadas las herramientas de texto y pintado
    this.buildToolsOptions();

    // Opcion de filtro de iconos del profesor
    me.showProfesorIcons = LDTronUtils.existProfesorIcons();
    if (me.showProfesorIcons) {
      me.createButtonFilterIcons();
    }
    

    $(document).on('documentLoaded', function() {
      var maxWidth = me.$control.outerWidth(true) - parseInt($('#toggleSidePanel').outerWidth(true)) - parseInt($('.right-aligned').outerWidth(true)) - 20;
      $('#toggleTitle').css('max-width', maxWidth).addClass('visible');
    });
  },

  buildToolsOptions: function() {

    var menuPaint = new LDTronWebMenu('menuPaint', 'AnnotationCreateFreeHand');
    menuPaint.initialize();

    var menuText = new LDTronWebMenu('menuText', 'AnnotationCreateTextUnderline');
    menuText.initialize();

    var menuSelect = new LDTronWebMenu('menuSelect', 'Pan');
    menuSelect.initialize();

    // cambiar la herramienta seleccionada
    $(document).on('click', '.navbar span[data-toolmode]', function(event) {

      $('.navbar span[data-toolmode]').removeClass('active');
      var btSel = $(event.target);
      btSel.addClass('active');

      readerControl.setToolMode(btSel.attr('data-toolmode'));
      
    });

    readerControl.setToolMode(PDFTron.WebViewer.ToolMode.Pan);

  },

  isModePanSelected: function() {
    return (readerControl.getToolMode() === PDFTron.WebViewer.ToolMode.Pan);
  },

  isNoteSelected: function() {
    var annot = window.ControlUtils.getSelectedAnnotation();
    return (annot !== null);
  },

  initScrollEvents:  function() {
    var me = this;
    // Cursor inicio del scroll
    var startScroll = {x:0, y:0};
    var $docViewer = $('#DocumentViewer');
    // Inicio del cursor
    $docViewer.on('mousedown', function(event) {
      // Scroll si esta activada la herramienta mano
      if (me.isModePanSelected() && !me.isNoteSelected() && !me.isDialogOpened()) {
        startScroll = {x: event.clientX, y: event.clientY};
        $docViewer.on('mouseup mouseleave mousemove', function handler(event) {
          // Detener el scroll
          if (event.type === 'mouseleave' || event.type === 'mouseup') {
            me.setScrolling(false);
            $docViewer.off('mouseup mouseleave mousemove', handler);
          }
          else {
            // Se inicia el scroll a partir del 5to pixels de moviendo verticalmente
            var diffDelta = 5;
            var deltaY = startScroll.y - event.clientY;
            if (Math.abs(deltaY) > diffDelta) {
              me.setScrolling(true);
            }
            // Ejecucion del scroll
            if (me.isScrolling()) {
              startScroll.y = event.clientY + deltaY;
              LDTronUtils.scrollTop(me.idAPI, deltaY);
            }
          }
        });
      }
    });
  },

  initPersonalFolders: function(tablePersonal) {
    var me = this;
    /*var $tabMenu = this.$tabs.find('#tabMenu');
    var tabIndex = $tabMenu.find('li').length + 1;
    var $tabButton = $('<li><a href="#tabs-' + tabIndex + '"><span data-i18n="[title]sidepanel.personal" class="glyphicons personal"></span></a></li>');
    $tabButton.appendTo($tabMenu);*/
    //$personalTabs.appendTo(this.$tabs);

    var $personalTabs= $('#tabs-4');

    me.$personalView = $personalTabs.find('#personalView');
    $.each(tablePersonal, function (index, elem) {
      var $personaDocWrapper = $("<div class='personaDocWrapper bookmarkWrapper' id=personaDocWrapper" + index + "></div>");
      var $personaDoc = $("<span id='personaDoc" + index + "'></span>");
      $personaDoc
        .html(elem.title)
        .click(function() {
          var content = {type: 'document',src: elem.doc}
          if ((typeof(elem.isSM) !== 'undefined') && (elem.isSM)){
            content.type = 'sm';
          }
          LDTronContent.show(me, content, this);
        })
        .appendTo($personaDocWrapper);
      $personaDocWrapper.appendTo(me.$personalView);
    });

    this.$tabs.tabs("refresh");
  },

  setScrolling: function(value) {
    this.scrolling = value;
  },

  isScrolling: function() {
    return this.scrolling;
  },

  setDialogOpened: function(value) {
    this.dialogOpened = value;
  },

  isDialogOpened: function() {
    return this.dialogOpened;
  },

  appendTitle: function (title) {
    $('.left-aligned').append('<span id="toggleTitle" class="ui-label" title="' + title + '">' +  title + '</span>');
  },

  appendLogo: function(sello) {
    // se añade solo un div de 10 px para despegar del borde el ultimo icono 
    $('.right-aligned').append('<div style="display:inline-block; width:10px;"/>');
  },

  createButtonFilterIcons: function() {
    var me = this;
    /*var $layoutModes = $('#layoutModes');
    if ($layoutModes.length === 0) return;*/

    me.$buttonFilterIcons = $('<div id="buttonFilterIcons" class="isShown"></div>');
    var $spanButton = $('<span></span>').attr({
      'class': 'sm-icons sm-icons-profe',
      'data-i18n': '[title]controlbar.teacherIcons.iconsHide'
    })
    .appendTo(me.$buttonFilterIcons)
    .i18n();

    me.$buttonFilterIcons.click(function() {
      me.showProfesorIcons = !me.showProfesorIcons;
      $('.pageContainer').each(function() {
        var pageIndex = parseInt($(this).attr('id').split('pageContainer')[1]);
        me.appendIconsContentData(pageIndex);
      });

      if (me.showProfesorIcons) {
        $(this).addClass('isShown');
        $spanButton.attr('data-i18n', '[title]controlbar.teacherIcons.iconsHide').i18n();
      }
      else {
        $(this).removeClass('isShown');
        $spanButton.attr('data-i18n', '[title]controlbar.teacherIcons.iconsShow').i18n();
      }
    }).i18n();

    $('#filterIconsPlaceHolder').append(me.$buttonFilterIcons);

  },
  // Posicionamiento del la busqueda de frases
  searchTextLDTron: function(pattern, mode) {
    if (pattern !== '') {
      var me = this;
      mode = mode | readerControl.docViewer.SearchMode.e_page_stop | readerControl.docViewer.SearchMode.e_highlight;
      readerControl.docViewer.textSearchInit(pattern, mode, false,
        function(result) {
          if (result.resultCode === XODText.ResultCode.e_done) {
            alert(i18n.t("endOfDocument"));
          }
          else if(result.resultCode === XODText.ResultCode.e_found) {
            $(document).trigger('displaySearchResult', result, me.headerHeight);
          }
        });
    }
  },

  initThumbnailsPageNumber: function() {
    var me = this;
    var $divNumber;
    $('.thumbContainer div:last-child').each(function(index, divNumber) {
      $divNumber = $(divNumber);
      $divNumber.html(LDTronUtils.getRealPageByNumber(index + 1, me.options.initialPage));
    });
  },

  initToolbarPageNumbers: function() {
    var viewer = readerControl.docViewer;
    var pageIndex = viewer.getCurrentPage() - 1;
    var pageBook = pageIndex + this.options.initialPage;
    var totalPages = LDTronUtils.getRealPageByNumber(viewer.getPageCount(), this.options.initialPage);
    $('#totalPages').text('/' + totalPages);
    this.$pageNumberBox.val(pageBook);
  },

  initLocale: function(lang) {
    LDTronUtils.i18n(lang, function() {
      $('.noteContainer .noteSubject').each(function() {
        var $annot = $(this);
        $annot.html($annot.html().replace('FreeText', i18n.t('annotations.types.freetext')));
      });
    });
  },

  // Si tenemos color de fondo, aplicamos...
  changeBackgroundColor: function(color) {
    if (color) {
      $('#DocumentViewer')[0].style.setProperty( 'background', color, 'important' );
      // $('#DocumentViewer').css('background', color);
    }
  },

  fixIFrameHeight: function() {
    var me = this;
    if (me.iFrameData) {
      setTimeout(function() {
        var viewer = readerControl.docViewer;
        var iframeHeight = $('#viewer').outerHeight(true) + me.$control.outerHeight(true) + 35;
        var minHeight = me.iFrameData.height - me.iFrameData.topFrame;
        if (iframeHeight < minHeight) {
          iframeHeight = minHeight;
        }

        var notesHeight = 0;
        if (!me.$notesPanel.hasClass('timeSort')) {
          me.$notesPanel.find('.panelElement ').each(function () {
            var bottom = this.getBoundingClientRect().bottom;
            if (notesHeight < bottom) {
              notesHeight = bottom;
              notesHeight += 40;
            }
          });
          notesHeight += me.$relaseInfo.outerHeight(true);
        }

        if (iframeHeight < notesHeight) {
          iframeHeight = notesHeight;
        }


        if (me.viewerHeight !== iframeHeight) {
          me.viewerHeight = iframeHeight;
          LDTronUtils.fixHeightParent(me.idAPI, me.viewerHeight);

          var $docPad = $('#docpad');
          if($docPad.length > 0) {
            var docPadHeight = 0;
            var $docViewer = $('#DocumentViewer');
            var $pageSection = $('#pageSection' + (viewer.getCurrentPage() - 1));
            if ($docViewer.width() > $pageSection.outerWidth(true)){
              var pageHeight = parseFloat($pageSection.outerHeight(true));
              var docHeight = me.iFrameData.height - me.iFrameData.topFrame - pageHeight - me.$control.outerHeight(true);
              if (docHeight > 0) {
                docPadHeight = Math.floor((docHeight)/2.0);
              }
            }
            $docPad.css('margin-bottom', docPadHeight + 'px');
          }
        }
      },100);
    }
  },

  updatePageNumber: function(pageNumber) {
    var viewer = readerControl.docViewer;
    var number = parseInt(pageNumber, 10) - this.options.initialPage + 1;
    if (isNaN(number) || number < 1) {
      number = 1;
    }
    else if (number > viewer.getPageCount()) {
      number = viewer.getPageCount();
    }
    this.$pageNumberBox.val(LDTronUtils.getRealPageByNumber(viewer.getCurrentPage(), this.options.initialPage));
    viewer.setCurrentPage(number);
  },

  initBookmarkViewLDTron: function() {
    var ldReader = window.ldReader;
    if (tableOutline !== null && tableOutline.length === 0) {
       $("#tabs").tabs("disable", "#tabs-2");
    }
    else {
      var viewer = readerControl.docViewer;
      // Se ordena los indices por el campo page
      for (var i = 0; i < tableOutline.length; i++) {
        var bookmarPage = parseInt(tableOutline[i].page) - parseInt(ldReader.options.initialPage) + 1;
        var idBookmark = LDTronUtils.getRealPageByNumber(bookmarPage, ldReader.options.initialPage);
        var wrapper = $("<div class='bookmarkWrapper' id=bookmarkWrapper" + idBookmark + "></div>");
        var bookmark = $("<span id='bookmark" + idBookmark + "'></span>");
        bookmark
          .html(tableOutline[i].title)
          .attr('page', bookmarPage)
          .attr('pageNum', tableOutline[i].page)
          .click(function() {
            viewer.setCurrentPage($(this).attr('page'));
          });
        bookmark.appendTo(wrapper);
        wrapper.appendTo(ldReader.$bookmarkView);
      }
      ldReader.updateBookmarksView(1);
    }
  },

  updateBookmarksView: function(pageNumber) {
    $(".bookmarkWrapper").removeClass("selected");
    var realPageNumber = LDTronUtils.getRealPageByNumber(pageNumber, this.options.initialPage);
    $("#bookmarkWrapper" + realPageNumber).addClass("selected");
    this.$pageNumberBox.val(realPageNumber);
  },

  appendIconsContentData: function(pageIndex) {
    var me  = this;
    if (contentData && contentData.length > 0) {
      var $div = $('#pageContainer' + pageIndex);
      var viewer = readerControl.docViewer;
      var zoom = viewer.getZoom();
      $div.find(".smIcon").remove();
      $.each(contentData, function (i, icon) {
        var pagePos = pageIndex + 1;
        if (icon.page === pagePos && (icon.person === '0' || (icon.person === '1' && me.showProfesorIcons))) {
          var image = new Image();
          image.onload = function () {
            $div.append(image);

            var size = 30;
            var variacion_z = 0.75;
            var widthImg = parseInt(size * (zoom/variacion_z));
            var leftImg = parseInt(icon.x * (zoom/variacion_z));
            var topImg = parseInt(icon.y * (zoom/variacion_z));

            $(image).addClass('smIcon').css({
              'position': 'absolute',
              'cursor': 'pointer',
              'left': leftImg,
              'top': topImg,
              'width': widthImg + 'px',
              'height': widthImg + 'px',
              'z-index': 400
            }).on('click', function(evt) {
              evt.stopPropagation();
              LDTronContent.show(me, icon, this);
            });
          };
          var person = (parseInt(icon.person) == 0) ?'a' :'p';
          image.src = me.options.path + 'lib/ldtron/imgs/' + icon.icon + '_' + person + '.png';
          image.title = icon.title;
        }
      });
    }
  },

  resize: function() {
    LDTronUtils.resize(this.idAPI);
  },

  repaint: function() {
    this.updatePageNumber(this.$pageNumberBox.val());
  },

  getClientPosition: function() {
    var mess = '{"id": "' + this.idAPI + '", "action":"getPosition"}';
    window.top.postMessage(mess, "*");
  },

  getLayoutMode: function() {
    return readerControl.docViewer.getDisplayModeManager().getDisplayMode().mode;
  },

  setLayoutMode: function(mode) {
    var toMode = this.DisplayModes.Single;
    switch (mode) {
      case this.DisplayModes.Single:
      case this.DisplayModes.Facing:
      case this.DisplayModes.CoverFacing:
        toMode = mode;
      break;
      default:
        var defaultDoblePagina = (this.options.twoPages === '1' && this.options.defaultPages === "2");
        toMode = (defaultDoblePagina)? (this.options.showCoverPage) ? this.DisplayModes.CoverFacing : this.DisplayModes.Facing : this.DisplayModes.Single;
      break;
    }
    readerControl.setLayoutMode(toMode);
  }
};