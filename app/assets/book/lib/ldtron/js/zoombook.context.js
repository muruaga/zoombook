(function ($) {
  var actualStatus = false;
  var shoContextActive = false;
  $(document).on('toggleOptionContexts', function (event, status) {
    actualStatus = status;
    if (actualStatus) {
      $('.context').addClass('active');
    }
    else {
      $('.context').removeClass('active');
    }
  });

  $(document).on('ContextShow', function (event) {
    shoContextActive = true;
    $('.content').removeClass('active');
    $('body').addClass('context-show');
  });

  $(document).on('ContextClose', function (event) {
    shoContextActive = false;
    $('body').removeClass('context-show');
  });

  var isEditing = false;
  $(document).on('toggleEditing', function (event, _isEditing) {
    isEditing = _isEditing;
  });
  
  $(document).on('DragContextMode', function(event, active) {
    if (active) {
      $(document).trigger('toggleOptionContexts', false);
      $('.context').removeClass('active');
      $('body').addClass('drag-mode');
    } else {
      $('body').removeClass('drag-mode');
    }
  });

  $(document).on('documentLoaded', function () {

    // Carga de los contextos de las página que se están mostrando
    readerControl.docViewer.on('pageComplete', function (event, pageIndex) {
      ZoomBookContext.appendContexts(pageIndex);
    });
  });

  $(document).on('ZoomBookNextContext', function () {
    ZoomBookContext.nextContext();
  });

  $(document).on('ZoomBookPrevContext', function () {
    ZoomBookContext.prevContext();
  });

  $(document).on('ContextClose', function (event, context) {
    window.contextShown = null;
  });

  window.contextShown = null;
  var ZoomBookContext = {

    appendContexts: function(pageIndex) {
      var me = this;
      if (contextData && contextData.length > 0) {
        var pagePos = pageIndex + 1;
        var $divPage = $('#pageContainer' + pageIndex);
        $divPage.find('.context').remove();
        $.each(contextData, function (index, context) {
          context.index = index;
          if (context.page === pagePos) {
            context.pageIndex = pageIndex;
            me.appendContext($divPage, context);
          }
        });
        me.eventsContexts(me);
      }
    },

    eventsContexts: function(me) {
      $('.context-sketch').click(function(e) {
        e.stopPropagation();
        $('#tool-sketch').removeClass('circle-set');
        $(document).trigger('ContextShow', context);
        me.showContextElem($(e.target).parents('.context'));
        $('body').removeClass('click-sketch');
        setTimeout(function() {
          $(document).trigger('showSketch');
          $(document).trigger('closeAttachMenu');
        }, 900);
      });

    },

    appendContext: function($divPage, context) {
      var me = this;
      var viewer = readerControl.docViewer;
      var zoom = viewer.getZoom();

      var width = parseInt(context.width * (zoom));
      var height = parseInt(context.height * (zoom));
      var left = parseInt(context.x * (zoom));
      var top = parseInt(context.y * (zoom));

      var $divContext = $('<div class="context"></div>').css({
        'left': left,
        'top': top,
        'width': width + 'px',
        'height': height + 'px'
      })
      .attr('id', context.id)
      .data('context', context)
      .on('click', function (event) {
        if (window.contextShown === null && !isEditing) {
          $(document).trigger('ContextShow', context);
          me.showContextElem($(event.target));
        }
      });
      if (me.showPageAreas) {
        $divContext.addClass('isShown');
      }

      var $divContextButtons = $('<ul class="context-buttons"></ul>');

      var teacherActivities = jQuery.grep(context.activities, function( activitie, index ) {
        return ( activitie.origin === 'teacher' );
      });
      if (teacherActivities.length) {
        $divContextButtons.append('<li class="context-button teacher">' + teacherActivities.length + '</li>');
      }

      var smTeacherActivities = jQuery.grep(context.activities, function( activitie, index ) {
        return ( activitie.origin === 'sm-teacher' );
      });
      if (smTeacherActivities.length) {
        $divContextButtons.append('<li class="context-button sm-teacher">' + smTeacherActivities.length + '</li>');
      }

      var smActivities = jQuery.grep(context.activities, function( activitie, index ) {
        return ( activitie.origin === 'sm' &&  activitie.type !== 'solutions');
      });
      if (smActivities.length) {
        $divContextButtons.append('<li class="context-button sm">' + smActivities.length + '</li>');
      }

      var solutionsActivities = jQuery.grep(context.activities, function( activitie, index ) {
        return ( activitie.type === 'solutions' );
      });

      if (solutionsActivities.length) {
        $divContextButtons.append('<li class="context-button solutions"><i data-icons="solucionario.svg" style="width: 14px; transform: scale(0.8); background-image: url(&quot;../sm/icons/solucionario.svg&quot;);"></i></li>');
      }

      $divContext.append($divContextButtons)

      if (actualStatus && !shoContextActive) {
        $divContext.addClass('active');
      }

      $divPage.append($divContext);

      // Drag & Drop
      var $divInput = $('<div class="context-input">' +
                          '<input class="context-file" type="file" name="file_upload" data-multiple-caption="{count} ficheros seleccionados" multiple />\n' +
                          '<label>Haz clic o arrastra el recurso aquí para añadirlo a este área.</label>\n' +
                        '</div>');
      var $divSketch = $('<div class="context-sketch">' +
                          '<label>Haz clic para editar la pizarra de este área.</label>\n' +
                        '</div>');
      var $divLoading = $('<div class="context-loading"><p>Cargando...</p></div>');
      var $divError = $('<div class="context-success"><p>¡Recurso añadido!</p></div>');

      $divContext.append($divInput);
      $divContext.append($divSketch);
      $divContext.append($divLoading);
      $divContext.append($divError);

      $divInput.find('input').fileupload({
        dropZone: $divInput,
        url: 'http://demo.grupo-sm.es/service/upload.php',
        dataType: 'json',
        acceptFileTypes: /(\.|\/)(gif|jpe?g|png|mp3|ogg|mp4|mov|avi|zip)$/i,
        add: function (e, data) {
          $divContext.find('.context-loading').show();
          data.submit();
        },
        done: function (e, data) {

          if (data.result.error_msg) {
            $divContext.find('.context-loading').hide();
            alert(data.result.error_msg);
          }
          else {
            var idContext = $divContext.attr('id');
            var pageIndex = 0;
            for (var i = 0; i < contextData.length; i++) {
              if (contextData[i].id === idContext) {
                pageIndex = contextData[i].page - 1;
                contextData[i].activities.push({
                  title: data.files[0].name,
                  type: data.result.type,
                  origin:'teacher',
                  url: data.result.url
                });
                break;
              }
            }

            $divContext.find('.context-loading').hide();
            $divContext.find('.context-success').show();
            $('#edit-button').click();
            setTimeout(function() {
              $divContext.find('.context-success').hide();
              $('#tool-upload').removeClass('circle-set');
              $(document).trigger('closeAttachMenu');
              ZoomBookContext.appendContexts(pageIndex);
              $(document).trigger('toggleOptionContexts', true);
              if (window.contextShown) {
                $(document).trigger('ContextChange', context);
              }
            }, 1000);
          }
        },
        progressall: function (e, data) {
          var progress = parseInt(data.loaded / data.total * 100, 10);
          $('#progress .bar').css(
            'width',
            progress + '%'
          );
        }
      });

      feather.replace();
    },

    showContext: function(context) {
      var $elem = $('#' + context.id);
      this.showContextElem($elem);
    },

    showContextElem: function($elem) {
      window.contextShown = $elem.data('context');
      var zoomWidth = ($(window).width() - 60) / window.contextShown.width;
      var zoomHeight = ($(window).height() - 110) / window.contextShown.height;
      var zoom = (zoomWidth < zoomHeight) ? zoomWidth : zoomHeight;

      //var actualZoom = readerControl.docViewer.getZoom();
      //zoom *= actualZoom;
      var pageLeft = (window.contextShown.pageIndex%2) ? readerControl.docViewer.getPageWidth(window.contextShown.pageIndex) : 0;
      var x = parseInt((pageLeft + window.contextShown.x - 20) * zoom);
      var y = parseInt((window.contextShown.y - 15)  * zoom);

      //readerControl.docViewer.zoomTo(zoom, x, y);
      this.animateZoom(zoom, x, y);
      $(document).trigger('ContextChange', window.contextShown);
    },

    animateZoom: function(zoom, x, y) {
      var steps = 30;
      var stepVelocity = 30;


      var $viewerElement = $('#DocumentViewer');

      var initX = $viewerElement.scrollLeft();
      var stepX = (x - initX) / steps;
      var incrementX = initX;

      var initY = $viewerElement.scrollTop();
      var stepY = (y - initY) / steps;
      var incrementY = initY;

      var initZoom = readerControl.docViewer.getZoom();
      var stepZoom = (zoom - initZoom) / steps;
      var incrementZoon = initZoom;

      var step = 0;
      var stepInterval = setInterval(function () {
        step += 1;
        incrementZoon += stepZoom;
        incrementX += stepX;
        incrementY += stepY;

        if (step > steps) {
          readerControl.docViewer.zoomTo(zoom, x, y);
          clearInterval(stepInterval);
        }
        else {
          readerControl.docViewer.zoomTo(incrementZoon, incrementX, incrementY);
        }

      }, stepVelocity);

    },

    scrollToContextElem: function($elem){
      var $viewerElement = $('#DocumentViewer');
      if (window.contextShown) {
        $viewerElement.animate({
          scrollLeft: $elem.offset().left,
          scrollTop: $elem.offset().top
        }, 200);
      }
    },

    nextContext: function () {
      if (window.contextShown && window.contextShown.index < contextData.length - 1) {
        this.showContext(contextData[window.contextShown.index + 1]);
      }
    },

    prevContext: function () {
      if (window.contextShown && window.contextShown.index > 0) {
        this.showContext(contextData[window.contextShown.index - 1]);
      }
    }
  };
})(jQuery);
