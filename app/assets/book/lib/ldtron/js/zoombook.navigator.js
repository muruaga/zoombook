'use strict';

(function ($) {

  window.showMode = 'document';

  $(document).on('documentLoaded', function () {
    ZoomBookNavigator.init();

    readerControl.docViewer.on('pageComplete', function (event, pageIndex) {
      if (pageIndex%2 == 0) {
        setTimeout(function () {
          if (pageIndex === readerControl.docViewer.getPageCount() - 2) {
            pageIndex = readerControl.docViewer.getPageCount();
          }
          var percent = (pageIndex / (readerControl.docViewer.getPageCount())) * 100;
          if (ZoomBookNavigator.sliderStop !== true || pageIndex === 0 || pageIndex === readerControl.docViewer.getPageCount()) {
            $('.zoombook-nav-slide').find('.ui-slider-handle').css('left', percent + '%');
            var pixels = Math.floor(percent * 4);
            $('.zoombook-nav-slide').find('.slide-value').css('width', pixels + 'px');
          }
          ZoomBookNavigator.sliderStop = false;

        }, 100);
      }
      ldReader.updateBookmarksView(pageIndex);
    });
  });

  var isContextMode = false;
  $(document).on('ContextShow', function () {
    window.showMode = 'context';
    ZoomBookNavigator.hide();
  });

  $(document).on('ContextClose', function () {
    window.showMode = 'document';
    ZoomBookNavigator.show();
  });

  var ZoomBookNavigator = {
    viewer: null,

    init: function() {
      this.viewer = readerControl.docViewer;
      this.createFlitPages();
      this.createNavSlide();
      this.createNavButtons();
    },

    show: function() {
      this.$flitPages.show();
      this.$navSlide.show();
    },

    hide: function() {
      this.$flitPages.hide();
      this.$navSlide.hide();
    },

    createNavButtons: function() {
      var htmlButtons =
        '<div id="navigate-left" class="zoombook-nav-button svg-button"><i data-feather="arrow-left"></i></div>' +
        '<div id="navigate-right" class="zoombook-nav-button svg-button"><i data-feather="arrow-right"></i></div>';
      $('body').append(htmlButtons);
      feather.replace();
      this.eventNavButtons();
    },

    eventNavButtons: function () {
      var me = this;
      $('#navigate-right').on('click', function () {
        ZoomBookUtil.animationClick($(this), 'flash', function () {
          if (showMode === 'document') {
            me.nextPage();
          }
          else {
            me.nextContext();
          }
        });
      });

      $('#navigate-left').on('click', function () {
        ZoomBookUtil.animationClick($(this), 'flash', function () {
          if (showMode === 'document') {
            me.prevPage();
          } else {
            me.prevContext();
          }
        });
      });
    },

    nextPage: function () {
      var me = this;
      var currentPage = me.viewer.getCurrentPage();
      var pageCount = me.viewer.getPageCount();
      if (currentPage < pageCount) {
        currentPage +=2;
        if (currentPage > pageCount) {
          currentPage = pageCount;
        }
        me.viewer.setCurrentPage(currentPage);
      }
    },

    prevPage: function () {
      var me = this;
      if (showMode === 'document') {
        var currentPage = me.viewer.getCurrentPage();
        if (currentPage > 0) {
          currentPage -=2;
          if (currentPage === 2 ) {
            currentPage = 1;
          }
          me.viewer.setCurrentPage(currentPage);
        }
      }
    },

    nextContext: function () {
      $(document).trigger('ZoomBookNextContext');
    },

    prevContext: function () {
      $(document).trigger('ZoomBookPrevContext');
    },

    createFlitPages: function() {
      var pagesCount = readerControl.docViewer.getPageCount();
      var $body = $('body');
      var flitPages = '<div id="flitPages"><div id="FlitPagesContainer"><ul>';
      for ( var i = 0; i < pagesCount; i++) {
        var pear = i%2;
        if (!pear) {
          flitPages += '<li data-flip-title="">';
        }
        flitPages += '<div class="zoombook-flit-page"><span>' + (i + viewerConfig.initialPage) + '</span></div>';
        if (pear) {
          flitPages += '</li>';
        }
      }
      flitPages += '</ul></div></div>';
      $body.append(flitPages);

      var me = this;
      setTimeout(function () {
        me.$flitPages = $('#flitPages');
        me.flat = me.$flitPages.flipster({
          style: 'flat',
          spacing: -0.25,
          click: true,
          keyboard: false,
          scrollwheel: false,
          touch:false,
          onItemSwitch: function ($selectedItem) {
            me.$flitPages.find('');
          }
        });
      }, 10);

      $(document).on('OnSlidePageHover', function (event, pageIndex) {
        //$flitPages[pageIndex];
      });

      this.createFlitPageThumbs();
    },


    createFlitPageThumbs: function() {
      var $flitPage = $('.zoombook-flit-page');
      var doc = readerControl.docViewer.getDocument();
      var pagesCount = readerControl.docViewer.getPageCount();
      for (var i = 0; i < pagesCount; i++) { //elimina las 2 primeras páginas
        (function() {
          var pageIndex = i;
          var requestId = doc.loadThumbnailAsync(pageIndex, function(thumb) {
            $($flitPage[pageIndex]).prepend(thumb);
          });
        })();
      }
    },

    showflitPages: function() {
      this.$flitPages.addClass('visible-up');
    },

    hideflitPages: function() {
      this.$flitPages.removeClass('visible-up');
    },

    sliderStop: false,

    createNavSlide: function () {
      var htmlNavSlide = '<ul class="zoombook-nav-slide">';
      htmlNavSlide +='<div id="sliderPages"><div class="slide-value"></div></div>';
      htmlNavSlide += '</ul>';
      this.$navSlide = $(htmlNavSlide);
      $('body').append(this.$navSlide);

      var me = this;
      var pagesCount = readerControl.docViewer.getPageCount();
      var $slideValue = $('.slide-value');
      var slideHandleInterval;
      setTimeout(function () {
        me.$sliderPages = $('#sliderPages');
        me.$sliderPages.slider({
          min: viewerConfig.initialPage,
          max: viewerConfig.initialPage + pagesCount - 1,
          value: viewerConfig.initialPage,
          start: function(event, ui) {
            slideHandleInterval = setInterval(function () {
              var width = ui.handle.offsetLeft;
              if (width < 0) {
                width = 0;
              }
              $slideValue.css('width',width);
            },100);
          },
          stop: function() {
            clearInterval(slideHandleInterval);
            var currentPage = me.$flitPages.find('.flipster__item--current span:first').text();
            currentPage -= viewerConfig.initialPage - 1;
            me.viewer.setCurrentPage(currentPage + 1);
            me.sliderStop = true;
          },
          change: function (event, ui) {
            var width = ui.handle.offsetLeft;
            if (width < 0) {
              width = 0;
            }
            $slideValue.css('width',width);
          }
        });

        me.$sliderPages.mouseenter(function () {
          me.showflitPages();
        });

        me.$sliderPages.mousemove(function (event) {
            var pageWidth = me.$sliderPages.width() / readerControl.docViewer.getPageCount() * 2;
            var offsetX = event.offsetX;
            if (event.toElement !== event.currentTarget) {
              offsetX += parseInt($(event.toElement).position().left);
            }
            var pageIndex = parseInt(offsetX / pageWidth);
            var maxPageIndex =me.$flitPages.find('.flipster__item').length;
            if (pageIndex === maxPageIndex ) {
              --pageIndex;
            }
            else if (pageIndex < 0) {
              pageIndex = 0;
            }
            me.flat.flipster('jump', pageIndex);
        });

        $('.flipster').mouseleave(function () {
          me.hideflitPages();
        });

        $('.zoombook-flit-page').on('click', function () {
          var currentPage = $(this).text();
          me.$sliderPages.slider('value', parseInt(currentPage));
          currentPage -= viewerConfig.initialPage - 1;
          me.viewer.setCurrentPage(currentPage);

        });
      }, 10);

    }

  };
})(jQuery);