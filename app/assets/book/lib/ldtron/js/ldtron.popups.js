'use strict';

var LDTronPopup = $.extend({
  openHTMLContent: function(params) {
    var me = this;
    if ($('#iframeContent').length > 0) {
      $('#iframeContent').remove();
    }
    if ($('#iframeContent').dialog('instance') != null) {
      $('#iframeContent').dialog('destroy');
    }
    var $iframe = $('<iframe id="iframeContent" style="display:none;width:100px;height:100px"></iframe>');
    $iframe.attr('title', params.title.trim());
    $iframe.dialog({
      modal: true,
      width: params.popupW,
      height: params.popupH,
      dialogClass: 'ldtron-dialog-popup',
      resizable: false,
      open: function (event, ui) {
        if (params.popupW != 0) {
          $iframe.css('width', params.popupW + 'px');
        }
        if (params.popupH != 0) {
          $iframe.css('height', params.popupH + 'px');
        }
        $iframe.attr('src', params.src);

        $(document).trigger('openDialogLDTron', 'html');
      },
      close: function() {
        $(document).trigger('closeDialogLDTron');
        $('#iframeContent').remove();
      }
    });
  },

  openImgContent: function(params) {
    var me = this;
    var style = '';
    if (params.maxWidth && params.maxWidth < params.popupW) {
      style = 'style="max-width:' + params.maxWidth + 'px !important"';
    }
    var $img = $('<img id="imgContent" src="' + params.src + '" border="0" ' + style + '/>');
    $img.appendTo('body');
    $img.attr('title', params.title.trim());
    $img.load(function(event) {
      var $dialog= $img.dialog({
        modal: true,
        dialogClass: 'ldtron-dialog-popup',
        resizable: false,
        width:'auto',
        open: function(event, ui) {
          me.updateTitleWidth($(event.target).dialog('instance'));
          $(document).trigger('openDialogLDTron', 'image');
        },
        close: function() {
          $(document).trigger('closeDialogLDTron');
          $img.remove();
        }
      });
    });
  },

  openVideoContent: function(params) {
    var me = this;
    var srcMp4 = params.src;
    var srcWebm = params.src2;

    if (srcMp4.indexOf('.mp4') === -1 || srcWebm.indexOf('.webm') === -1) {
      srcWebm = params.src;
      srcMp4 = params.src2;
    }

    var popup_w = parseInt(params.popupW);
    var popup_h = parseInt(params.popupH + 10);

    var vdo = '<div style="width: '+ popup_w + 'px; height: ' + popup_h + 'px;" >' +
      '<video id="videoContent" width="' + popup_w + '" height="' + popup_h + '" controls="controls" preload="none">' +
      '<source type="video/webm" src="'+ srcWebm +'"></source>' +
      '<source type="video/mp4" src="'+ srcMp4 +'"></source>' +
      '</video></div>';

    var $vdo = $(vdo).appendTo('body');
    $vdo.attr('title', params.title.trim());
    var player = null;

    $vdo.dialog({
      modal: true,
      dialogClass: 'ldtron-dialog-popup',
      resizable: false,
      width:'auto',
      open: function(event, ui) {
        player = new MediaElementPlayer('#videoContent', {
          features: ['playpause', 'progress', 'duration', 'current', 'volume', 'fullscreen', 'mediaCordova'],
          defaultVideoWidth: popup_w,
          defaultVideoHeight: popup_h,
          pauseOtherPlayers: true,
          success: function (mediaElement, domObject) {
            $('.mejs-container').css({'overflow': 'hidden'});
            $('.mejs-container').parent().css({'overflow': 'hidden'});
            mediaElement.load();
            mediaElement.play();
          }
        });
        me.updateTitleWidth($(event.target).dialog('instance'));
        $(document).trigger('openDialogLDTron', 'video');
      },
      close: function () {
        $(document).trigger('closeDialogLDTron');
        player.pause();
        player.remove();
        $vdo.dialog('destroy');
        $vdo.remove();
      }
    });
  },

  openAudioContent: function(params, bt) {
    var srcMp3 = params.src;
    var srcOgg = params.src2;
    if (srcMp3.indexOf('.mp3') === -1 || srcOgg.indexOf('.ogg') === -1) {
      srcMp3 = params.src2;
      srcOgg = params.src2;
    }

    var source = '<audio id="audioContent">';
    source += '<source id="audio_player_ogg" src="' + srcOgg + '"  type="audio/ogg" />';
    source += '<source id="audio_player_mp3" src="' + srcMp3 + '"  type="audio/mpeg" />';
    source += '</audio>';

    var $audio = $(source).appendTo('body');
    $audio.attr('title', params.title.trim());

    var player = null;
    var playerWidth = 250, playerHeihgt = 30;

    $audio.dialog({
      modal: true,
      dialogClass: 'ldtron-dialog-popup',
      resizable: false,
      width: playerWidth,
      height: playerHeihgt,
      position: {at : 'right+80 top', of: bt },
      open: function () {
        player = new MediaElementPlayer('#audioContent', {
          features: ['playpause', 'progress', 'duration', 'current', 'mediaCordova'],
          audioWidth: playerWidth,
          audioHeight: playerHeihgt,
          pauseOtherPlayers: true,
          success: function (mediaElement, domObject) {
            $('.mejs-container').css('overflow', 'hidden');
            $audio.css({ 'width': playerWidth + 'px', 'height': playerHeihgt + 'px' });
            mediaElement.play();
          }
        });
        $(document).trigger('openDialogLDTron', 'audio');
      },
      close: function () {
        $(document).trigger('closeDialogLDTron');
        player.pause();
        player.remove();
        $audio.dialog('destroy');
        $audio.remove();
      }
    });
  },

  showInfo: function(title, msg) {
    var $msgBody =
      $('<div id="dialog" title="' + title +  '">'+
        '<p>' + msg + '</p>' +
      '</div>').appendTo("body");

    var msg = $msgBody.dialog({
      modal: true,
      dialogClass: 'ldtron-dialog-popup info',
      resizable: false,
      open: function() {
        $(document).trigger('openDialogLDTron', 'info');
      },
      close: function () {
        $(document).trigger('closeDialogLDTron');
        $msgBody.dialog('destroy');
        $msgBody.remove();
      }
    });
  },

  updateTitleWidth: function(dialogInstance) {
    var $uiDialog = dialogInstance.uiDialog,
        $uiDialogTitlebar = dialogInstance.uiDialogTitlebar,
        $uiDialogTitlebarClose = dialogInstance.uiDialogTitlebarClose;
    var width = parseInt($uiDialog.width() - ($uiDialogTitlebarClose.width())*3);
    $uiDialogTitlebar.find('.ui-dialog-title').width(width);
  }
}, (LDTronPopup) ? LDTronPopup : {});

// Inciar listener
$(window).ready(function() {
  $(document).on('LDTronPopupInfo', function(event, options){
    var title = options.title.trim();
    var msg = options.msg;
    LDTronPopup.showInfo(title, msg);
  });
});