'use strict';

/**
* LDTron.Viewer visor PDFTron como libro digital de Savia.
* Espacio denombre reservado para LDTron
* @namespace LDTron namespace 
*/

var LDTron = LDTron || {};

LDTron.VERSION = {
  RELEASE: '4.3.0',
  DATE: '07/06/2018'
};

// Evita el error cross-domain del visor nativo PDFTron.
window.utils = {
  isNodeJS: false
};

// Clase del visor LDTron y se oculta a la espera que se inicie
$('#ui-display').addClass('ldtron');

/**
  @class Clase Viewer que iniciacializa el la instacia del visor sesun dispositivo WEB o Mobile
  @param {LDTron.Viewer.Options} Opciones del definidos en el Backend que se encuentra en data/data.js
*/
LDTron.Viewer = function(options) {
  this.options = options;
};

/**
 * @class Options de creación de la instancias Viewer.
 * @property {string} [type="html5,html5Mobile"] the type of WebViewer to load. Values must be comma-separated in order of preferred WebViewer. (Possibe values: html5, html5Mobile)
 * @property {string} initialDoc the URL path to a xod document to load on startup, it is recommended to use absolute paths.
 * @property {number} [showCoverPage=0]
 * @property {string} [pdfVisibleTitle="Sin titulo"] Nombre del título del documento que se muestra en la cabecera del visor
 * @property {string} [pdfFile="sin-titulo.pdf"] Nombre del archivo del libro que se utiliza para salvar las anotaciones
 * @property {string} [lang="es"] Idioma en que se presenta el visor
 * @property {number} [initialPage=1] Número de página que se muetra en el inico del visor
 * @property {string} [sello="1"] Indica qué Logo se muetra en la cabecera del visor
 * @property {string} [twoPages="0"] Indica si se muestra el botón de doble página
 * @property {string} [defaultPages="0"] Indica al visor cómo se muestra por defecto el documento, 1 Simple, 2 Doble página
 * @property {string} [draw="1"] Indica si se muestra las anotaciones
 * @property {number} [yScrollTopIncrement= 20] Número de pixels en que muevo el scroll usando el API de integración
 */
LDTron.Viewer.Options = {
  showCoverPage:0,
  pdfVisibleTitle:"Sin titulo",
  pdfFile:"sin-titulo.pdf",
  lang:"es",
  initialPage:1,
  sello:"1",
  expAnnots:"",
  twoPages:"0",
  defaultPages:"0",
  draw:"1",
  yScrollTopIncrement: 20
};

LDTron.Viewer.prototype = {
  create: function() {
    LDTron.API_RELEASE = this.options.getString('apiSMRelease');
    LDTron.API_DATE = this.options.getString('apiSMDate');
    // Crear e inicializa el visor
    var idAPI = this.options.getString('idAPI');
    this.createLDTronReader(idAPI).initialize();

    // Ajustar caundo se muestra el visor
    this.initFitLayoutEvents();

    // Se muestra el visor
    $('#ui-display').css({'visibility':'visible'});

    window.ldReader.getVersion = this.getVersion;
    window.ldReader.getStrVersion = this.getStrVersion;
  },


  getVersion: function () {
    var version =
      '<span>Release: ' + LDTron.VERSION.RELEASE + ' | ' + LDTron.VERSION.DATE + '</span>' +
      '<span>Lib: ' + readerControl.docViewer.version + '</span>' +
      '<span>API Int: ' + LDTron.API_RELEASE + ' | ' + LDTron.API_DATE + '</span>';
    return version;
  },


  getStrVersion: function () {
    var version =
      'Release: ' + LDTron.VERSION.RELEASE + ' | ' + LDTron.VERSION.DATE +
      '\nLib: ' + readerControl.docViewer.version +
      '\nAPI Int: ' + LDTron.API_RELEASE + ' | ' + LDTron.API_DATE;
    return version;
  },

  /**
   * Instancia la del visor según dispositivo
   */
  createLDTronReader: function(id) {
    // Opciones de ejecución
    var options = this.createReaderOptions();

    // Instancia Mobile
    if (LDTronUtils.isMobileDevice()) {
      window.ldReader = new LDTronMobile(id, options);
    }
    // Instancia WEB
    else {
      window.ldReader = new LDTronWeb(id, options);
    }


    var pilotoLocalStorage = $.initNamespaceStorage('PILOTO').localStorage;
    options.userGUID = pilotoLocalStorage.get('useruuid');
    if (typeof options.userGUID === 'undefined') {
      options.userGUID = 'USER-TEST-' + new Date().getTime();
      pilotoLocalStorage.set('useruuid', options.userGUID );
    }

    readerControl.storage = this.createLocalStorage(window.ldReader, options);
    if (readerControl.storage != null && options.draw === '1') {
      this.initAPIAnnotations(window.ldReader, readerControl.storage, options);
    }

    // Si no hay localstore se muestra el modo configurado
    $(document).on('documentLoaded', function() {
      if (readerControl.storage === null) {
        window.ldReader.setLayoutMode();
      }

      // Se deshabilita el clipboard.
      // Tener en cuenta que este evento pone el foco a un textArea, provando un scrollTop = 0 en la ventana
      readerControl.docViewer.off('textSelected');
    });

    return window.ldReader;
  },

  /**
   * Devuelve las opcions de ejecución del visor
   */
  createReaderOptions: function() {
    var ldTronOptions = $.extend(
      LDTron.Viewer.Options,
      window.viewerConfig
    );

    ldTronOptions.path = this.options.getString('absolutePath', '');
    ldTronOptions.userGUID = this.options.getString('userGUID', '');
    ldTronOptions.piezaGUID = this.options.getString('piezaGUID', '');
    ldTronOptions.productCode = this.options.getString('productCode', '');
    ldTronOptions.sessionID = this.options.getString('sessionID', '');
    ldTronOptions.annotationsServiceURL = this.options.getString('annotationsServiceURL', null);

    if (typeof window.contentData === 'undefined') {
      window.contentData = [];
    }
    if (typeof window.tableOutline === 'undefined') {
      window.tableOutline = [];
    }
    if (typeof window.tablePersonal === 'undefined') {
      window.tablePersonal = [];
    }

    ldTronOptions = $.extend(
      ldTronOptions, {
        contentData: contentData,
        tableOutline: tableOutline,
        tablePersonal: tablePersonal
      }
    );
    return ldTronOptions;
  },

  // Evento para ajustar al ancho el documento cuando se cambia de modo
  initFitLayoutEvents: function() {
    var viewer = readerControl.docViewer;
    $(document).on('LDTronLayoutModeChanged', function() {
      viewer.setFitMode(viewer.FitMode.FitWidth);
    });
  },

  // LocalStorage para consultar las anotaciones y variables de control timestamp y needpush
  createLocalStorage: function(ldReader, options) {
    var storage = null;
    if (typeof options.userGUID === 'string' &&
        options.userGUID.length > 0 &&
        typeof options.piezaGUID === 'string' &&
        options.piezaGUID.length > 0) {
      storage = new LDTronStorage(ldReader, {
        storageID: options.userGUID + options.piezaGUID
      });
    }
    return storage;
  },

  // Se inicia el control de anotaciones para guadarlas al servicio
  initAPIAnnotations: function(ldReader, storage, options) {
    readerControl.serverUrl = null; // El visor nativo PDFTron no enviará peticiones

    // Anotaciones en Remoto
    if (typeof options.userGUID !== 'undefined' &&
        typeof options.piezaGUID !== 'undefined' &&
        typeof options.annotationsServiceURL !== 'undefined' &&
        typeof options.sessionID !== 'undefined') {

      // Registro cambios de anotaciones pendientes de salvar. Se inicia a 1 para cargar la ultima version
      var pendingPush = 1;

      // Los cambios de anotaciones se guardan en LocalStore con needpush=true
      $(document).on('LDTronStorageAnnotationsSaved', function() {
        if (storage.getLocalStorage('timestamp') !== null) {
          ++pendingPush;
        }
      });

      // Opciones de ejecucion del API de anotaciones
      var optionsAPI = {
        guidUser: options.userGUID,
        guidPieza: options.piezaGUID,
        productCode: options.productCode,
        sessionID: options.sessionID,
        serviceURL: options.annotationsServiceURL
      };

      // Datos para salvar las anotaciones
      var resquestAnnotations = function() {
        var annotations = null;
        if (pendingPush > 0) {
          annotations = {
            annotations: storage.getLocalStorage(STORAGE_KEYS.ANNOTATIONS), // Cadena con las anotaciones
            timestamp: storage.getLocalStorage('timestamp')                 // Indica si se tiene que salvar las anotaciones
          };
        }
        return annotations;
      };

      // Respuesta al salvar anotaciones
      var responseAnnotations = function(response) {
        storage.setLocalStorage('timestamp', response.timestamp); // Se guarda en LocalStorage el ultimo timestamp
        if (response.data !== null && response.data.length > 0) { // Si hay anotaciones con una version superior esta se pinta en el visor
          storage.syncronizedAnnotations(response.data);
        }
        // Si se han quedado anotaciones por salvar se salva el ultimo cambio,  sino se espera el siguiente cambio
        pendingPush = (pendingPush > 1) ? 1 : 0;
      };

      // Ejecución del API de anotaciones
      new api.sm.Annotations(optionsAPI, resquestAnnotations, responseAnnotations);

      // About
      $(document).on('dblclick', '#toggleTitle', function() {
        alert(window.ldReader.getVersion());
      });
      // About mobile
      $(document).on('swipeleft', '#headerTitle', function() {
        alert(window.ldReader.getVersion());
      });
    }
  }
};

/**
* Ejecucion del visor cuando se ha cargado la configuracion
*/
$(document).on('viewerLoaded', function() {
  var options = window.ControlUtils.getQueryStringMap();
  options.path = options.getString('absolutePath', '');
  readerControl.docViewer.setMargin(0); // Default margin is 8
  // Cuando se obtiene la configuración [data/data.js] se ejecuta el visor
  $.getScript(options.path + 'data/data.js')
    .done(function() {
      new LDTron.Viewer(options).create();
    })
    .fail(function(jqxhr, settings, exception) {
      alert('El visor no puede cargar los datos configurados.');
      throw new String('LDTron getScript exception:' + exception);
    });

    // Herramienta de dibujo (mano alzada) con trazos individuales
    Tools.FreeHandCreateTool.prototype.createDelay = 0;

    // Filtrado en lista de anotaciones sin trazos
    readerControl.docViewer.getAnnotationManager().on('annotationChanged', function(e, annots, action) {
      if (action === 'add') {
        annots.forEach(function(annot) {
          if (annot instanceof Annotations.FreeHandAnnotation) {
            annot.Listable = false;
          }
        });
      }
    });
});

/**
* Posicionamiento vertical centrado de los dialogos
*/
$(document).on('openDialogLDTron repaintDialogLDTron', function(event) {
  var iFrameData = window.ldReader.iFrameData;
  if (typeof iFrameData !== 'undefined') {
    var $dialog = $("div.ldtron-dialog-popup, div#LDTronPopup-popup");
    if ($dialog.length > 0) {
      var minTop = parseInt($('#control,#pageHeader').outerHeight(true));
      if (minTop < 20) { minTop = 20; }
      var iframeTop = parseInt(iFrameData.topFrame) + minTop,
          iframeScrollTop = parseInt(iFrameData.scrollTop),
          iframeHeight = parseInt(iFrameData.height),
          dialogTop = iframeTop - iframeScrollTop;
      var areaHeight = iframeHeight + (iframeScrollTop / 2) - iframeTop;
      var top = (areaHeight + minTop + iframeScrollTop - $dialog.height()) / 2;
      if (top < minTop) {
        top = minTop;
      }
      $dialog.css("top", top + "px");
    }
  }
});

/**
 * Scroll del visor al resultado cuando se realiza una búsqueda de texto
 */
$(document).on('displaySearchResult', function(event, result, headerHeight) {
  readerControl.docViewer.displaySearchResult(result);
  $(document).trigger('scrollToResult', result, headerHeight);
});

/**
 * Posicionamiento vertical del scroll al resultado de la busqueda.
 */
$(document).on('scrollToResult', function(event, result, headerHeight) {
  if (result.quads.length > 0) {
    var quads = result.quads[0];
    if (typeof headerHeight === 'undefined') {
      headerHeight = 0;
    }
    var top = parseInt(quads.y1 * readerControl.getZoomLevel()) + headerHeight;
    LDTronUtils.scrollY(window.ldReader.idAPI, top);
  }
});

/**
 * Detiene todos los audios y videos
 */
function PauseMedias() {
  $('audio,video').each(function () {
    this.pause();
    if (this.player) {
      this.player.pause();
    }
  });
}


function deletePageAnnotations(pages) {
  var am = readerControl.docViewer.getAnnotationManager();
  var arrAllAnnotations = am.getAnnotationsList();
  var arrPageNotations = arrAllAnnotations.filter(function(annot) {
    return (pages.includes( annot.PageNumber ));
  });
  am.deleteAnnotations(arrPageNotations);
}

// solve function deletePageAnnotations for IE
if (!Array.prototype.includes) {
  Object.defineProperty(Array.prototype, 'includes', {
    value: function(searchElement, fromIndex) {

      // 1. Let O be ? ToObject(this value).
      if (this == null) {
        throw new TypeError('"this" is null or not defined');
      }

      var o = Object(this);

      // 2. Let len be ? ToLength(? Get(O, "length")).
      var len = o.length >>> 0;

      // 3. If len is 0, return false.
      if (len === 0) {
        return false;
      }

      // 4. Let n be ? ToInteger(fromIndex).
      //    (If fromIndex is undefined, this step produces the value 0.)
      var n = fromIndex | 0;

      // 5. If n ≥ 0, then
      //  a. Let k be n.
      // 6. Else n < 0,
      //  a. Let k be len + n.
      //  b. If k < 0, let k be 0.
      var k = Math.max(n >= 0 ? n : len - Math.abs(n), 0);

      // 7. Repeat, while k < len
      while (k < len) {
        // a. Let elementK be the result of ? Get(O, ! ToString(k)).
        // b. If SameValueZero(searchElement, elementK) is true, return true.
        // c. Increase k by 1.
        // NOTE: === provides the correct "SameValueZero" comparison needed here.
        if (o[k] === searchElement) {
          return true;
        }
        k++;
      }

      // 8. Return false
      return false;
    }
  });
}