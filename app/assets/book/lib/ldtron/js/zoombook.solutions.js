'use strict';

(function ($) {

    $(document).on('documentLoaded', function () {
        ZoombookSolutions.init();
    });

    $(document).on('openSolutions', function (event, solutions) {
        ZoombookSolutions.initQuestions(JSON.parse(solutions));
        $('#solutions-popup').addClass('visible');
    });

  var ZoombookSolutions = {
      init: function () {
          this.viewer = readerControl.docViewer;
          this.createSolutions();
      },
      createSolutions: function () {
          var htmlSolutions =
            '<div id="solutions-popup" class="visible">' +
                '<div class="solutions-header">' +
                    '<div id="icon-solutions"><i data-icons="solucionario.svg" style="transform: scale(2);width: 10px;position: absolute;top: 70px;"></i></div>' +
                    '<div id="close-solutions"><i data-feather="x"></i></div>' +
                    '<title>Solucionario de actividades</title>' +
                    '<subtitle>Actividades <span id="sols-from-to">23 - 25</span></subtitle>' +
                '</div>' +
                '<div class="solutions-body">' +
                    '<div class="solutions-question closed">' +
                        '<div class="solutions-question-button">' +
                          '<div class="solutions-question-eye"><i data-feather="eye"></i></div>' +
                          '<div class="solutions-question-eye-off"><i data-feather="eye-off"></i></div>' +
                        '</div>' +
                        '<div class="solutions-question-text">Copia dos de las columnas en tu cuaderno y relaciónalas con flechas</div>' +
                        '<div class="solutions-answer">Lorem ipsum...</div>' +
                    '</div>' +
                '</div>' +
            '</div>';
          $('body').append(htmlSolutions);
          var list = document.querySelectorAll('[data-icons]');
          for (var i = 0; i < list.length; i++) {
              var url = '../sm/icons/' + list[i].getAttribute('data-icons');
              list[i].style.backgroundImage="url('" + url + "')";
          }
          feather.replace();
          $('#close-solutions').click(function () {
              $('#solutions-popup').removeClass('visible');
          });
          $('#solutions-popup').removeClass('visible');
      },
      initQuestions:function (solutions) {
          var $body = $('.solutions-body');
          $body.empty();
          var questionTemplate =
              '<div class="solutions-question closed">' +
              '<div class="solutions-question-button">' +
              '<div class="solutions-question-eye"><i data-feather="eye"></i></div>' +
              '<div class="solutions-question-eye-off"><i data-feather="eye-off"></i></div>' +
              '</div>' +
              '<div class="solutions-question-text">{0}</div>' +
              '<div class="solutions-answer">{1}</div>' +
              '</div>';
          var htmlQuestions = '';
          var minQ = 0, maxQ = 0;
          for (var i = 0; i < solutions.length; i++) {
              if (minQ === 0 || solutions[i].number < minQ) minQ = solutions[i].number;
              if (solutions[i].number > maxQ) maxQ = solutions[i].number;
              htmlQuestions = htmlQuestions + questionTemplate.replace('{0}',solutions[i].question).replace('{1}',solutions[i].answer)
          }
          $('#sols-from-to').text('' + minQ + ' - ' + maxQ);
          $body.append(htmlQuestions);
          feather.replace();
          $('.solutions-question-button').click(function() {
              $(this).parent().toggleClass('closed').toggleClass('opened');
          });
      }
  }
})(jQuery);