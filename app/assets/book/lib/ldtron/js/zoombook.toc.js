'use strict';

(function ($) {

  window.showMode = 'document';

  $(document).ready(function () {
      ZoomBookToc.init();
  });

  var ZoomBookToc = {

    init: function() {
      this.createButton();
    },

    createButton: function() {
      var htmlButton =
        '<div id="showToc" class="button-toc svg-button"><i data-feather="menu"></i></div>' +
        '<div id="zoomToc"><div id="closeToc"><i data-feather="x"></i></div></div>';
      $('body').append(htmlButton);
      var $toc = $('#bookmarkView').detach();
      $('#zoomToc').append($toc);
      feather.replace();
      this.eventButtons();
    },

    eventButtons: function () {
      var me = this;
      var $zoomToc = $('#zoomToc');
      $('#showToc').on('click', function () {
          anime({
              targets: '#zoomToc',
              left: 0,
              easing: 'easeInOutSine',
              duration: 500
          });
      });
      $('#closeToc').on('click', function () {
          anime({
              targets: '#zoomToc',
              left: -500,
              easing: 'easeInOutSine',
              duration: 500
          });
      });
    }
  };
})(jQuery);