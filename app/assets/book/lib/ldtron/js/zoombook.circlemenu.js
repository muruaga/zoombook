'use strict';

(function ($) {

  window.showMode = 'document';

  $(document).on('closeCircleMenu', function() {
      ZoomBookCircleMenu.closeCircleMenu();
  });
  $(document).on('openCircleMenu', function() {
      ZoomBookCircleMenu.openCircleMenu();
  });
  $(document).on('documentLoaded', function () {
      ZoomBookCircleMenu.init();
  });
  $(document).on('toggleEditing', function (event, isEditing) {
      ZoomBookCircleMenu.isEditing = isEditing;
      ZoomBookCircleMenu.closeCircleMenu();
        if (ZoomBookCircleMenu.isEditing) {
          $('#circle-menu').addClass('editing');
          ZoomBookCircleMenu.spreadAngle = 150;
          ZoomBookCircleMenu.lessIndex = 1;
          $('#layer-teacher').removeClass('circle-active');
          $('#layer-teacher').tooltipster('content', 'Mostrar anotaciones');
          $('#layer-contexts').removeClass('circle-active');

          $('#tool-highlight').addClass('circle-active');
          $('#tool-attachment').addClass('circle-active');
          $('#tool-comment').addClass('circle-active');

          $('.tool').replaceClass('circle-set');
          $('#tool-highlight').addClass('circle-set');
          readerControl.setToolMode('AnnotationCreateTextHighlight');
          $(document).trigger('toggleOptionTeacher', true);

      } else {
          $('#circle-menu').removeClass('editing');
          ZoomBookCircleMenu.spreadAngle = 60;
        ZoomBookCircleMenu.lessIndex = 0;
          $('#layer-teacher').addClass('circle-active');
          $('#layer-teacher').tooltipster('content', 'Ocultar anotaciones');
          $('#layer-contexts').addClass('circle-active');
          $('#tool-highlight').removeClass('circle-active');
          $('#tool-attachment').removeClass('circle-active');
          $('#tool-comment').removeClass('circle-active');

          readerControl.setToolMode('Pan');
      }
  });
    $(document).on('toggleOptionTeacher', function (event, activated) {
        if (activated) {
            ZoomBookCircleMenu.circles['layer']['teacher'] = true;
            $('#layer-teacher').addClass('circle-set');
            $('#layer-teacher').tooltipster('content', 'Ocultar anotaciones');
        } else {
            ZoomBookCircleMenu.circles['layer']['teacher'] = false;
            $('#layer-teacher').removeClass('circle-set');
            $('#layer-teacher').tooltipster('content', 'Mostrar anotaciones');
        }
    });
      $(document).on('toggleOptionContexts', function (event, activated) {
        if (activated) {
            ZoomBookCircleMenu.circles['layer']['contexts'] = true;
            $('#layer-contexts').addClass('circle-set');
        } else {
            ZoomBookCircleMenu.circles['layer']['contexts'] = false;
            $('#layer-contexts').removeClass('circle-set');
        }
    });
    $(document).on('toggleOptionQuestions', function (event, activated) {
        if (activated) {
            ZoomBookCircleMenu.circles['layer']['questions'] = true;
            $('#layer-questions').addClass('circle-set');
        } else {
            ZoomBookCircleMenu.circles['layer']['questions'] = false;
            $('#layer-questions').removeClass('circle-set');
        }
    });
    $(document).on('toggleOptionHighlight', function (event, activated) {
      $('.tool').removeClass('circle-set');

      ZoomBookCircleMenu.circles['tool']['highlight'] = true;
      ZoomBookCircleMenu.circles['tool']['comment'] = false;
      ZoomBookCircleMenu.circles['tool']['attachment'] = false;
      $('#tool-highlight').addClass('circle-set');

      readerControl.setToolMode('AnnotationCreateTextHighlight');
    });


    $(document).on('toggleOptionAttachment', function (event, activated) {
        $('.tool').removeClass('circle-set');
        ZoomBookCircleMenu.circles['tool']['attachment'] = true;
        ZoomBookCircleMenu.circles['tool']['highlight'] = false;
        ZoomBookCircleMenu.circles['tool']['comment'] = false;
        $('#tool-attachment').addClass('circle-set');

        readerControl.setToolMode('AnnotationCreateFreeHand');

    });
    $(document).on('toggleOptionComment', function (event, activated) {
        $('.tool').removeClass('circle-set');
        ZoomBookCircleMenu.circles['tool']['comment'] = true;
        ZoomBookCircleMenu.circles['tool']['attachment'] = false;
        ZoomBookCircleMenu.circles['tool']['highlight'] = false;
        $('#tool-comment').addClass('circle-set');

        readerControl.setToolMode('AnnotationCreateFreeText');

    });

  var ZoomBookCircleMenu = {
    active: false,
    dist: 60,
    spreadAngle: 60,
    lessIndex:0 ,
    circles : {
        layer: {
            teacher: false,
            contexts: false,
            student: false,
            questions: false
        },
        tool: {
            highlight: true,
            attachment: false,
            comment: false,
        }
    },
    isEditing: false,

    init: function() {
      this.viewer = readerControl.docViewer;
      this.createCircleMenu();
      this.eventCircleButtons();
      this.closeCircleMenu();


      var list = document.querySelectorAll('[data-icons]');
      for (var i = 0; i < list.length; i++) {
        var url = '../sm/icons/' + list[i].getAttribute('data-icons');
        list[i].style.backgroundImage="url('" + url + "')";
      }
    },

    createCircleMenu: function() {
      var htmlCircleMenu =
        '<div id="circle-menu">' +
          '<div class="background-menu"></div>' +
          '<div id="layer-teacher" class="layer round-button circle-option circle-active tooltip" title="Mostrar anotaciones"><i data-feather="align-justify"></i></div>' +
          '<div id="layer-contexts" class="layer round-button circle-option circle-active tooltip" title="Zoom y recursos"><i data-icons="capas_zoom.svg"></i></div>' +
          '<div id="layer-student" class="layer round-button circle-option tooltip" title="Anotaciones del alumno"><i data-feather="align-justify"></i></div>' +
          '<div id="layer-questions" class="layer round-button circle-option tooltip" title="Preguntar al profesor"><i data-feather="book-open"></i></div>' +
          '<div id="tool-attachment" class="tool round-button circle-option tooltip" title="Dibujar trazos"><i data-icons="pincel.svg"></i></div>' +
          '<div id="tool-highlight" class="tool round-button circle-option circle-set tooltip" title="Subrayar libro"><i data-icons="subrayar.svg"></i></div>' +
          '<div id="tool-comment" class="tool round-button circle-option tooltip" title="Comentarios textuales"><i data-icons="cuadro_de_texto.svg"></i></div>' +
          '<div id="toggle-circle" class="round-button tooltip" title="Capas">' +
            '<div id="toggle-x"><i data-feather="x"></i></div>' +
            '<div id="toggle-mas"><i data-icons="pencil_case.svg"></i></div>' +
            '<div id="toggle-eye"><i data-feather="layers"></i></div>' +
          '</div>' +
        '</div>';
      $('body').append(htmlCircleMenu);
      feather.replace();
      $('.tooltip').tooltipster({side: 'left', theme: 'tooltipster-shadow'});
    },

    eventCircleButtons: function() {
        $('#toggle-circle').click(this.toggleCircleMenu);
        $('.circle-option').click(this.clickCircle);
    },

    clickCircle: function() {
        var idText = $(this).attr('id');
        var id = idText.split('-');
        var activated = !ZoomBookCircleMenu.circles[id[0]][id[1]];
        // Se envía un evento del tipo "toggleOptionTeacher", "toggleOptionContexts"...
        $(document).trigger('toggleOption' + id[1].charAt(0).toUpperCase() + id[1].slice(1), activated);
        if (idText === 'layer-teacher') {
            if (activated) {
                $('#layer-teacher').tooltipster('content', 'Ocultar anotaciones');
            } else {
                $('#layer-teacher').tooltipster('content', 'Mostrar anotaciones');
            }
        }
    },

    showCircleMenu: function() {
      $('#circle-menu').show('slow');
    },

    toggleCircleMenu: function() {
      if (ZoomBookCircleMenu.active) {
        ZoomBookCircleMenu.closeCircleMenu();
      } else {
        ZoomBookCircleMenu.openCircleMenu();
      }
    },

    hideCircleMenu: function() {
      $('#circle-menu').hide('slow');
    },

    openCircleMenu: function() {
      $(document).trigger('closeAttachMenu');
      $('#circle-menu').addClass('opened');
      var slices = $('.circle-option.circle-active').length - 1;
      var angle = (ZoomBookCircleMenu.spreadAngle / slices);
      anime({
          targets: '#toggle-x',
          opacity: 1,
      });
      anime({
          targets: '#toggle-eye',
          opacity: 0,
      });
      anime({
          targets: '#toggle-mas',
          opacity: 0,
      });
      anime({
        targets: '.circle-option.circle-active',
        opacity: [0, 1],
        delay: anime.stagger(100),
        translateX: function(el, i) {
          return (-1) * Math.round(Math.cos(angle * (i - ZoomBookCircleMenu.lessIndex) * Math.PI / 180) * ZoomBookCircleMenu.dist);
        },
        translateY: function(el, i) {
          return (-1) * Math.round(Math.sin(angle * (i - ZoomBookCircleMenu.lessIndex) * Math.PI / 180) * ZoomBookCircleMenu.dist);
        },
      });
      this.active = true;
    },

    closeCircleMenu: function() {
      $('#circle-menu').removeClass('opened');
      anime({
          targets: '#toggle-x',
          opacity: 0,
      });
      var opacityEye = ZoomBookCircleMenu.isEditing ? 0 : 1;
      var opacityMas = ZoomBookCircleMenu.isEditing ? 1 : 0;
      anime({
          targets: '#toggle-eye',
          opacity: opacityEye,
      });
      anime({
          targets: '#toggle-mas',
          opacity: opacityMas,
      });
      anime({
        targets: '.circle-option',
        rotate: 0,
        opacity: [1, 0],
        delay: anime.stagger(100),
        translateX: 0,
        translateY: 0,
      });
      this.active = false;
      if (opacityEye) {
          $('#toggle-circle').tooltipster('content', 'Opciones presentación');
      } else {
          $('#toggle-circle').tooltipster('content', 'Herramientas de edición');
      }
    }

  };
})(jQuery);