(function ($) {
  var isEditing = false;
  var isTeacher = true;
  $(document).on('documentLoaded', function () {
    $(document).trigger('toggleOptionTeacher', false);
  });

  $(document).on('toggleEditing', function (event, activated) {
      var annotManager = readerControl.docViewer.getAnnotationManager();
      if (activated && !isTeacher) {
          annotManager.toggleAnnotations();
          isTeacher = activated;
      }
      isEditing = activated;
  });

  $(document).on('toggleOptionTeacher', function (event, activated) {
    var annotManager = readerControl.docViewer.getAnnotationManager();
    if (activated != isTeacher) {
      annotManager.toggleAnnotations();
      isTeacher = activated;
    }
  });
})(jQuery);