'use strict';

var LDTron = LDTron || {};

// Requiere ControlUtils.js
if (typeof window.ControlUtils === 'undefined') {
  alert('No se puede ejcutar el visor.');
  throw 'El visor LDTron requiere que esté instalado ControlUtils.js' ;
};
// Requiere la configuracion del visor
if (typeof window.viewerConfig === 'undefined') {
  throw 'El visor LDTron requiere que esté la configuración data.js' ;
};

/**
 * @class PDFTronOptions para la creación de una instancia del visor nativo PDFTron.WebViewer.
 * @property {string} [type="html5,html5Mobile"] the type of WebViewer to load. Values must be comma-separated in order of preferred WebViewer. (Possibe values: html5, html5Mobile)
 * @property {string} initialDoc the URL path to a xod document to load on startup, it is recommended to use absolute paths.
 * @property {number} [showCoverPage=0]
 * @property {string} [pdfVisibleTitle="Sin titulo"] Nombre del título del documento que se muestra en la cabecera del visor
 * @property {string} [pdfFile="sin-titulo.pdf"] Nombre del archivo del libro que se utiliza para salvar las anotaciones
 * @property {string} [lang="es"] Idioma en que se presenta el visor
 * @property {number} [initialPage=1] Número de página que se muetra en el inico del visor
 * @property {string} [sello="1"] Indica qué Logo se muetra en la cabecera del visor
 * @property {string} [twoPages="0"] Indica si se muestra el botón de doble página
 * @property {string} [defaultPages="0"] Indica al visor cómo se muestra por defecto el documento, 1 Simple, 2 Doble página
 * @property {string} [draw="1"] Indica si se muestra las anotaciones
 * @property {number} [yScrollTopIncrement= 20] Número de pixels en que muevo el scroll usando el API de integración
 */
LDTron.PDFTronOptions = {
  type: 'html5',
  initialDoc: LDTronUtils.getAbsolutePath(window.location.href) + 'data/data.xod',
  documentType: 'xod',
  streaming: false,
  autoCreate: false,
  enableAnnotations: (window.viewerConfig.draw === '1')
};

/**
  @class LDTron.Runner Clase que ejecuta el HTML para WEB o Mobile según entorno
  @param {LDTron.Runner.Options} options Opciones que se pasan para contruir LDTron.Viewer.
*/
LDTron.Runner = function Runner(options) {
  var runOptions = $.extend(LDTron.PDFTronOptions, options);
  this.options = $.extend(LDTron.PDFTronOptions, runOptions);
  this.path = LDTronUtils.getAbsolutePath(window.location.href);
};

LDTron.Runner.prototype = {
  start: function() {
    this.initListeners();
    if (window.top === window) {
      var id = 'LDTron';
      var options = {
        idAPI: id,
        absolutePath: this.path
      };
      var $iframeLDTron = $('<iframe id="' + id + '" src="' + this.getHTMLViewerURL(options) + '" width="100%" height="400px"></iframe>');
      $('body').append($iframeLDTron);
    }
    else {
      var options = '';
      options += ', "scroll":"start|stop"';
      options += ', "allowfullscreen":"true"';
      options += ', "scrolling":"no"';
      window.top.postMessage('{"action":"register", "origin":"' + window.location.href + '"' + options + '}', '*');
    }
  },
  getHTMLViewerURL: function(options) {
    var queryStringOptions = new PDFTron.WebViewer(this.options, null)._getHTML5OptionsURL() + '&' + $.param(options);
    var isMobileDevice = LDTronUtils.isMobileDevice();
    return this.path + (isMobileDevice ? 'lib/html5/LDTronMobile.html' : 'lib/html5/LDTronWeb.html') + queryStringOptions;
  },
  getGUIDPieza : function(url) {
    var arrPath = url.split('/');
    var guid = arrPath.pop();
    if (guid.length === 0) {
      guid = arrPath.pop();
    }
    return guid;
  },
  openHTMLViewer: function(id, options) {
    options = $.extend(options, {
      idAPI: id,
      piezaGUID: this.getGUIDPieza(options.absolutePath)
    });
    options.absolutePath = this.path;
    var postMessage = {
      action: "redirect",
      id: id,
      scroll: "start|stop",
      url: this.getHTMLViewerURL(options)
    };
    window.top.postMessage(JSON.stringify(postMessage), '*');
  },
  initListeners: function() {
    var me = this;
    function messageLDTronListener (event) {
      var json = JSON.parse(event.data);
      switch (json.action) {
        case "confirmRegister":
          me.openHTMLViewer(json.idAPI, json);
        break;
      }
    }
    if (window.addEventListener) { // All browsers except IE before version 9
      window.removeEventListener("message", messageLDTronListener, false);
      window.addEventListener("message", messageLDTronListener, false);
    }
    else if (window.attachEvent) { // IE before version 9
      window.attachEvent("onmessage", messageLDTronListener);
    }
  }
};