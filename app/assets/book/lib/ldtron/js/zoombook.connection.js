(function ($) {
  $(document).ready(function () {
    var connectionInterval = setInterval(function () {
      var existConnection = navigator.onLine;
      if (!existConnection) {
        $(document).trigger('ConnectionOff');
      }
      else {
        $(document).trigger('ConnectionOn');
      }
    }, 5000);

    var htmlconn =
      '<div class="popup-conn off">' +
        '<div class="msg">' +
          '<div class="logo"><i data-feather="wifi-off"></i></div>'+
          '<h1>Sin conexión de internet</h1>' +
          '<h2>No podrás acceder a los recursos externos del libro</h2>' +
          '<div class="button-close">Cerrar</div>' +
        '</div>' +
      '</div>' +
      '<div class="popup-conn on">' +
        '<div class="msg">' +
          '<div class="logo"><i data-feather="wifi"></i></div>'+
          '<h1>Conexión de internet restablecida</h1>' +
          '<h2>Ya están diponibles los recursos externos dellibro</h2>' +
          '<div class="button-close">Cerrar</div>' +
        '</div>' +
      '</div>';

    $('body').append(htmlconn);
    feather.replace();

    var connectionOff = false;
    $(document).on('ConnectionOff', function () {
      if (!connectionOff){
        connectionOff = true;
        $('.popup-conn.off').addClass('active');
      }
    });

    $(document).on('ConnectionOn', function () {
      if (connectionOff) {
        connectionOff = false;
        $('.popup-conn.on').addClass('active');
      }
    });

    $('.popup-conn .button-close').on('click', function () {
      $('.popup-conn').removeClass('active');
    });
  });


})(jQuery);