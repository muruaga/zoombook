'use strict';

// LDTron Web para Web
var LDTronWebMenu = function(menu, selected) {
  this.btMenu = menu + "ToolsButton" ;
  this.$btMenu = $('#' + this.btMenu);

  this.btOpener = menu + "Tools";
  this.$btOpener = $('#' + this.btOpener);

  this.menu = menu + "ToolsContainer";
  this.$menu = $('#' + this.menu);

  this.toolSelected = selected;
};

// Metodos LDTron para Web
LDTronWebMenu.prototype = {
    initialize: function() {
        var me = this;

         context.settings({
            click: true,
            right: true,
            minWidth: false,
            onOpen: function(){
               // for handling changes from contextual color config
               var config = me.getToolActualConfig(me.toolSelected);
               me.repaintColorSelection(config.color, config.thickness, config.opacity);
               me.selectToolFromNavBar();
            }
        });

        context.attach('#' +  this.btOpener , this.$menu[0], {translateY: 45, class: 'horizontal'});
        me.$menu.parent().addClass('horizontal');
        
        var config = me.getToolActualConfig(me.toolSelected);
        me.repaintColorSelection( config.color, config.thickness, config.opacity);
        
        me.selectTool(me.toolSelected);
        me.initEvents();
 
    },
    // UI 
    // ------------------------------
    repaintColorSelection : function(StrokeColor, StrokeThickness, Opacity ){

      var toolbar = '#' + this.menu;
      var notSelectedDot = "#505267";
      $(toolbar + ' .size-option > div').css({'background': notSelectedDot, 'opacity':1});
      $(toolbar + ' .opacity-option > div').css({'background': notSelectedDot});

      var dotSelected = $(toolbar + ' .size-option[data-thickness = '+ StrokeThickness +']').find("div");
      dotSelected.css({"background": StrokeColor, "opacity": Opacity});

      // caso surlinear
      /*if (Opacity == null){
        $(toolbar + ' .opacity-option:not(.opacity100)').css('visibility','hidden');
        $(toolbar + ' .opacity100 > div').css({'background': StrokeColor});
      }else{
        $(toolbar + ' .opacity-option').css('visibility','visible');
        dotSelected = $(toolbar + ' .opacity-option[data-opacity = "'+ Opacity +'"]').find("div");
        dotSelected.css({"background": StrokeColor});
      }*/

      $(toolbar + ' .opacity-option:not(.opacity100)').css('display','none');
      $(toolbar + ' .opacity100 > div').css({'background': StrokeColor});

      var el = this;
      $(toolbar + " .color-option").each(function(index, item){
        
        var $item = $(item);
        if ( el.rgb2hex( $item.css('background-color') ) ===  StrokeColor ){
          $item.addClass("selected");
        }else{
          $item.removeClass("selected");
        }

      });

    },
    selectToolFromNavBar: function(){

        $('.navbar span[data-toolmode]').removeClass('active');
        $('#' + this.btMenu).addClass('active');
        this.setToolMode($('#' + this.btMenu).attr("data-toolmode"));

    },
    updateOverflowButton : function(overflowTool, active) {

      $('.navbar span[data-toolmode]').removeClass('active');
      // clone the overflow button and replace the current tool button in the overflow position with this button
      var replacingOverflowButton = overflowTool.clone().attr('id', this.btMenu);
      replacingOverflowButton.removeClass('selected').removeClass('btn');
      if (active) {
        replacingOverflowButton.addClass('active');
      }
      $('#' + this.btMenu).replaceWith(replacingOverflowButton);
    },

    // viewer actions
    updateToolStyle : function(config) {
      
       var toolmodeSelect = this.toolSelected;
      if (typeof(PDFTron.WebViewer.ToolMode[toolmodeSelect]) !== 'undefined'){
          toolmodeSelect = PDFTron.WebViewer.ToolMode[toolmodeSelect];
      }
 
      var toolModeMap = readerControl.toolModeMap[toolmodeSelect];

      if (config.color) {
        toolModeMap.defaults.StrokeColor = config.color;
      }
      if (config.thickness) {
        toolModeMap.defaults.StrokeThickness = config.thickness;
      }
       if (config.opacity) {
        toolModeMap.defaults.Opacity = config.opacity;
      }

      var config = this.getToolActualConfig(this.toolSelected);
      this.repaintColorSelection(config.color, config.thickness, config.opacity);

    },

    setToolMode: function(toolmode){
         readerControl.setToolMode(toolmode);
    },
   
    // events
    initEvents: function(){

        var me = this;
        var toolbarQuery = '#' + this.menu;

        // cambiar el color
        $( toolbarQuery).on('click', '.color-option', function(event) {

            var $colorOption = $(event.target);

            $colorOption.addClass('selected');

            var config = {};
            config.color = new Annotations.Color( me.rgb2hex( $colorOption.css('background-color') ) );
            // change default and repaint
            me.updateToolStyle(config);
        });

        
        // cambiar el grossor
        $(toolbarQuery).on('click','.size-option', function(event) {

            var $sizeOption = $(event.target);

            if ($sizeOption.hasClass('dot')) {
                $sizeOption = $sizeOption.parent();
            }
           
            var config = {};
            config.thickness = $sizeOption.attr('data-thickness');
            me.updateToolStyle(config);
        });

         // cambiar la opacidad
        $(toolbarQuery).on('click','.opacity-option', function(event) {

            var $opacityOption = $(event.target);

            if ($opacityOption.hasClass('dot')) {
                $opacityOption = $opacityOption.parent();
            }
           
            var config = {};
            config.opacity = $opacityOption.attr('data-opacity');
            me.updateToolStyle(config);
        });

        $(document).on('click', '#' + this.btMenu, function(){
          me.$btOpener.trigger('click');
        });

        // cambiar la herramienta seleccionada
        $(document).on('click', toolbarQuery + ' span[data-toolmode]', function(event) {

            var $annotTool = $(event.target);
            me.selectTool($annotTool.attr('data-toolmode'));

        });

    },
    selectTool:function(selected){

       var me = this;
       var toolbarQuery = '#' + this.menu;

       var $annotTool =  $( toolbarQuery + '  span[data-toolmode = ' + selected + ']');

       $( toolbarQuery + '  span[data-toolmode]').removeClass('selected');
       $annotTool.addClass('selected');

        me.toolSelected = selected;

        me.setToolMode(me.toolSelected);
        me.updateOverflowButton($annotTool, true);

        var config = me.getToolActualConfig(me.toolSelected);
        me.repaintColorSelection(config.color, config.thickness, config.opacity);

    },
    // utils
    getToolActualConfig : function(toolmode){

      var config = {};
      var toolmodeSelect = toolmode;
      if (typeof(PDFTron.WebViewer.ToolMode[toolmode]) !== 'undefined'){
          toolmodeSelect = PDFTron.WebViewer.ToolMode[toolmode];
      }
 
      var toolModeMap = readerControl.toolModeMap[toolmodeSelect];

      if (typeof(toolModeMap.defaults) !== "undefined"){

        if (typeof(toolModeMap.defaults.StrokeColor) !== "undefined"){
            config.color = this.rgbObj2hex(toolModeMap.defaults.StrokeColor);
        }
        config.thickness =  toolModeMap.defaults.StrokeThickness;
        config.opacity = toolModeMap.defaults.Opacity;

        // supu
        if ((toolmode !== 'AnnotationCreateTextHighlight') && (typeof(config.opacity) === 'undefined')){
            config.opacity = 1;
        }

      }

      return config;

    },
    rgb2hex : function (rgb) {
      rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
      function hex(x) {
        return ("0" + parseInt(x).toString(16)).slice(-2);
      }
      return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
    },
    rgbObj2hex : function (rgb) {
      function hex(x) {
        return ("0" + parseInt(x).toString(16)).slice(-2);
      }
      return "#" + hex(rgb.R) + hex(rgb.G) + hex(rgb.B);
    }
}