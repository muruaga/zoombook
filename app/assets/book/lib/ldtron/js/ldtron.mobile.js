'use strict';

// LDTron para Mobile
var LDTronMobile = function LDMobileReader(idAPI, options) {
  this.idAPI = idAPI;
  this.options = options;
};

// Metodos LDTron para Mobile
LDTronMobile.prototype = {
  initialize: function() {
    // Instancias a clases
    var me  = this; // LDTron
    var viewer = readerControl.docViewer; // DocumentViewer

    // Elementos DOM del visor
    me.$pageHeader = $('#pageHeader');
    me.$pageFooter = $('#pageFooter');
    me.$preview = $('#preview');
    me.$annotCreatePaintButton = $('#annotCreatePaintButton');
    me.$annotPaintContext = $('#annotPaintContext');
    me.$annotCreateSelectButton = $('#annotCreateSelectButton');
    me.$annotCreateSelectCancelButton = $('#annotCreateSelectCancelButton');
    me.$annotSelectionContext = $('#annotSelectionContext');

    // Constantes
    me.DisplayModes = readerControl.pageDisplayModes; // Modos de vista de paginas
    me.HeightFooterBar = parseInt(me.$pageFooter.outerHeight(true)); // Alto barra de pie

    // Clase de pintado del visor LDTron Mobile
    $('.ldtron').addClass('mobile');

    // Funciones sobreescitas del visor nativo PDFTron
    readerControl = $.extend(readerControl, {
      createBookmarkList: me.createBookmarkListLDTron,
      setCurrentPage: this.setCurrentPageLDTron,
      onSwipe: this.onSwipeLDTron,
      onSliderMove: this.onSliderMoveLDTron,
      onSliderEnd: this.onSliderEndLDTron,
      searchText: me.searchTextLDTron,
      onTapHold: function() {},
      onTouchMove: me.onTouchMoveLDTron,
      refreshAnnotationItem: me.refreshAnnotationItemLDTron
    });

    viewer.on('documentLoaded', function() {
      // Rango de numero de paginas de la barra de navegacion
      readerControl.$slider.attr('min', me.options.initialPage );
      readerControl.$slider.attr('max', me.options.initialPage + readerControl.docViewer.getPageCount() - 1);

      me.initToolsMenus();

      // Idioma del visor
      LDTronUtils.i18n(me.options.lang);
      // Eventos para el scroll del visor
      me.initScrollEvents();
      // Pintado del visor cuando hay cambio de orientacion
      me.initOrientationEvents();
      // Pintado del visor cuando hay cambio de modo de pagina
      me.changeLayoutModeEvents();
      // Se filtran los eventos que ejecutan el zoom para corregir su ejecución repetida.
      me.filterZoomEvents();

      /*if (tableOutline !== null && tableOutline.length === 0) {
        $('#bookmarkButton, .btn-bookmarks').remove();
        $('.btn-annotations').addClass('ui-first-child');
      }*/
      // Incio de visor finalizado
      $(document).trigger('ldtronLoaded');

      readerControl.$wrapper.unbind('tap');
      readerControl.$wrapper.bind('tap', _(window.ldReader.onTapLDTron).bind(readerControl));

      setTimeout(function () {
        var $annotContainer = null, annotation = null;
        $('.annotContainer').each(function () {
          $annotContainer = $(this);
          annotation = $annotContainer.data('annot');
          $annotContainer.find('.lastmodified').text( i18n.t('annotations.created') + annotation.DateCreated.toLocaleString());
        });
      }, 500);



      var $relaseInfoPopup = $('#releasePopup');
      $relaseInfoPopup.find('[data-role="content"]').html(window.ldReader.getVersion());
      $relaseInfoPopup.popup();

      var $relaseInfo = $('<div class="ReleaseInfo" title="' + window.ldReader.getVersion() + '"></div>').text(LDTron.VERSION.RELEASE);
      $relaseInfo.appendTo($('#annotationDialog'));
      $relaseInfo.on('click', function () {
        $relaseInfoPopup.popup('open');
      });
    });

    $(document).on('heightFrameDoneLDTron', function() {
      if (me.changingOrientation) {
        me.changingOrientation = false;
        readerControl.setCurrentPage(readerControl.currentPageIndex);
      }
      setTimeout(function() {
        me.repaintPages(false);
        me.repaintAllBars(me.iFrameData);
      }, 100);
    });

    $(document).on('pageCompleted', function(e, pageIndex) {
      var pageIndexCompleted = pageIndex - 1;
      // Se adicionan los pictos en la pagina
      me.appendIconsContentData(pageIndexCompleted);
      // Se ajusta el alto del iframe
      if (readerControl.currentPageIndex === pageIndexCompleted) {
        me.fixIFrameHeight();
      }
    });

    viewer.on('pageNumberUpdated', function(e, pageNumber) {
      // Se pinta el numero de pagina segun configuracion de inicio de pagina
      me.repaintPageNumber(pageNumber);
      readerControl.$annotEditPopup.popup('close');
    });

    function goToPageLDTron() {
      // Calculo del numero segun configuracion initialPage
      var pageNum = ( parseInt(readerControl.$pageNumberInput.val(), 10) - me.options.initialPage + 1 ) || readerControl.docViewer.getCurrentPage();
      readerControl.$pageNumberInput.val('');
      var maxPage = readerControl.docViewer.getPageCount();
      pageNum = pageNum > maxPage ? maxPage : pageNum;
      // Minimo de numero de pagina
      if (pageNum < 1) {
        pageNum = 1;
      }
      // close popup before going to the page so that the virtual keyboard is hidden and the window size is correct
      readerControl.$goToPagePopup.popup('close');
      readerControl.setCurrentPage(pageNum - 1);
      window.scrollTo(0, 0);
    }

    // Reemplazo de interface nativo ir a pagina por LDTron
    me.$goToPageForm = $('#goToPageForm');
    me.$goToPageForm.replaceWith(
      '<form id="goToPageForm">' +
      '<div class="ui-input-text ui-body-a ui-corner-all ui-shadow-inset">' +
      '<input type="number" min="1" data-i18n="[placeholder]mobile.pageNumber" data-theme="a" id="pageNumberInput">' +
      '</form>'
    );
    readerControl.$pageNumberInput = $('#pageNumberInput');
    readerControl.$goToPagePopup.find('#pageNumberGoButton').on('click', goToPageLDTron);
    me.$goToPageForm.submit(function() {
      goToPageLDTron();
      return false;
    });

    // Se quitan los eventos de pasa pagina en los bordes
    $(document).on('swipeleft swiperight', '.smIcon', function(event) {
      event.stopPropagation();
      event.preventDefault();
    });

    // Se ocultan las barras cuando comienza el scroll
    $(document).on('scrollStartLDTron', function() {
      var iFrameData = me.iFrameData;
      if (typeof iFrameData !== 'undefined') {
        var topHeader =  parseInt(iFrameData.topFrame) - parseInt(iFrameData.scrollTop);
        if (topHeader < 0) {
          me.hideHeaderBar();
        }
        me.hideFooterBar();
      }
    });

    // Se muestran y ajustan el alto de las barras cuando termina el scroll
    $(document).on('scrollEndLDTron', function() {
      me.setScrolling(false);
      var iFrameData = me.iFrameData;
      if (typeof iFrameData !== 'undefined') {
        me.fixTopAllBars(iFrameData);
        me.showAllBars();
      }
    });

    // Opciones de pintado del visor
    me.customizeViewer();

    // Alto barra de herramientas
    me.headerHeight = parseInt($('#control, #pageHeader').outerHeight(true));

    // Se muestra la primera página
    me.repaintPageNumber(1);

    // Para poder editar en cajas de texto en IOS hay que poner el foco en la ventana
    if (LDTronUtils.isIOS()) {
      me.initWindowFocusEvents();
    }

    $('#deletePageAnnotationsButton').on('click', function(ev) {

      var pages = readerControl.getVisibleZoomedPages();
      for(var i = 0; i < pages.length; i++)
      {
          pages[i] += 1;
      }

      var messageConfirm = i18n.t("annotations.deleteAllConfirm");

      if (pages.length > 1){
        messageConfirm = i18n.t("annotations.deleteAllConfirm2");
      }

      var confirm = {
        "mess": messageConfirm, 
        "btyes": i18n.t("notesPanel.deleteAll"), 
        "btno": i18n.t("notesPanel.cancel"), 
        "bt": ev.target,
        "callback":function(){
                        deletePageAnnotations(pages);
                    }
        }
        
        $(document).trigger('LDTronPopupConfirm', confirm);

    });

    readerControl.$annotEditButtons.find('#editNoteButton').on('click', function () {
      var selected = readerControl.annotationManager.getSelectedAnnotations();
      if (!selected || selected.length !== 1) {
        return;
      }
      var annotation = selected[0];

      var container = readerControl.getAnnotationNoteContainer(annotation);
      container.find('#btnEdit, #btnDelete').hide();
      container.find('#btnSave, #btnClose').show();
    });

    // Se ajusta al tamaño del visor
    me.resize();
  },

  onTapLDTron: function(evt) {
    readerControl.onTap(evt);

    var docX = window.innerWidth;
    var pageX = evt.pageX;
    var pageY = evt.pageY;

    var hotspotWidth = readerControl.getTapHotSpotWidth();
    var menuBarBufferHeight = 40;

    // don't check for hotspot tapping if we're in annotation mode because the triggered swipe is going to be blocked anyway
    // also if we return false it will stop the event from propagating to the click handler on the stroke color picker
    if (!readerControl.annotMode) {
      if (pageY > menuBarBufferHeight) {
        if (pageX < hotspotWidth) {
          evt.stopPropagation();
        }
        else if ((docX - pageX) < hotspotWidth) {
          evt.stopPropagation();
        }
      }
    }
  },

  customizeViewer: function() {
    var me = this;

    // Se deshabita modo 2 pagina si por configuracion se indica
    if (me.options.twoPages === '0') {
      $('#displayDouble').prop('disabled', true);
    }

    // Si quitan los botones de anotaciones si por configuracion se deshabilitan  las anotaciones
    if (me.options.draw === '0') {
      me.$annotCreatePaintButton.remove();
      me.$annotCreateSelectButton.remove();
    }

    // Se muesra por defecto a baja resolucion.
    window.utils.setCanvasMultiplier(1);

    // Se adiciona el titulo
    me.appendTitle(me.options.pdfVisibleTitle);

    // Se adiciona el logo
    if (this.options.sello !== '0') {
      me.appendLogo(me.options.sello);
    }

    // Carpeta personal
    if (tablePersonal !== null && tablePersonal.length > 0) {
      me.appendPersonalFolders(tablePersonal);
    }
    else {
      $('.goToPersonalPanelButton').remove();
    }

    // Opcion de filtro de iconos del profesor
    me.showProfesorIcons = LDTronUtils.existProfesorIcons();
    if (me.showProfesorIcons) { // Si existe ictos del prfesor se muestra la opcion
      me.showTeacherOption();
    }
  },

  filterZoomEvents: function() {
    if (!window.utils.ie && !window.utils.isEdge) {
      readerControl.$wrapper.unbind('dblclick');
    }
    else {
      readerControl.$wrapper.unbind('doubletap');
    }
  },

  // Control modo de vista para ajustar el alto del Iframe
  changeLayoutModeEvents: function() {
    var me = this;
    $('#displaySingle, #displayDouble').unbind('click');
    $('#displaySingle').on('click', function() {
      me.changeLayoutMode(me.DisplayModes.Single);
    });
    $('#displayDouble').on('click', function() {
      me.changeLayoutMode(me.DisplayModes.Double);
    });
  },

  initToolsMenus: function(){

    var me = this;

    var menuSearch = new LDTronMobileMenu('btSearch');
    menuSearch.initialize();

    var menuPaint = new LDTronMobileMenu('btAnnotCreatePaintButton', 'AnnotationCreateFreeHand');
    menuPaint.initialize();

    var menuText = new LDTronMobileMenu('btAnnotCreateButton', 'AnnotationCreateTextUnderline');
    menuText.initialize();

    var menuSelect = new LDTronMobileMenu('btAnnotCreateSelectButton', 'Pan');
    menuSelect.initialize();

    var toolsButtonsQuery = '#defaultMenuContext a[data-toolmode]';
    $(document).on('click', toolsButtonsQuery, function(event) {
      
      var btSel = $(event.target).closest("a");
      if (btSel.hasClass('active')){
        me.setDefautToolMode();
      }else{
        me.setToolModeDefaultMenu(btSel);
      }

    });

    $(document).on('repaintTitle', me.repaintTitle );
    $(document).on('selectPanTool', me.setDefautToolMode );

  },

  setToolModeDefaultMenu: function(bt){

    $('#defaultMenuContext  a').removeClass('active');
    bt.addClass('active');

    readerControl.endAnnotationQuickMode();
    readerControl.setToolbarContext("tools");
    readerControl.setMenuTapToggle(false);
    readerControl.annotMode = true;
    readerControl.setToolMode(bt.attr('data-toolmode'));

  },

  setDefautToolMode: function(){

    var toolsButtonsQuery = '#defaultMenuContext a[data-toolmode]';
    $(toolsButtonsQuery).removeClass('active');
    $("#btAnnotCreateSelectButton").addClass('active');

     readerControl.docViewer.clearSelection();
     readerControl.annotationManager.deselectAllAnnotations();
     readerControl.$defaultMenuContext.fadeIn('fast');

     readerControl.setToolbarContext("top");
     readerControl.setMenuTapToggle(true);
     readerControl.annotMode = false;

     readerControl.setToolMode('Pan');

  },

/* Todo el codigo de menus y tool tranaladado en ldtron.mobile.menus.js
  // Herramienta de pintado
  annotPaintContextBehavior: function() {
    var me = this;
    me.$annotCreatePaintButton.click(function() {
      readerControl.endAnnotationQuickMode();
      readerControl.$defaultMenuContext.hide();
      if (readerControl.readOnly) {
        readerControl.$annotPaintContext.find('a').hide();
        readerControl.$annotPaintContext.find('#annotCreatePaintCancelButton').show();
      }
      me.$annotPaintContext.fadeIn('fast');
      readerControl.setToolbarContext("tools");
      readerControl.setMenuTapToggle(false);
      me.repaintTitle();
    }).show();

    $('#annotCreatePaintCancelButton').click(function() {
      readerControl.docViewer.clearSelection();
      readerControl.annotationManager.deselectAllAnnotations();
      readerControl.$defaultMenuContext.fadeIn('fast');
      me.$annotPaintContext.hide();
      readerControl.setToolbarContext("top");
      readerControl.setMenuTapToggle(true);
      me.repaintTitle();
    });

    me.$annotPaintContext.on('click', function(e) {
      var $buttonPressed = $(e.target).closest('a');
      var buttonId = $buttonPressed.attr('id');

      // simulate toggling: if a tool is already selected, de-select and switch to PanEdit mode.
      if ($buttonPressed.hasClass('active')) {
        readerControl.docViewer.setToolMode(readerControl.defaultToolMode);
        readerControl.annotMode = false;
        return;
      }

      var toolMode = readerControl.buttonIdsToToolModes[buttonId];
      if (!_.isUndefined(toolMode)) {
        // anything other than the cancel button is pressed
        readerControl.docViewer.setToolMode(toolMode);
        readerControl.annotMode = true;
      }
      if (buttonId === 'annotCreatePaintCancelButton') {
        readerControl.docViewer.setToolMode(readerControl.defaultToolMode);
        readerControl.endAnnotationQuickMode();
        readerControl.$defaultMenuContext.show();
        readerControl.setToolbarContext("top");
        me.$annotPaintContext.hide();
      }
      readerControl.closeEditPopup();
    });

    readerControl.docViewer.on('toolModeUpdated', function(e, newToolMode, oldToolMode) {
      var allButtons = me.$annotPaintContext.find('a');
      allButtons.removeClass('active');
      var toolName = '';
      for (var key in readerControl.toolModeMap) {
        if (newToolMode === readerControl.toolModeMap[key]) {
          toolName = key;
          break;
        }
      }
      if (toolName) {
        me.$annotPaintContext.find("[data-toolmode=" + toolName + "]").addClass('active');
      }
    });

  },

  // Herramienta de seleccion
  annotSelectionContextBehavior: function() {
    var me = this;
    me.$annotCreateSelectButton.click(function() {
      readerControl.endAnnotationQuickMode();
      readerControl.$defaultMenuContext.hide();
      if (readerControl.readOnly) {
        readerControl.$annotSelectionContext.find('a').hide();
        readerControl.$annotSelectionContext.find('#editAnnotButton').show();
        readerControl.$annotSelectionContext.find('#annotCreateSelectCancelButton').show();
      }
      me.$annotSelectionContext.fadeIn('fast');
      readerControl.setToolbarContext("tools");
      readerControl.setMenuTapToggle(false);

      var toolMode = readerControl.buttonIdsToToolModes[$('#editAnnotButton').attr('id')];
      if (!_.isUndefined(toolMode)) {
        // anything other than the cancel button is pressed
        readerControl.docViewer.setToolMode(toolMode);
        readerControl.annotMode = true;
      }
      me.repaintTitle();
    }).show();

    me.$annotCreateSelectCancelButton.click(function() {
      readerControl.docViewer.clearSelection();
      readerControl.annotationManager.deselectAllAnnotations();
      readerControl.endAnnotationQuickMode();
      readerControl.$defaultMenuContext.fadeIn('fast');
      readerControl.setToolbarContext("top");
      readerControl.setMenuTapToggle(true);
      readerControl.closeEditPopup();
      me.$annotSelectionContext.hide();
      me.repaintTitle();
    });

    readerControl.docViewer.on('toolModeUpdated', function(e, newToolMode, oldToolMode) {
      var allButtons = me.$annotSelectionContext.find('a');
      allButtons.removeClass('active');
      var toolName = '';
      for (var key in readerControl.toolModeMap) {
        if (newToolMode === readerControl.toolModeMap[key]) {
          toolName = key;
          break;
        }
      }
      if (toolName) {
        me.$annotSelectionContext.find("[data-toolmode=" + toolName + "]").addClass('active');
      }
    });
  },
*/
  // Elimina el espacio superior en blanco de las paginas
  repaintPages: function(animate) {
    readerControl.lastHeight = -1;
    readerControl.shouldRerender = true;
    if (animate) {
      readerControl.c.$e.addClass('animated');
    }
    if (readerControl.c) {
      readerControl.clearTransform(readerControl.c.$e.find('canvas'));
    }
    readerControl.onTouchEnd({
      originalEvent: { touches: [] }
    }, animate);
  },

  // Muestra el titulo al ancho maximo posible teniendo en cuenta las herramientas
  repaintTitle:  function() {
    var widthContainer = 0;
    var visible = $("#pageHeader > div:visible");
    widthContainer = visible.outerWidth(true);

    // el menu por defecto puede no haberse renderisado todavía
    if (widthContainer < 80){
      widthContainer = 0;
      visible.find('.mobile-button, #smLogo').each(function(index, item){
        widthContainer += $(item).outerWidth(true);
      });
    }

    var maxidth = $('body').width() - widthContainer - 40;     
    $('#headerTitle').css('max-width', maxidth);

  },

  // Pone el foco a la ventana en cualquier evento de teclado o toque
  initWindowFocusEvents: function() {
    document.addEventListener('keydown', function (e) {
      window.focus();
    });
    document.addEventListener('touchend', function (e) {
      window.focus();
    });
  },

  // Ajusta el alto cuando hay cambio de modo
  changeLayoutMode: function(pageDisplay) {
    readerControl.pageDisplay = pageDisplay;
    readerControl.docViewer.trigger('displayModeUpdated');
    this.fixIFrameHeight();
  },

  // Oculta la barra de cabecera
  hideHeaderBar: function() {
    this.$pageHeader.addClass('hiddenAnimated');
  },

  // Oculta la barra de navegacion
  hideFooterBar: function() {
    return /*** SE QUITA CUANDO SE CORRIGA EL VIEWPORT DE LA CARCASA***/
    this.$pageFooter.addClass('hiddenAnimated');
    // Tambien se oculta el marco de preview
    this.$preview.css("opacity", "0");
    this.$preview.css("z-index", "0");
  },

  // Oculta todas las barras
  hideAllBars: function() {
    this.hideHeaderBar();
    this.hideFooterBar();
  },

  // Muestra todas las barras
  showAllBars: function() {
    this.$pageHeader.removeClass('hiddenAnimated');
    this.$pageFooter.removeClass('hiddenAnimated');
    this.repaintTitle();
  },

  // Posicionamiento vertical de la barra cabecera
  fixTopHeaderBar: function(iFrameData) {
    var topHeader = parseInt(iFrameData.topFrame) - parseInt(iFrameData.scrollTop);
    if (topHeader < 0) {
      this.$pageHeader.addClass('scrolling').css('top', Math.abs(topHeader) + 'px');
    }
    else {
      this.$pageHeader.removeClass('scrolling').css('top', '');
    }
  },

  // Posicionamiento vertical de la barra de navegacion
  fixTopFooterBar: function(iFrameData) {
    return /*** SE QUITA CUANDO SE CORRIGA EL VIEWPORT DE LA CARCASA***/
    var bottomFooter = window.innerHeight - parseInt(iFrameData.height) + parseInt(iFrameData.topFrame) - parseInt(iFrameData.scrollTop);
    if (bottomFooter <= 0) {
      bottomFooter = 0;
    }
    this.$pageFooter.removeClass('scrolling').css('bottom', bottomFooter);
    this.$preview.css('bottom', bottomFooter + this.HeightFooterBar);
  },

  // Posicionamiento vertical de las 2 barras
  fixTopAllBars: function(iFrameData) {
    this.fixTopHeaderBar(iFrameData);
    this.fixTopFooterBar(iFrameData);
  },

  // Repintado vertical de las 2 barras
  repaintAllBars: function(iFrameData) {
    this.fixTopAllBars(iFrameData);
    this.showAllBars();
  },

  // Posicionamiento del la busqueda de frases
  searchTextLDTron: function(pattern, searchUp) {
    var me  = this;
    var pageResults = [];
    var Text = XODText;
    if (pattern !== '') {
      var mode = readerControl.docViewer.SearchMode.e_page_stop | readerControl.docViewer.SearchMode.e_highlight;
      if (searchUp) {
        mode = mode | readerControl.docViewer.SearchMode.e_search_up;
      }
      readerControl.docViewer.textSearchInit(pattern, mode, false,
        function(result) { // onSearchCallback
          if (result.resultCode === Text.ResultCode.e_found) {
            pageResults.push(result.page_num);
            readerControl.docViewer.displaySearchResult(result, _(readerControl.jumpToFound).bind(readerControl));
            $(document).trigger('scrollToResult', result, me.headerHeight);
          } else if (result.resultCode === Text.ResultCode.e_done) {
            alert(i18n.t("endOfDocument"));
          }
        }
      );
    }
  },

  // Pintado de numero de pagina en la caja de de navegacion a partir de initialPage
  repaintPageNumber : function(pageNumber) {
    readerControl.$slider.val(LDTronUtils.getRealPageByNumber(pageNumber, this.options.initialPage)).slider("refresh");
  },

  // Eventos para hacer scroll
  initScrollEvents: function() {
    var me = this;

    me.setTouchWheel(false);

    // Los eventos de pdftron no se ejecutan si hay scroll del visor ldtron
    readerControl.$viewerPage.unbind('touchstart');
    readerControl.$viewerPage.bind('touchstart', function(event) {
      if (!me.isTouchWheel() && !me.isScrolling()) {
        readerControl.onTouchStart(event);
      }
      // Maximo zoom permido el mismo que se calcula al hacer doble clic
      var DOUBLE_TAP_ZOOM_SCALE = 3;
      me.maxPageZoom = DOUBLE_TAP_ZOOM_SCALE * readerControl.getFitPageZoom(readerControl.currentPageIndex);
    });

    // Permitir  ejecucion de eventos de PDFTron, solo si no hay scroll
    readerControl.$viewerPage.unbind('touchmove');
    readerControl.$viewerPage.bind('touchmove', function(event) {
      if (!me.isTouchWheel() && !me.isScrolling()) {
        readerControl.onTouchMove(event);
      }
    });
    readerControl.$viewerPage.unbind('touchend');
    readerControl.$viewerPage.bind('touchend', function(event) {
      if (!me.isTouchWheel() && !me.isScrolling()) {
        readerControl.onTouchEnd(event);
      }
    });

    // Posicion inicial del scroll
    var start = {x:0, y:0};
    // Inicio del scroll pulsando con un solo dedo
    readerControl.$viewerPage.bind('touchstart', function(event) {
      var touches = event.originalEvent.touches;
      if (touches.length === 1) {
        var touch = touches[0];
        start.x = touch.clientX;
        start.y = touch.clientY;
      }
      me.setScrolling(false);
    });
    // Fin del scroll
    readerControl.$viewerPage.bind('touchend', function(event) {
      if (me.isTouchWheel()) {
        event.stopImmediatePropagation();
        event.preventDefault();
        setTimeout(function() {
          me.setTouchWheel(false);
          me.setScrolling(false);
        }, 500);
      }
    });
    // Ejecucion del scroll
    readerControl.$viewerPage.bind('touchmove', function(event) {
      // No se ejecuta si se esta usando la heramienta de anotaciones
      if (readerControl.annotMode) {
        me.setTouchWheel(false);
        me.setScrolling(false);
        return;
      }
      var touches = event.originalEvent.touches;
      if (touches.length === 1) {
        var touch = touches[0];
        var deltaX = start.x - touch.clientX;
        var deltaY = start.y - touch.clientY;
        // Se inicia el scroll a partir del 5to pixels moviendo verticalmente
        var diffDeltaY = 5;
        if (!me.isTouchWheel() && Math.abs(deltaX) < diffDeltaY && Math.abs(deltaY) > diffDeltaY) {
          me.setTouchWheel(true);
        }
        // Ejecucion del scroll con el API de integracion
        if (me.isTouchWheel()) {
          event.stopPropagation();
          if (!me.isScrolling()) {
            me.setScrolling(true);
            start.y = touch.clientY + deltaY;
            LDTronUtils.scrollTop(me.idAPI, deltaY);
          }
        }
      }
    });
  },

  setTouchWheel: function(value) {
    readerControl.setReadOnly(value);
    this.touchWheel = value;
  },

  isTouchWheel: function() {
    return this.touchWheel;
  },

  setScrolling: function(value) {
    this.scrolling = value;
  },

  isScrolling: function() {
    return this.scrolling;
  },

  initOrientationEvents: function() {
    var me = this;
    $(window).bind('orientationchange', function(event)  {
      me.changingOrientation = true;
      readerControl.lastHeight = -1;
      me.hideAllBars();
      me.repaintTitle();
      me.fixIFrameHeight();
      if (!me.$pageHeader.hasClass('ui-fixed-hidden')) {
        me.$pageHeader.addClass('ui-fixed-hidden');
        me.$pageFooter.addClass('ui-fixed-hidden');
        setTimeout(function () {
          me.$pageHeader.removeClass('ui-fixed-hidden');
          me.$pageFooter.removeClass('ui-fixed-hidden');
        }, 100);
      }
    });
  },

  // Ajusta el alto del Iframe al alto de la pagina
  fixIFrameHeight: function() {
    var me = this;
    var page = readerControl.doc.getPageInfo(readerControl.currentPageIndex);
    var windowZoom =  parseFloat(parseFloat(me.iFrameData.zoom).toFixed(1));
    var viewerWidth = page.width;
    if (LDTronUtils.isIOS()) {
      viewerWidth = page.width * windowZoom;
    }
    if (readerControl.pageDisplay === me.DisplayModes.Double) {
      viewerWidth *= 2;
    }
    var bodyWidth = $('body').width();
    var prop = bodyWidth / viewerWidth;
    var iframeHeight = Math.ceil(page.height*prop);
    if (me.viewerHeight !== iframeHeight) {
      me.hideAllBars();
      me.viewerHeight = iframeHeight;
      setTimeout(function() {
        LDTronUtils.fixHeightParent(me.idAPI, me.viewerHeight);
      }, 100);
    }
    else {
      me.repaintAllBars(me.iFrameData);
    }
  },

  resize: function() {
    LDTronUtils.resize(this.idAPI);
  },

  // Adiciona los pictos a una pagina
  appendIconsContentData: function(pageIndex) {
    var me = this;
    if (contentData && contentData.length > 0) {
      // SM - Enriquecimiento Movile version
      var container = $('#pageContainer' + pageIndex).parent();
      var pageTransform = readerControl.displayMode.getPageTransform(pageIndex);
      var pageOffset = readerControl.displayMode.getPageOffset(pageIndex);
      var zoom = readerControl.docViewer.getPageZoom(pageIndex);
      var offset = {
        left: pageOffset.x - pageTransform.x,
        top: pageOffset.y - pageTransform.y
      };
      container.find("img.smIcon[page = " + pageIndex + " ]").remove();
      $.each(contentData, function (i, icon) {
        var pagePos = pageIndex + 1;
        if (icon.page === pagePos && (icon.person === '0' || (icon.person === '1' && me.showProfesorIcons))) {
          var variacion_z = 1.333;
          var widthImg = parseInt(42 * zoom) + 'px';
          var leftImg = parseInt(icon.x * variacion_z);
          var topImg = parseInt(icon.y * variacion_z);
          var location = readerControl.displayMode.pageToWindow({
            x: leftImg,
            y: topImg
          }, pageIndex);
          var person = (parseInt(icon.person) === 0) ? 'a' :'p';
          var imgSrc = me.options.path + 'lib/ldtron/imgs/' + icon.icon + '_' + person + '.png';
          var image = $('<img class="smIcon" page="' + pageIndex + '" src="' + imgSrc + '">')
            .css({
              'position': 'absolute',
              'width': widthImg,
              'z-index': 100,
              'top': parseInt(location.y + offset.top) + 'px',
              'left': parseInt(location.x + offset.left) + 'px'
            }).on('tap', function (event) {
              event.stopPropagation();
              event.preventDefault();
            }).on('touchstart', function (event) {
              event.stopPropagation();
              event.preventDefault();
              LDTronContent.show(me, icon, image);
            }).on('touchend', function (event) {
              event.stopPropagation();
              event.preventDefault();
            });

          image.attr('title', icon.title);
          container.append(image);
        }
      });
    }
  },

  // Crea los indices segun canfiguracion tableOutline
  createBookmarkListLDTron: function(level) {
    var ldReader = window.ldReader;
    var $bookmarkList = readerControl.$bookmarkList;
    var bookmarkString = '';
    if (tableOutline !== null && tableOutline.length > 0) {
      var bm, pageData, liContent, newLiString;
      for (var j = 0; j < tableOutline.length; j++) {
        bm = tableOutline[j];
        pageData = parseInt(bm.page) - parseInt(ldReader.options.initialPage) + 1;
        liContent =
          '<a href="#viewerPage" class="unselectable" data-transition="none" data-bookmark-page="' + pageData + '">' +
            '<span>' + bm.title + '</span>' +
          '</a>';
        newLiString =
          '<li style="-webkit-transform: translateZ(0)" data-icon="false" class="bookmark-item"  data-pagenumber="' + pageData + '">' + liContent + '</li>';
        bookmarkString += newLiString;
      }
    }
    else {
      bookmarkString = '<li data-i18n="mobile.bookmarks.noBookmarks"></li>'
    }
    $bookmarkList.html(bookmarkString);
    $bookmarkList.i18n();
    $bookmarkList.listview();
    $bookmarkList.listview('refresh');
  },

  // Modifica la funcion nativa para mostrar el numero de pagina segun initialPage en configurado
  setCurrentPageLDTron: function(pageIndex) {
    var ldReader = window.ldReader;
    var reader = readerControl;
    reader.transformOffset = null;
    // clear any transforms on the canvases
    if (reader.c) {
      reader.clearTransform(reader.c.$e.find('canvas'));
    }
    reader.setCurrentToMinZoom();
    if (reader.zoomedWrapper) {
      reader.zoomAbort();
    }
    // only close the edit popup if we are actually changing pages
    if (pageIndex !== reader.currentPageIndex) {
      reader.closeEditPopup();
    }
    reader.currentPageIndex = pageIndex;
    reader.updateCurrentZooms(reader.currentPageIndex);
    var wrapperIndex = reader.pageToWrapper(pageIndex);
    reader.docViewer.removeContent();
    var vis = [];
    reader.addPages(vis, wrapperIndex);
    reader.c = reader.createPageWrapper(wrapperIndex, 0);
    reader.$viewer.append(reader.c.$e);
    // if there should be a next page
    if (wrapperIndex < reader.numberOfWrappers() - 1) {
      var nextWrapperIndex = wrapperIndex + 1;
      reader.addPages(vis, nextWrapperIndex);
      reader.n = reader.createPageWrapper(nextWrapperIndex, window.innerWidth);
      reader.$viewer.append(reader.n.$e);
    } else {
      reader.n = null;
    }
    // if there should be a previous page
    if (wrapperIndex > 0) {
      var prevWrapperIndex = wrapperIndex - 1;
      reader.addPages(vis, prevWrapperIndex);
      reader.p = reader.createPageWrapper(prevWrapperIndex, -window.innerWidth);
      reader.$viewer.append(reader.p.$e);
    } else {
      reader.p = null;
    }
    reader.docViewer.updateView(vis, pageIndex);
    // LDTron - Numero de pagina a partir de la opcion initialPage
    reader.$slider.val((pageIndex + ldReader.options.initialPage)).slider("refresh");
  },

  // Muestra el numero de pagina segun initialPage en configurado cuando se cambia de pagina usando la barra de navegacion
  onSwipeLDTron: function(evt) {
    var ldReader = window.ldReader;
    var reader = readerControl;
    if (reader.isInToolbar(evt.target)
      || reader.isSliding
      || reader.annotMode
      || reader.isPinching
      || reader.isZoomedIn()
      || reader.recentlyZooreaderd
      || reader.isWidgetTargetType(evt.target.type)) {
      return;
    }
    var direction;
    if (evt.type === 'swiperight' && reader.p) {
      direction = -1;
      reader.offsetSwipe--;
    } else if (evt.type === 'swipeleft' && reader.n) {
      direction = 1;
      reader.offsetSwipe++;
    } else {
      return;
    }
    reader.unZoomWrapper();
    var currentPageIndex = reader.docViewer.getCurrentPage() - 1;
    var wrapperIndex = reader.pageToWrapper(currentPageIndex) + reader.offsetSwipe;
    var pageIndex = reader.wrapperToPage(wrapperIndex);
    reader.currentPageIndex = pageIndex;
    reader.updateCurrentZooms(reader.currentPageIndex);
    var wpToRemove = null;
    var tmpWp = reader.c;
    if (direction === 1) {
      reader.c = reader.n;
      wpToRemove = reader.p;
    } else {
      reader.c = reader.p;
      wpToRemove = reader.n;
    }

    reader.vWOffset = reader.vWOffset - direction * window.innerWidth;
    reader.$viewerWrapper.stop();
    reader.$viewerWrapper.addClass('animated');
    reader.transform(reader.$viewerWrapper, reader.vWOffset, 0);
    reader.vwxPos = reader.vWOffset;

    var nextWrapperIndex = wrapperIndex + direction;
    var prevWrapperIndex = wrapperIndex - direction;
    // Remove the previous page.
    if (wpToRemove) {
      wpToRemove.$e.remove();

      // if a page is being removed then we should notify document viewer the visible pages have changed
      // but we also don't want to rerender yet
      reader.docViewer.updateVisiblePages();
    }

    var wpToAdd = null;
    // Append the next page.
    if (nextWrapperIndex >= 0 && nextWrapperIndex < reader.numberOfWrappers()) {
      var offset = direction * window.innerWidth;
      wpToAdd = reader.createPageWrapper(nextWrapperIndex, offset);
    }
    if (direction === 1) {
      reader.n = wpToAdd;
      reader.p = tmpWp;
      if (reader.n) {
        reader.$viewer.append(reader.n.$e);
      }
    } else {
      reader.p = wpToAdd;
      reader.n = tmpWp;
      if (reader.p) {
        reader.$viewer.prepend(reader.p.$e);
      }
    }

    // LDTron - control pagina
    reader.$slider.val(LDTronUtils.getRealPageByIndex(pageIndex, ldReader.options.initialPage)).slider("refresh");

    clearTimeout(reader.swipeTimeout);
    reader.swipeTimeout = setTimeout(function() {
      reader.offsetSwipe = 0;
      var vis = [];
      reader.addPages(vis, wrapperIndex);
      if (nextWrapperIndex >= 0 && nextWrapperIndex < reader.numberOfWrappers()) {
        reader.addPages(vis, nextWrapperIndex);
      }
      if (prevWrapperIndex >= 0 && prevWrapperIndex < reader.numberOfWrappers()) {
        reader.addPages(vis, prevWrapperIndex);
      }
      reader.docViewer.updateView(vis, pageIndex);
    }, 250);
  },

  // Muestra el numero de pagina segun initialPage en configurado cuando se cambia de pagina usando la barra de navegacion
  onSliderMoveLDTron: function() {
    var ldReader = window.ldReader;
    var reader = readerControl;
    if (reader.isSliding === false) {
      return;
    }
    var pageNumber = reader.$slider.get(0).value;
    var div = $('#textdiv');
    div.attr('data-i18n', 'mobile.thumbnailPageNumber');
    // need to use the data function instead of .attr('data-i18n-options') or else it will be cached and won't update
    div.data('i18n-options', {
      "current": parseInt(pageNumber, 10), // LDTron - control pagina
      "total": LDTronUtils.getRealPageByNumber(reader.nPages, ldReader.options.initialPage) // LDTron - control pagina
    });
    div.i18n();
    if (!reader.hasThumbs) {
      return;
    }
    clearTimeout(reader.getThumbnailTimeout);
    reader.getThumbnailTimeout = setTimeout(function() {
      // LDTron - control pagina
      var pageIndex = reader.$slider.get(0).value - ldReader.options.initialPage;
      var thumbContainer = reader.$thumbContainer.get(0);
      // try to cancel the last requested thumbnail
      if (reader.lastRequestedThumbnail) {
        reader.doc.cancelLoadThumbnail(reader.lastRequestedThumbnail);
      }
      reader.lastRequestedThumbnail = reader.doc.loadThumbnailAsync(pageIndex, function(thumb) {
        var ratio, width, height;
        if (thumb.width > thumb.height) {
          ratio = thumb.width / 150;
          height = thumb.height / ratio;
          width = 150;
        } else {
          ratio = thumb.height / 150;
          width = thumb.width / ratio;
          height = 150;
        }
        thumb.style.width = width + "px";
        thumb.style.height = height + "px";
        while (thumbContainer.hasChildNodes()) {
          thumbContainer.removeChild(thumbContainer.firstChild);
        }
        reader.$thumbContainer.css("width", width + "px");
        reader.$thumbContainer.css("height", height + "px");
        reader.$preview.css("width", width + "px");
        reader.$preview.css("height", height + 17 + "px");
        reader.$thumbContainer.prepend(thumb);
        reader.lastRequestedThumbnail = null;
      });
    }, 15);
  },

  // Muestra el numero de pagina segun initialPage en configurado cuando se cambia de pagina usando la barra de navegacion
  onSliderEndLDTron: function() {
    var ldReader = window.ldReader;
    var reader = readerControl;
    if (!reader.isSliding) {
        return;
    }
    reader.isSliding = false;

    // LDTron - control pagina
    var pageNumber = reader.$slider.get(0).value - ldReader.options.initialPage + 1;
    if (pageNumber !== reader.docViewer.getCurrentPage()) {
      reader.setCurrentPage(pageNumber - 1);
    }

    reader.$preview.css("opacity", "0");
    reader.$preview.css("z-index", "0");
    reader.$slider.slider("refresh");
  },

  getLayoutMode: function() {
    return readerControl.pageDisplay;
  },

  setLayoutMode: function(pageDisplay) {
    var toPageDisplay = this.DisplayModes.Single;
    switch (pageDisplay) {
      case this.DisplayModes.Single:
      case this.DisplayModes.Double:
        toPageDisplay = pageDisplay;
      break;
      default:
        var defaultDoblePagina = (this.options.twoPages === '1' && this.options.defaultPages === "2");
        toPageDisplay = (defaultDoblePagina) ? this.DisplayModes.Double : this.DisplayModes.Single;
      break;
    }
    readerControl.pageDisplay = toPageDisplay;
    this.resizeControl();
  },

  resizeControl: function () {
    readerControl.lastHeight = -1; // Se fuerza a que se ejecute el resize
    readerControl.onResize();
  },

  // Adiciona el titulo al barra cabera
  appendTitle: function (title) {
    var isExpanded = false;
    var $title = $('<span id="headerTitle" title="' + title + '">' +  title + '</span>');
    $title.appendTo(this.$pageHeader)
      .on('touchstart', function(event) {
        $title.addClass('expanded');
        isExpanded = true;
        setTimeout(function() {
          if ($title.hasClass('expanded') && isExpanded) {
            isExpanded = false;
            $title.removeClass('expanded');
          }
        }, 5000); // En 5 segundos se quita el modo expandido
      })
      .on('touchend', function(event) {
        isExpanded = false;
        $title.removeClass('expanded');
      });

     this.repaintTitle();
  },

  // Adiciona el logo a la barra de navegacion
  appendLogo: function(sello) {
    //$('#defaultMenuContext').append('<img id="smLogo" src="' + this.options.path + 'lib/ldtron/imgs/logos/' + sello + '.png"/>');
    $('#defaultMenuContext').append('<div style="display:inline-block; width:5px;"/>');
  },

  // Adicona la herramieta de carpetas personales
  appendPersonalFolders: function(tablePersonal) {
    $('.goToPersonalPanelButton').on('click', function() {
      readerControl.changeVisiblePage('personalDialog');
    });

    var $tabPersonal = $('<a id="personalListButton" href="#personalDialog" data-transition="none" class="mobile-button">' +
                      '<span class="glyphicons personal"></span>' +
                      '</a>');
    //$('#annotListButton').after($tabPersonal);

    $('#personalListButton').show();

    var me = this;
    var $personalList = $('#personalList');
    var personalString = '';
    if (tablePersonal === null || tablePersonal.length < 1) {
      personalString = '<li data-i18n="mobile.personal.noPersonals"></li>';
      $personalList.html(personalString);
      $personalList.i18n();
      $personalList.listview();
      $personalList.listview('refresh');
      return;
    }
    $.each(tablePersonal, function (index, elem) {
      var childCount = 0;
      var liContent = ('<span>' + elem.title + '</span>');
      var newLiString = '<li style="-webkit-transform: translateZ(0)" data-icon="false" class="bookmark-item personalItem" index="' + index + '">' + liContent + '</li>';
      personalString += newLiString;
    });

    $personalList.html(personalString);
    $personalList.i18n();
    $personalList.listview();
    $personalList.listview('refresh');

    $('.personalItem').on('click', function(event) {
      var index = $(this).attr('index');
      var content = {type: 'document',src: tablePersonal[index].doc}
      LDTronContent.show(me, content, this);
    });
  },

  // Adiciona la opcion de mostrar/ocultar los picto del profesor
  showTeacherOption: function() {
    var me = this;
    $('#teacherOptions').show();
    $('#cbShowTeacherIcons').change(function() {
      me.showProfesorIcons = $(this).is(":checked");
      $("[id^='pageWidgetContainer']").each(function() {
        var $page = $(this);
        var pageIndex = parseInt($page.attr('id').split('pageWidgetContainer')[1]);
        me.appendIconsContentData(pageIndex);
      });
    }).prop('checked', true);
  },

  // Se soobrescribe este evento para controlar el maximo zoom permitido
  onTouchMoveLDTron: function(evt) {
    var me = readerControl;

    me.restartTouchTimeout();

    var touch0;
    var touch1;
    // don't pan in annotation create mode
    if (me.isInToolbar(evt.target) || me.isSliding || me.annotMode || me.isWidgetTargetType(evt.target.type)) {
      return;
    }

    var width = me.c.$e.width();
    var height = me.c.$e.height();

    if (evt.originalEvent.touches.length === 1 && !me.isPinching) {
      touch0 = evt.originalEvent.touches[0];
      me.$viewerWrapper.removeClass('animated');
      var scrollX = me.oldTouch.x - touch0.clientX;
      var scrollY = me.oldTouch.y - touch0.clientY;

      if (!me.isZoomedIn()) {
        // Perform horizontal scrolling.

        me.vwxPos = me.vwxPos - scrollX;
        me.transform(me.$viewerWrapper, me.vwxPos, 0);
      } else {
        // Scrolled the zoomed in wrapper.

        me.c.tY = me.c.tY - scrollY;
        me.c.tX = me.c.tX - scrollX;
        me.transform(me.c.$e, me.c.tX, me.c.tY, me.newScale);

        me.shouldRerender = true;
        me.cancelPageRenders();
      }

      me.oldTouch.x = touch0.clientX;
      me.oldTouch.y = touch0.clientY;

    } else if (evt.originalEvent.touches.length > 1) {
      if (me.isPageScrolled()) {
          return;
      }

      touch0 = evt.originalEvent.touches[0];
      touch1 = evt.originalEvent.touches[1];

      var x1 = touch1.clientX;
      var y1 = touch1.clientY;
      var x0 = touch0.clientX;
      var y0 = touch0.clientY;

      me.newDist = Math.sqrt((x0 - x1) * (x0 - x1) + (y0 - y1) * (y0 - y1));
      me.distRatio = me.newDist / me.oldDist;

      // Find pinch center after each touch me.vWOffset event with two fingers
      var newPinchCenter = {
        x: (x0 + x1) / 2,
        y: (y0 + y1) / 2
      };

      me.newScale = me.distRatio * me.oldScale;
      var actualZoom = me.newScale * me.currentPageZoom;

      if (actualZoom > me.currentPageMaxZoom) {
        me.newScale = me.currentPageMaxZoom / parseFloat(me.currentPageZoom);
      }

      // Maximo zoom permido el mismo que se calcula al hacer doble clic
      if (actualZoom > window.ldReader.maxPageZoom) {
        me.newScale = window.ldReader.maxPageZoom / parseFloat(me.currentPageZoom);
      }

      // Relative to viewport.
      var pcMidX = me.c.tX + me.vWOffset + width / 2;
      var pcMidY = me.c.tY + height / 2;

      var pcCenter = {
        x: pcMidX,
        y: pcMidY
      };

      var scX = pcCenter.x - (me.newScale / me.oldScale) * (pcCenter.x - me.oldPinchCenter.x);
      var scY = pcCenter.y - (me.newScale / me.oldScale) * (pcCenter.y - me.oldPinchCenter.y);

      var scaledOldPinchCenter = {
        x: scX,
        y: scY
      };

      // Differences in the two pinch centers.
      var offsetX = newPinchCenter.x - scaledOldPinchCenter.x;
      var offsetY = newPinchCenter.y - scaledOldPinchCenter.y;
      me.c.tX = me.c.tX + offsetX;
      me.c.tY = me.c.tY + offsetY;

      me.transform(me.c.$e, me.c.tX, me.c.tY, me.newScale);

      // Update old values
      me.oldScale = me.newScale;
      me.oldDist = me.newDist;
      me.oldPinchCenter.x = newPinchCenter.x;
      me.oldPinchCenter.y = newPinchCenter.y;

      me.shouldRerender = true;
      me.cancelPageRenders();
    }
  },
//this.$annotList.attr('data-last-mode', 'list');
  refreshAnnotationItemLDTron : function(annotation, $li) {
    var me = this;
    var commentString = annotation.getContents() || '';
    var $annotContainer = $li.children('.annotContainer');
    $annotContainer.find('span.comment-text').text(commentString);
    var $textarea = $annotContainer.find('textarea.comment-text');
    $textarea.val(commentString);
    $annotContainer.data('annot', annotation);
    $annotContainer.find('.lastmodified').text( i18n.t('annotations.created') + annotation.DateCreated.toLocaleString());


    // Iconos editar/borrar/salvar/close
    if (!$li.hasClass('ldtron')) {
      $li.addClass('ldtron');

      var $editButtonsContainer = $annotContainer.find('.editButtonsContainer');
      if (!$editButtonsContainer.length) {

        $editButtonsContainer = $(
          '<div class="editButtonsContainer" style="display: block;">' +
          '<a id="btnEdit" class="notesLink" data-i18n="[title]notesPanel.edit" title="Editar">' +
          '<span class="btn btnmini sm-icons sm-icons-editar"></span>' +
          '</a>' +
          '<a id="btnDelete" class="notesLink" data-i18n="[title]notesPanel.delete" title="Borrar">' +
          '<span class="btn btnmini sm-icons sm-icons-papelera"></span>' +
          '</a>' +
          '<a id="btnSave" type="button" data-i18n="[title]notesPanel.save" title="Guardar" style="display: none;">' +
          '<span class="btn btnmini sm-icons sm-icons-guardar"></span>' +
          '</a>' +
          '</a>' +
          '</div>');
        $annotContainer.append($editButtonsContainer);


        var classType = LDTronUtils.getAnnotationIconClass(annotation);
        var nodeType = $('<div class="node-type sm-icons ' + classType + '">');
        $annotContainer.prepend(nodeType);


        // Visibilidad de los botones.
        var $btnEdit = $annotContainer.find('#btnEdit');
        var $btnDelete = $annotContainer.find('#btnDelete');
        var $btnSave = $annotContainer.find('#btnSave');
        var $comment = $annotContainer.find('textarea');

        $btnEdit.on('click', function (e) {
          e.stopImmediatePropagation();
          me.viewAnnotsSingleMode(annotation);

          $btnEdit.hide();
          $btnDelete.hide();
          if (me.mayEditAnnotation(annotation)) {
            $btnSave.show();
          }
        });


        $btnDelete.on('click', function (e) {
          e.stopImmediatePropagation();
          if (annotation.isReply()) {
            var returnMode = me.$annotList.attr('data-return-mode');
            if (returnMode === "thread") {
              if (annotation) {
                me.viewAnnotsThreadMode(annotation);
              } else {
                me.viewAnnotsListMode();
              }
            } else {
              me.viewAnnotsListMode();
            }
          } else {
            //deleting the root
            me.viewAnnotsListMode();
          }
          me.annotationManager.deleteAnnotation(annotation);
        });


        $btnSave.on('click', function (e) {
          e.stopImmediatePropagation();

          $btnEdit.show();
          $btnDelete.show();
          $btnSave.hide();

          me.annotationManager.setNoteContents(annotation, $comment.val());

          if (annotation instanceof Annotations.FreeTextAnnotation) {
            me.annotationManager.drawAnnotationsFromList([annotation]);
          }

          var returnMode = me.$annotList.attr('data-return-mode');
          if (returnMode === "thread") {
            if (annotation) {
              me.viewAnnotsThreadMode(annotation);
              return;
            }
          }
          me.viewAnnotsListMode();
        });


        $textarea.on('blur', function () {
          $btnEdit.show();
          $btnDelete.show();
          $btnSave.hide();
        });
      }
    }
    else {
      $annotContainer.find('#btnEdit').show();
      $annotContainer.find('#btnDelete').show();
      $annotContainer.find('#btnSave').hide();
    }
  }
};