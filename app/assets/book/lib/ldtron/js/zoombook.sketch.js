'use strict';

(function ($) {

    $(document).on('documentLoaded', function () {
        ZoombookSketch.init();
    });

    $(document).on('showSketch', function () {
        if ($('#sketch-area-wrapper').length === 0) {
            $('#zoom-img').attr('src', '../../assets/' + window.contextShown.thumb);

            $('#sketch-popup').addClass('visible');
            Painterro({
                id: 'sketch-area',
                activeColor: '#000000',
                defaulTool: 'brush',
                hiddenTools: ['crop', 'line', 'arrow', 'rotate', 'resize', 'save', 'open', 'close']
            }).show();
            $('#zoom-snapshot').addClass('small');
        }
    });

  var ZoombookSketch = {
      init: function () {
          this.viewer = readerControl.docViewer;
          this.createSketch();
      },
      createSketch: function () {
          var htmlSketch =
              '<div id="sketch-popup" class="visible">' +
                  '<div id="zoom-snapshot">' +
                      '<img id="zoom-img" src="#">' +
                  '</div>' +
                  '<div id="close-sketch"><i data-feather="x"></i></div>' +
                  '<div id="sketch-area"></div>' +
              '</div>';
          $('body').append(htmlSketch);
          feather.replace();
          $('#zoom-snapshot').click(function () {
              $('#zoom-snapshot').removeClass('small');
              setTimeout(function() {
                  $('#sketch-popup').removeClass('visible');
                  $('#sketch-area').empty();
              }, 500);
          });
          $('#sketch-popup').removeClass('visible');
          $('#close-sketch').click(function() {
              $('#zoom-snapshot').click();
          })
      }
  }
})(jQuery);