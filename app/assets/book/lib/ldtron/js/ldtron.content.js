'use strict';

var LDTronContent = {
  show: function(reader, content, $elem) {
    var $ldtron = $('.ldtron');
    var maxWidth = $ldtron.width() - 40;
    content = $.extend(content, {'maxWidth': maxWidth});
    switch (content.type) {
      case 'pagina':
        if (typeof content.src !== 'undefined') {
          var page = content.src - reader.options.initialPage + 1;
          readerControl.docViewer.setCurrentPage(page);
        }
        break;

      case 'document':
        this.downloadDoc(reader.idAPI, this.toAbsolutePath(reader, content).src, true);
        break;

      case 'enllaso':
        this.openPath(reader.idAPI, content.src);
        break;

      /* Brasil - Aunque con codigo comun */
      case 'comparativo':
      case 'interactivo':
      case 'imagen360':
      case 'zoom':
      case 'sm':
      case 'secuencia':
        this.openReference(reader.idAPI, content.src);
        break;

      case 'html':
        var content = this.toAbsolutePath(reader, content);
        if (content.popup === '1') {
          LDTronPopup.openHTMLContent(content);
        }
        else {
          this.openPath(reader.idAPI, content.src);
        }
        break;

      case "imatge":
        if (content.popup === '1') {
          LDTronPopup.openImgContent(this.toAbsolutePath(reader, content));
        }
        else {
          this.openPath(reader.idAPI, pathVisorLDTron + content.src);
        }
        break;

      case 'video':
        LDTronPopup.openVideoContent(this.toAbsolutePath(reader, content));
        break;

      case 'audio':
        LDTronPopup.openAudioContent(this.toAbsolutePath(reader, content), $elem);
        break;

      default:
        alert('Este icono de tipo ' + content.type + ', está por implementar');
        break;
    }
  },

  downloadDoc: function(idAPI, path, isAbsolutePath) {
    if (idAPI !== '') {
      var isAbsolutePath = (isAbsolutePath) ? ', "isAbsolutePath":true' : '';
      var mess = '{"id": "' + idAPI + '", "action":"open", "path": "' + path + '", "type": "doc"' + isAbsolutePath + '}';
      window.top.postMessage(mess, "*");
    }
    else {
      window.open(path);
    }
  },

  openPath: function(idAPI, path) {
    // Si existe Api de Integración
    if (idAPI !== '') {
      var mess = '{"id": "' + idAPI + '", "action":"open", "path": "' + path + '", "type": "link"}';
      window.top.postMessage(mess, "*");
    }
    else {
      window.open(path);
    }
  },

  openReference: function(idAPI, smreference) {
    if (idAPI !== '') {
      var mess = '{"id": "' + idAPI + '" , "action":"open_reference", "imessage": "' + smreference + '" }';
      window.top.postMessage(mess, "*");
    }
    else {
      alert("Contenido no disponible fuera del curso.");
    }
  },

  toAbsolutePath: function(reader, content) {
    var ret = $.extend({}, content);
    if (typeof content.src === 'string' && content.src.indexOf('http://') === -1) {
      ret.src = reader.options.path + ret.src;
    }
    if (typeof content.src2 === 'string' && content.src.indexOf('http://') === -1) {
      ret.src2 = reader.options.path + ret.src2;
    }
    return ret;
  }
};