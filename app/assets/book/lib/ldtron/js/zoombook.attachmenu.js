'use strict';

(function ($) {

    $(document).on('closeAttachMenu', function() {
        ZoomBookAttachMenu.closeAttachMenu();
    });
    $(document).on('openAttachMenu', function() {
        ZoomBookAttachMenu.openAttachMenu();
    });
    $(document).on('documentLoaded', function () {
        ZoomBookAttachMenu.init();
    });
    $(document).on('toggleEditing', function (event, isEditing) {
        if (isEditing) {
            $('#attach-menu').addClass('active');
        } else {
            $('#attach-menu').removeClass('active');
        }
        ZoomBookAttachMenu.closeAttachMenu();

    });
    $(document).on('execOptionSketch', function (event) {
        if (window.showMode === 'document') {
            ZoomBookAttachMenu.sketchContextMode = !ZoomBookAttachMenu.sketchContextMode;
            $('body').toggleClass('click-sketch');
            if (ZoomBookAttachMenu.sketchContextMode) {
                $('#tool-sketch').addClass('circle-set');
                ZoomBookAttachMenu.dragContextMode = false;
                $(document).trigger('DragContextMode', ZoomBookAttachMenu.dragContextMode);
                $('#tool-upload').removeClass('circle-set');
            } else {
                $('#tool-sketch').removeClass('circle-set');
            }
        } else {
            ZoomBookAttachMenu.closeAttachMenu();
            $(document).trigger('showSketch');
        }
    });
    $(document).on('execOptionUpload', function (event) {
        if (window.showMode === 'document') {
            ZoomBookAttachMenu.dragContextMode = !ZoomBookAttachMenu.dragContextMode;
            $(document).trigger('DragContextMode', ZoomBookAttachMenu.dragContextMode);
            if (ZoomBookAttachMenu.dragContextMode) {
                $('#tool-upload').addClass('circle-set');
                ZoomBookAttachMenu.sketchContextMode = false;
                $('body').removeClass('click-sketch');
                $('#tool-sketch').removeClass('circle-set');
            } else {
                $('#tool-upload').removeClass('circle-set');
            }
        } else {
            $('#' + window.contextShown.id).find('input').click();
        }
    });

    var ZoomBookAttachMenu = {
        active: false,
        dist: 60,
        spreadAngle: 60,
        lessIndex:0 ,
        dragContextMode: false,
        sketchContextMode: false,

        init: function() {
            this.viewer = readerControl.docViewer;
            this.createAttachMenu();
            this.eventAttachButtons();
        },

        createAttachMenu: function() {
            var htmlAttachMenu =
                '<div id="attach-menu">' +
                '<div class="attach-background-menu"></div>' +
                '<div id="tool-sketch" class="tool round-button attach-option tooltip" title="Pizarra digital"><i data-icons="pizarra.svg"></span></i></div>' +
                '<div id="tool-upload" class="tool round-button attach-option tooltip" title="Adjuntar contenidos"><i data-feather="upload"></span></i></div>' +
                '<div id="toggle-attach" class="round-button tooltip" title="Añadir recursos"><div id="toggle-x-attach"><i data-feather="x"></i></div><div id="toggle-paperclip"><i data-feather="paperclip"></i></div></div>' +
                '</div>';
            $('body').append(htmlAttachMenu);
            feather.replace();
        },

        eventAttachButtons: function() {
            $('#toggle-attach').click(this.toggleAttachMenu);
            $('.attach-option').click(this.clickCircle);
        },

        clickCircle: function() {
            var id = $(this).attr('id').split('-');
            // Se envía un evento del tipo "toggleOptionTeacher", "toggleOptionContexts"...
            $(document).trigger('execOption' + id[1].charAt(0).toUpperCase() + id[1].slice(1));
        },

        showAttachMenu: function() {
            $('#attach-menu').show('slow');
        },

        toggleAttachMenu: function() {
            if (ZoomBookAttachMenu.active) {
                ZoomBookAttachMenu.closeAttachMenu();
            } else {
                ZoomBookAttachMenu.openAttachMenu();
            }
        },

        hideAttachMenu: function() {
            $('#attach-menu').hide('slow');
        },

        openAttachMenu: function() {
            $(document).trigger('closeCircleMenu');
            $('#attach-menu').addClass('opened');
            var slices = $('.attach-option').length - 1;
            var angle = (ZoomBookAttachMenu.spreadAngle / slices);
            anime({
                targets: '#toggle-x-attach',
                opacity: 1,
            });
            anime({
                targets: '#toggle-paperclip',
                opacity: 0,
            });
            anime({
                targets: '.attach-option',
                opacity: [0, 1],
                delay: anime.stagger(100),
                translateX: function(el, i) {
                    return (-1) * Math.round(Math.cos(angle * (i - ZoomBookAttachMenu.lessIndex) * Math.PI / 180) * ZoomBookAttachMenu.dist);
                },
                translateY: function(el, i) {
                    return (-1) * Math.round(Math.sin(angle * (i - ZoomBookAttachMenu.lessIndex) * Math.PI / 180) * ZoomBookAttachMenu.dist);
                },
            });
            this.active = true;
        },

        closeAttachMenu: function() {
            $('#attach-menu').removeClass('opened');
            ZoomBookAttachMenu.dragContextMode = false;
            $('#tool-upload').removeClass('circle-set');
            $(document).trigger('DragContextMode', false);
            anime({
                targets: '#toggle-x-attach',
                opacity: 0,
            });
            anime({
                targets: '#toggle-paperclip',
                opacity: 1,
            });
            anime({
                targets: '.attach-option',
                rotate: 0,
                opacity: [1, 0],
                delay: anime.stagger(100),
                translateX: 0,
                translateY: 0,
            });
            this.active = false;
        }

    };
})(jQuery);