'use strict';

(function ($) {

  window.showMode = 'document';

  $(document).ready(function () {
    ZoomBookFullScreen.init();
  });

  var ZoomBookFullScreen = {

    init: function() {
      this.createButton();
    },

    createButton: function() {
      var htmlButton =
        '<div id="FullscreenMinimize" class="button-fullscreen svg-button"><i data-feather="minimize-2"></i></div>' +
        '<div id="FullscreenMaximize" class="button-fullscreen svg-button"><i data-feather="maximize-2"></i></div>';
      $('body').append(htmlButton);
      feather.replace();
      this.eventButtons();
    },

    eventButtons: function () {
      var me = this;
      $('#FullscreenMaximize').on('click', function () {
        ZoomBookUtil.animationClick($(this), 'flash', function () {
          me.openFullscreen(document.documentElement);
        });
      });

      $('#FullscreenMinimize').on('click', function () {
        ZoomBookUtil.animationClick($(this), 'flash', function () {
          me.closeFullscreen(document.documentElement);
        });
      });
    },

    openFullscreen : function (elem) {
      if (elem.requestFullscreen) {
        elem.requestFullscreen();
      } else if (elem.mozRequestFullScreen) { /* Firefox */
        elem.mozRequestFullScreen();
      } else if (elem.webkitRequestFullscreen) { /* Chrome, Safari and Opera */
        elem.webkitRequestFullscreen();
      } else if (elem.msRequestFullscreen) { /* IE/Edge */
        elem.msRequestFullscreen();
      }
      $('#FullscreenMaximize').hide();
      $('#FullscreenMinimize').show();
    },


    closeFullscreen: function () {
      if (document.exitFullscreen) {
        document.exitFullscreen();
      } else if (document.mozCancelFullScreen) { /* Firefox */
        document.mozCancelFullScreen();
      } else if (document.webkitExitFullscreen) { /* Chrome, Safari and Opera */
        document.webkitExitFullscreen();
      } else if (document.msExitFullscreen) { /* IE/Edge */
        document.msExitFullscreen();
      }
      $('#FullscreenMaximize').show();
      $('#FullscreenMinimize').hide();
    }
  };
})(jQuery);