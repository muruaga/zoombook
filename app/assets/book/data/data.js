var viewerConfig = {showCoverPage:0, initialPage:8,totalPages:16,pdfVisibleTitle:"El cuerpo humano y la relaciÃ³n",pdfFile:"160227-LIBRO-unidad-01-W.pdf",lang:"ES",sello:"1",color:"",expAnnots:"",twoPages:"1",defaultPages:"1",draw:"1"};

var contentData=new Array();

var tableOutline = new Array();

var tablePersonal = new Array();

tableOutline.push({title:"1. Las tres funciones vitales",page:"8"});

tableOutline.push({title:"2. La función de relación",page:"10"});

tableOutline.push({title:"3. Los Órganos receptores",page:"12"});

tableOutline.push({title:"4. El sistema nervioso",page:"14"});

tableOutline.push({title:"5. Los Órganos efectores",page:"16"});

tableOutline.push({title:"Organiza tus ideas",page:"18"});

tableOutline.push({title:"Repasa la unidad",page:"20"});

tableOutline.push({title:"Ponte a prueba",page:"22"});

var contextData = new Array();

contextData.push({id:"ff-f-dsfds-dsfsd1",page: 1, x: 60, y:30, width: 440,  height: 230, type: 'section', title:'Las tres funciones vitales', thumb:'imagenes/zona1.png',
    activities: [
        {title:'El cuerpo humano para niños', type:'video', origin:'sm', url:'https://www.youtube.com/watch?v=ppUnmAvLhwE'},
        {title:'Partes del cuerpo humano', type:'image', origin: 'sm-teacher', url:'https://upload.wikimedia.org/wikipedia/commons/thumb/e/eb/Human_body_features-es.svg/1024px-Human_body_features-es.svg.png'},
        {title:'Sistema digestivo', type:'image', origin: 'sm-teacher', url:'https://images.app.goo.gl/xxrYXxejbXdsrSwT8'},
        {title:'Civilización Romana', type:'interactive', origin: 'sm-teacher', url:'http://demo.grupo-sm.es/Civilizacion_Romana/index.html'}]
});
contextData.push({id:"fd-g-fff66-d797d2",page: 1, x: 60, y:260, width: 440,  height: 323, type: 'section', title:'El cuerpo humano y la nutrición', thumb:'imagenes/zona2.png',
    activities: []
});
contextData.push({id:"fd-g-fff66-d797d4",page: 1, x: 60, y:590, width: 720,  height: 470, type: 'section', title:'Aparatos', thumb:'imagenes/zona3.png', activities: []});
contextData.push({id:"ff-f-dsfds-dsfsd5",page: 2, x: 60, y:70, width: 450,  height: 240, type: 'section', title:'El ser humano y la relación', thumb:'imagenes/zona4.png',
    activities: [
        {title:'Civilización Romana', type:'interactive', origin: 'sm-teacher', url:'http://demo.grupo-sm.es/Civilizacion_Romana/index.html'}]
});
contextData.push({id:"ff-f-dsfds-dsfsd6",page: 2, x: 60, y:310, width: 450,  height: 160, type: 'section', title:'El ser humano y la reproducción', thumb:'imagenes/zona5.png',
    activities: []
});
contextData.push({id:"ff-f-dsfds-dsfsd7",page: 2, x: 60, y:470, width: 450,  height: 280, type: 'section', title:'La salud de nuestro organismo', thumb:'imagenes/zona6.png',
    activities: [
        {title:'Civilización Romana', type:'interactive', origin: 'sm-teacher', url:'http://demo.grupo-sm.es/Civilizacion_Romana/index.html'}]
});
contextData.push({id:"ff-f-dsfds-dsfsd9",page: 2, x: 60, y:750, width: 730,  height: 300, type: 'section', title:'Actividades', thumb:'imagenes/zona7.png',
    activities: [
        {title:'Soluciones', type:'solutions', origin: 'sm', solutions:[
                {number:1, question:'Copia dos de las columnas en tu cuaderno y relaciónalas con flechas', answer:'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ac tincidunt ante, eu venenatis purus. Sed dictum tortor orci, et cursus diam viverra vitae. Etiam laoreet finibus tellus, ut rhoncus dui vulputate a. Fusce fringilla laoreet euismod. Aliquam erat volutpat. Cras ipsum velit, pharetra vitae urna non, aliquam consequat tortor. Etiam tellus velit, consectetur vel enim a, volutpat sodales magna. Proin laoreet velit et feugiat accumsan. In non commodo risus. Nullam neque purus, iaculis dignissim augue eu, fringilla consectetur elit. In metus metus, vulputate sed blandit eget, tristique at dolor.'},
                {number:2, question:'¿Qué crees que ocurriría en nuestro organismo si el aparato excretor no funcionara de forma correcta?', answer:'Nunc efficitur dapibus tortor ut iaculis. Mauris non tempus quam, eu gravida velit. Maecenas egestas tristique enim ac feugiat. Mauris eu ligula non nisi condimentum lacinia. Pellentesque ut ipsum tristique, luctus magna eget, vulputate ante. Donec ut leo et mauris facilisis commodo eget ac ipsum. Phasellus facilisis tortor justo, eget gravida ipsum semper nec. Curabitur id maximus enim, vitae sodales augue. Aenean mollis, est euismod vehicula fringilla, metus sapien auctor arcu, ut egestas lorem nisi eu ante. Duis bibendum ex sem, nec commodo purus tincidunt vitae. Suspendisse sit amet tincidunt mauris, vitae mollis sapien. Morbi scelerisque aliquet sem, in aliquam diam sagittis ut. Suspendisse sed libero justo. Donec sapien lacus, gravida eget ex et, porta elementum odio. Nunc tincidunt interdum lectus vel faucibus.'},
                {number:3, question:'Actualmente está prohibido fumar en lugares públicos como comercios, cafeterías o colegios.', answer:'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Donec metus lacus, egestas eget erat sit amet, scelerisque semper ante. Donec blandit, neque eu pulvinar luctus, dolor augue lobortis erat, at varius neque ex in ligula. Vestibulum auctor fringilla ante vel tincidunt. Nunc rhoncus gravida dui, a interdum mauris aliquam quis. Duis massa ex, finibus sed augue at, euismod sagittis magna. Etiam ipsum est, facilisis ut pretium quis, mattis quis risus. Pellentesque consequat tristique felis, in dignissim augue varius in. Donec facilisis lacus eu quam fringilla, a luctus urna venenatis. Curabitur fringilla euismod dapibus. Sed sed nunc quis lectus mollis consectetur. Ut gravida ipsum quis volutpat rhoncus. Praesent egestas nec purus at ullamcorper. In interdum leo id auctor efficitur. Aliquam quis risus et massa mattis cursus sed vitae nunc. Suspendisse potenti.'}
            ]
        }]
});